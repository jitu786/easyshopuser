package com.easyshopuser.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.easyshopuser.R;

import java.util.ArrayList;
import java.util.HashMap;

public class Order_Return_Adapter extends RecyclerView.Adapter<Order_Return_Adapter.Myview>
{


    Context context;
    ArrayList<HashMap<String, String>> dataList;
    Order_Return_Adapter.OnItemClickListener mItemClickListener;

    public Order_Return_Adapter(Context context, ArrayList<HashMap<String, String>> dataList)

    {
        this.context = context;
        this.dataList=dataList;

    }

    public interface OnItemClickListener {
        void onItemClickList(View v, int position, int btnType, int qty);
    }
    public void setOnItemClickListener(OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }
    @NonNull
    @Override
    public Order_Return_Adapter.Myview onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.product_return_list, viewGroup, false);
        return new Order_Return_Adapter.Myview(v);

    }

    @Override
    public void onBindViewHolder(@NonNull final Order_Return_Adapter.Myview holder, final int position) {

        final HashMap<String, String> item = dataList.get(position);

        holder.view_order_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mItemClickListener.onItemClickList(view,position,1,0);
            }
        });
        holder.return_id.setText("Return Id :"+" "+item.get("return_id"));
        holder.return_status.setText(item.get("return_action"));
        holder.date_added.setText("Date Added :"+" "+item.get("date_added"));
        holder.customer.setText("Customer :"+" "+item.get("firstname")+ " " +item.get("lastname"));
        holder.order_id.setText("Order Id :"+" "+item.get("order_id"));

        Glide
                .with(context)
                .load(item.get("image"))
                .placeholder(R.drawable.imgcatlist)
                .into(holder.product_image);

    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }


    public class Myview extends RecyclerView.ViewHolder {
        TextView return_id,return_status,date_added,order_id,customer;
        LinearLayout view_order_detail;
        ImageView product_image;
        public Myview(@NonNull View itemView) {
            super(itemView);
            product_image=itemView.findViewById(R.id.product_image);
            view_order_detail=itemView.findViewById(R.id.view_order_detail);
            return_id=itemView.findViewById(R.id.return_id);
            return_status=itemView.findViewById(R.id.return_status);
            date_added=itemView.findViewById(R.id.date_added);
            order_id=itemView.findViewById(R.id.order_id);
            customer=itemView.findViewById(R.id.customer);

        }
    }
}

