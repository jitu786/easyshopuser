package com.easyshopuser.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.easyshopuser.R;

import java.util.ArrayList;
import java.util.HashMap;

public class Cart_Adapter extends RecyclerView.Adapter<Cart_Adapter.Myview>
{


    Context context;
    ArrayList<HashMap<String, String>> dataList = new ArrayList<>();
    private OnItemClickListener mItemClickListener;
    public Cart_Adapter(Context context, ArrayList<HashMap<String, String>> dataList)

    {
        this.context = context;
        this.dataList=dataList;
    }


    @NonNull
    @Override
    public Cart_Adapter.Myview onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.cart_list, viewGroup, false);
        return new Cart_Adapter.Myview(v);

    }
    public interface OnItemClickListener {
        void onItemClickList(View v, int position, int btnType,int qty);
    }


    public void setOnItemClickListener(OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }
    @Override
    public void onBindViewHolder(@NonNull final Cart_Adapter.Myview holder, final int position) {
        final HashMap<String,String> map=dataList.get(position);
        holder.cart_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mItemClickListener.onItemClickList(view,position,4,0);

            }
        });
        holder.p_Price.setText("Rs "+map.get("price"));
        holder.p_name.setText(map.get("name"));
        holder.p_desc.setText(map.get("description"));
        holder.p_qty.setText(map.get("quantity"));

        Glide.with(context)
                .load(map.get("image"))
                .placeholder(R.drawable.bwatch)
                .into(holder.img);

        holder.remove_cart_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mItemClickListener.onItemClickList(view,position,1,0);
            }
        });

        holder.move_to_fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mItemClickListener.onItemClickList(view,position,2,0);
            }
        });

        holder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Integer.parseInt(map.get("quantity"))>1)
                {
                    int qty = Integer.parseInt(map.get("quantity")) - 1;
                    holder.p_qty.setText("" + qty);
                    mItemClickListener.onItemClickList(view,position,3,qty);

                }
            }
        });

        holder.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int qty=Integer.parseInt(holder.p_qty.getText().toString())+1;
                holder.p_qty.setText(""+qty);
                mItemClickListener.onItemClickList(view,position,3,qty);
            }
        });
        holder.click_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }


    public class Myview extends RecyclerView.ViewHolder {
        RelativeLayout cart_item;
        TextView p_desc,p_name,p_Price,p_qty,move_to_fav;
        ImageView img,plus,minus,remove_cart_item;
        LinearLayout click_layout;
        public Myview(@NonNull View itemView) {
            super(itemView);
            cart_item=itemView.findViewById(R.id.cart_item);
            p_desc=itemView.findViewById(R.id.p_desc);
            p_name=itemView.findViewById(R.id.p_name);
            p_qty=itemView.findViewById(R.id.p_qty);
            p_Price=itemView.findViewById(R.id.p_Price);
            img=itemView.findViewById(R.id.img);
            plus=itemView.findViewById(R.id.plus);
            minus=itemView.findViewById(R.id.minus);
            remove_cart_item=itemView.findViewById(R.id.remove_cart_item);
            move_to_fav=itemView.findViewById(R.id.move_to_fav);
            click_layout=itemView.findViewById(R.id.click_layout);

        }
    }
}
