package com.easyshopuser.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.easyshopuser.Activity.ProductDescription;
import com.easyshopuser.R;

import java.util.ArrayList;
import java.util.HashMap;

public class Favourite_Adapter extends RecyclerView.Adapter<Favourite_Adapter.Myview>
{


    Context context;
    ArrayList<HashMap<String, String>> dataList;
    private OnItemClickListener mItemClickListener;

    public Favourite_Adapter(Context context,ArrayList<HashMap<String, String>> dataList)
    {
        this.context = context;
        this.dataList=dataList;
    }
    public interface OnItemClickListener {
        void onItemClickList(View v, int position, int btnType, int qty);
    }
    public void setOnItemClickListener(OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }
    @NonNull
    @Override
    public Favourite_Adapter.Myview onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.fav_list, viewGroup, false);
        return new Favourite_Adapter.Myview(v);

    }

    @Override
    public void onBindViewHolder(@NonNull final Favourite_Adapter.Myview holder, final int position) {


        final HashMap<String, String> item = dataList.get(position);
        name.setText(item.get("name"));
        Glide
                .with(context)
                .load(item.get("image"))
                .placeholder(R.drawable.imgcatlist)
                .into(image_Pro);
        price.setText("$"+item.get("price"));





      /*  if(item.get("TotalRating").isEmpty())
            ratingbar.setRating(0);
        else
            ratingbar.setRating(Float.parseFloat(item.get("TotalRating")));

*/

        product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, ProductDescription.class);
                intent.putExtra("product_id",item.get("product_id"));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
        cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mItemClickListener.onItemClickList(view,position,1,0);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    RelativeLayout product;
    ImageView image_Pro;
    CardView cancle;
    TextView price,name;
    RatingBar ratingbar;
    public class Myview extends RecyclerView.ViewHolder {
        public Myview(@NonNull View itemView) {


            super(itemView);
            cancle=itemView.findViewById(R.id.cancle);
            image_Pro=itemView.findViewById(R.id.image_Pro);
            price=itemView.findViewById(R.id.price);
            name=itemView.findViewById(R.id.name);
            product=itemView.findViewById(R.id.product);
            ratingbar=itemView.findViewById(R.id.ratingbar);
        }
    }
}

