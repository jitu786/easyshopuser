package com.easyshopuser.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.SectionIndexer;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.easyshopuser.Activity.ProductDescription;
import com.easyshopuser.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CatProductListAdapter  extends RecyclerView.Adapter<CatProductListAdapter.Myview> implements SectionIndexer
{


    Context context;
    ArrayList<HashMap<String, String>> dataList = new ArrayList<>();
    private ArrayList<Integer> mSectionPositions;



    public CatProductListAdapter(Context context, ArrayList<HashMap<String, String>> dataList)
    {
        this.context = context;
        this.dataList=dataList;
    }

    public void filterList(ArrayList<HashMap<String, String>> dataList1)
    {
        this.dataList=dataList1;
        notifyDataSetChanged();
    }


    @Override
    public int getSectionForPosition(int position) {
        return position;
    }
    @Override
    public Object[] getSections() {
        List<String> sections = new ArrayList<>(dataList.size());
        mSectionPositions = new ArrayList<>(dataList.size());
        for (int i = 0, size = dataList.size(); i < size; i++) {
            HashMap<String, String> data= dataList.get(i);

            String section = String.valueOf((data.get("name").charAt(0)+"").toUpperCase());
            if (!sections.contains(section)) {
                sections.add(section);
                mSectionPositions.add(i);
            }
        }
        return sections.toArray(new String[0]);
    }

    @Override
    public int getPositionForSection(int sectionIndex) {
        return mSectionPositions.get(sectionIndex);
    }




    @NonNull
    @Override
    public CatProductListAdapter.Myview onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.adapter_category_product_list, viewGroup, false);
        return new CatProductListAdapter.Myview(v);

    }

    @Override
    public void onBindViewHolder(@NonNull final CatProductListAdapter.Myview holder, int position) {

        final HashMap<String,String> map=dataList.get(position);

        holder.price.setText("Rs "+map.get("price"));
        holder.p_name.setText(map.get("name"));
        holder.cat_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, ProductDescription.class);
                intent.putExtra("product_id",map.get("product_id"));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
        if(map.get("TotalRating").isEmpty())
            holder.ratingbar.setRating(0);
        else
            holder.ratingbar.setRating(Float.parseFloat(map.get("TotalRating")));
        Glide.with(context)
                .load(map.get("image"))
//.diskCacheStrategy(DiskCacheStrategy.NONE)
                .placeholder(R.drawable.watch)
//.skipMemoryCache(true)
                .into(holder.img);


    }


    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class Myview extends RecyclerView.ViewHolder {
        LinearLayout cat_list;
        ImageView img;
        TextView p_name,price;
        RatingBar ratingbar;
        public Myview(@NonNull View itemView) {
            super(itemView);

            cat_list=itemView.findViewById(R.id.cat_list);
            img=itemView.findViewById(R.id.img);
            p_name=itemView.findViewById(R.id.p_name);
            price=itemView.findViewById(R.id.price);
            ratingbar=itemView.findViewById(R.id.ratingbar);

        }
    }
}

