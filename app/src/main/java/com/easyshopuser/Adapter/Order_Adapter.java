package com.easyshopuser.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.easyshopuser.R;

import java.util.ArrayList;
import java.util.HashMap;

public class Order_Adapter extends RecyclerView.Adapter<Order_Adapter.Myview>
{


    Context context;
    ArrayList<HashMap<String, String>> dataList ;
    OnItemClickListener mItemClickListener;
    public Order_Adapter(Context context, ArrayList<HashMap<String, String>> dataList)

    {
        this.context = context;
        this.dataList=dataList;
    }


    @NonNull
    @Override
    public Order_Adapter.Myview onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.order_list, viewGroup, false);
        return new Order_Adapter.Myview(v);

    }
    public interface OnItemClickListener {
        void onItemClickList(View v, int position, int btnType,int qty);
    }
    public void setOnItemClickListener(OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }
    @Override
    public void onBindViewHolder(@NonNull final Order_Adapter.Myview holder, final int position) {

        HashMap<String,String> map=dataList.get(position);
        holder.view_order_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mItemClickListener.onItemClickList(view,position,1,0);
            }
        });
        holder.order_id.setText("Order ID: "+map.get("order_id"));
        holder.txtprice.setText("Rs "+map.get("totalPrice"));
        holder.txtProductname.setText(map.get("productName"));
        holder.txtstatus.setText(map.get("orderStatus"));

/* if(map.get("order_status_id").equals("5"))
{
holder.txtstatus.setText("Complete");

}*/
        Glide.with(context)
                .load(map.get("image"))
                .placeholder(R.drawable.imgcatlist)
                .into(holder.product_image);

    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }


    public class Myview extends RecyclerView.ViewHolder {
        LinearLayout view_order_detail;
        TextView order_id,txtstatus,txtProductname,txtprice;
        ImageView product_image;
        public Myview(@NonNull View itemView) {
            super(itemView);

            view_order_detail=itemView.findViewById(R.id.view_order_detail);
            order_id=itemView.findViewById(R.id.order_id);
            txtstatus=itemView.findViewById(R.id.txtstatus);
            txtProductname=itemView.findViewById(R.id.txtProductname);
            txtprice=itemView.findViewById(R.id.txtprice);
            product_image=itemView.findViewById(R.id.product_image);

        }
    }
}



