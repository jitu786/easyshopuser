package com.easyshopuser.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.easyshopuser.Activity.ProductDescription;
import com.easyshopuser.R;

import java.util.ArrayList;
import java.util.HashMap;

public class RecentViewedAdapter extends RecyclerView.Adapter<RecentViewedAdapter.Myview>
{


    Context context;
    ArrayList<HashMap<String, String>> dataList;
    public String CURRENT_PRODUCT_DISPLAY_MODE = "GRID";

    public RecentViewedAdapter(Context context,ArrayList<HashMap<String, String>> dataList)
    {
        this.context = context;
        this.dataList = dataList;
    }


    @NonNull
    @Override
    public RecentViewedAdapter.Myview onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
//View v = LayoutInflater.from(context).inflate(R.layout.recent_viewed_list, viewGroup, false);
        View view = LayoutInflater.from(context).inflate(CURRENT_PRODUCT_DISPLAY_MODE.equals("GRID") ? R.layout.recent_viewed_list : R.layout.item_main_page_category_list_design, viewGroup, false);
        return new RecentViewedAdapter.Myview(view);

    }

    @Override
    public void onBindViewHolder(@NonNull final RecentViewedAdapter.Myview holder, int position) {

        final HashMap<String, String> item = dataList.get(position);

        Glide
                .with(context)
                .load(item.get("image"))
                .placeholder(R.drawable.imgcatlist)
//.error(R.drawable.load)
                .into(holder.img);
        holder.p_name.setText(item.get("name"));
        holder.price.setText("$"+item.get("price"));

        holder.rv_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, ProductDescription.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("product_id",item.get("product_id"));
                context.startActivity(intent);
            }
        });

        if(!item.get("special_price").isEmpty())
        {
            holder.special_price.setVisibility(View.VISIBLE);
            Double per=(((Double.parseDouble(item.get("price"))-Double.parseDouble(item.get("special_price")))*100)/Double.parseDouble(item.get("price")));
            holder.txtdiscount.setText("-"+Math.round( per)+"%");
            holder.special_price.setText("$"+item.get("special_price"));
            holder.price.setPaintFlags(holder.price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.share.setVisibility(View.VISIBLE);
        }
        else
        {
            holder.special_price.setVisibility(View.GONE);
            holder.txtdiscount.setVisibility(View.GONE);
            holder.share.setVisibility(View.GONE);
        }

        if(item.get("TotalRating").isEmpty())
            holder.ratingBar.setRating(0);
        else
            holder.ratingBar.setRating(Float.parseFloat(item.get("TotalRating")));




    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }


    public class Myview extends RecyclerView.ViewHolder {
        LinearLayout rv_list;
        ImageView img;
        TextView p_name,price,txtdiscount,special_price;
        CardView share;
        RatingBar ratingBar;
        public Myview(@NonNull View itemView) {
            super(itemView);
            img=itemView.findViewById(R.id.img);
            p_name=itemView.findViewById(R.id.p_name);
            price=itemView.findViewById(R.id.price);
            rv_list=itemView.findViewById(R.id.rv_list);
            txtdiscount=itemView.findViewById(R.id.txtdiscount);
            special_price=itemView.findViewById(R.id.special_price);
            share=itemView.findViewById(R.id.share);
            ratingBar=itemView.findViewById(R.id.ratingbar);


        }
    }
}
