package com.easyshopuser.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.easyshopuser.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ViewPagerAdapter extends PagerAdapter {

    private static final String TAG = "ViewPagerAdapter";
    private Context context;
    private LayoutInflater layoutInflater;
    private List images = new ArrayList();
    int index;
    public ViewPagerAdapter(Context context, List images, int position) {
        this.context = context;
        this.images=images;
        index=position;
        // this.images.addAll(images);
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.image_dialoug, null);
        ImageView imageView = (ImageView) view.findViewById(R.id.user_fullimage);
        Picasso.with(context)
                .load(String.valueOf(images.get(index)))
                .fit()
                .into(imageView);

        Log.e(TAG, "instantiateItem: "+String.valueOf(images.get(index)) );
        //imageView.setImageResource(R.drawable.add_money_button_normal);

        ViewPager vp = (ViewPager) container;
        vp.addView(view, 0);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        ViewPager vp = (ViewPager) container;
        View view = (View) object;
        vp.removeView(view);
    }
}

