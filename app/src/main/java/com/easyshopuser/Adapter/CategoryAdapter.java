package com.easyshopuser.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.easyshopuser.Activity.Category_Product_List;
import com.easyshopuser.R;
import com.easyshopuser.utils.CommonUtilities;

import java.util.ArrayList;
import java.util.HashMap;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.Myview>
{


    Context context;
    ArrayList<HashMap<String, String>> dataList = new ArrayList<>();
    public CategoryAdapter(Context context, ArrayList<HashMap<String, String>> dataList)
    {
        this.context = context;
        this.dataList=dataList;
    }
    @NonNull
    @Override
    public CategoryAdapter.Myview onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.category_frg_list, viewGroup, false);
        return new CategoryAdapter.Myview(v);

    }

    @Override
    public void onBindViewHolder(@NonNull final CategoryAdapter.Myview holder, int position) {

        final HashMap<String,String> map=dataList.get(position);


        holder.cat_name.setText(map.get("name"));


        Glide.with(context)
                .load(CommonUtilities.Image_Common_URL+map.get("image"))
//.diskCacheStrategy(DiskCacheStrategy.NONE)
                .placeholder(R.drawable.watch)
//.skipMemoryCache(true)
                .into(holder.img);

        holder.cat_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, Category_Product_List.class);
                intent.putExtra("product_id",map.get("product_id"));
                intent.putExtra("category_id",map.get("category_id"));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class Myview extends RecyclerView.ViewHolder {
        LinearLayout cat_list;
        ImageView img;
        TextView cat_name;
        public Myview(@NonNull View itemView) {
            super(itemView);

            cat_list=itemView.findViewById(R.id.cat_list);
            img=itemView.findViewById(R.id.cat_img);
            cat_name=itemView.findViewById(R.id.cat_name);


        }
    }
}



