package com.easyshopuser.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.easyshopuser.R;
import com.easyshopuser.generalfiles.GeneralFunctions;

import java.util.ArrayList;
import java.util.HashMap;

public class CustomerAddressRecycleAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    ArrayList<HashMap<String, String>> list;
    Context mContext;
    public GeneralFunctions generalFunc;

    private OnItemClickListener mItemClickListener;
    public CustomerAddressRecycleAdapter(Context mContext, ArrayList<HashMap<String, String>> list, GeneralFunctions generalFunc, boolean isFooterEnabled)
    {
        this.mContext = mContext;
        this.list = list;
        this.generalFunc = generalFunc;
    }

    public interface OnItemClickListener {
        void onItemClickList(View v, int btnType, int position);
    }

    public void setOnItemClickListener(OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_addresses_design, parent, false);
        return new ViewHolder(view);

    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        HashMap<String,String> map=list.get(position);
        viewHolder.txtname.setText(""+map.get("firstname")+" "+map.get("lastname"));
        viewHolder.txtaddress1.setText("Address1:"+map.get("address_1"));
        viewHolder.txtaddress2.setText("Address2:"+map.get("address_2"));
        viewHolder.txtaddress2.setText("Address2:"+map.get("address_2"));
        viewHolder.txtcitypin.setText("City:"+map.get("city") +" PinCode:"+map.get("postcode"));
        viewHolder.txtcountry.setText("Country:"+map.get(""));

        viewHolder.removeItemArea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mItemClickListener.onItemClickList(view,1,position);
            }
        });

        viewHolder.editItemArea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mItemClickListener.onItemClickList(view,2,position);
            }
        });

        viewHolder.edit_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mItemClickListener.onItemClickList(view,3,position);
            }
        });
    }

    // inner class to hold a reference to each item of RecyclerView
    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txtname,txtaddress1,txtaddress2,txtcitypin,txtstate,txtcountry;
        public View editItemArea;
        public View edit_address;
        public View removeItemArea;
        public ViewHolder(View view) {
            super(view);
            txtcountry=view.findViewById(R.id.txtcountry);
            txtname=view.findViewById(R.id.txtname);
            txtaddress1=view.findViewById(R.id.txtaddress1);
            txtaddress2=view.findViewById(R.id.txtaddress2);
            txtcitypin=view.findViewById(R.id.txtcitypin);
            txtstate=view.findViewById(R.id.txtstate);
            txtcountry=view.findViewById(R.id.txtcountry);
            removeItemArea=view.findViewById(R.id.removeItemArea);
            editItemArea=view.findViewById(R.id.editItemArea);
            edit_address=view.findViewById(R.id.edit_address);

        }
    }

    // Return the size of your itemsData (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return list.size();
    }
}


