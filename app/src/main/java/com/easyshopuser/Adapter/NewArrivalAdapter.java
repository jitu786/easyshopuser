package com.easyshopuser.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.easyshopuser.Activity.ProductDescription;
import com.easyshopuser.R;
import com.easyshopuser.utils.CommonUtilities;

import java.util.ArrayList;
import java.util.HashMap;

public class NewArrivalAdapter extends RecyclerView.Adapter<NewArrivalAdapter.Myview>
{


    Context context;
    ArrayList<HashMap<String, String>> dataList = new ArrayList<>();
    public NewArrivalAdapter(Context context, ArrayList<HashMap<String, String>> dataList)
    {
        this.context = context;
        this.dataList=dataList;
    }
    @NonNull
    @Override
    public NewArrivalAdapter.Myview onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.new_arrival_list, viewGroup, false);
        return new NewArrivalAdapter.Myview(v);

    }

    @Override
    public void onBindViewHolder(@NonNull final NewArrivalAdapter.Myview holder, int position) {
        final HashMap<String,String> map=dataList.get(position);

        holder.price.setText("Rs "+map.get("price"));
        holder.p_name.setText(map.get("name"));
        holder.new_arrival_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, ProductDescription.class);
                intent.putExtra("product_id",map.get("product_id"));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
        Glide.with(context)
                .load(map.get("image"))
//.diskCacheStrategy(DiskCacheStrategy.NONE)
                .placeholder(R.drawable.imgcatlist)
//.skipMemoryCache(true)
                .into(holder.img);

        if(map.get("TotalRating").isEmpty())
            holder.ratingbar.setRating(0);
        else
            holder.ratingbar.setRating(Float.parseFloat(map.get("TotalRating")));




    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class Myview extends RecyclerView.ViewHolder {
        LinearLayout new_arrival_list;
        ImageView img;
        TextView p_name,price;
        RatingBar ratingbar;

        public Myview(@NonNull View itemView) {
            super(itemView);

            new_arrival_list=itemView.findViewById(R.id.new_arrival_list);
            img=itemView.findViewById(R.id.img);
            p_name=itemView.findViewById(R.id.p_name);
            ratingbar=itemView.findViewById(R.id.ratingbar);

            price=itemView.findViewById(R.id.price);

        }
    }
}

