package com.easyshopuser.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.easyshopuser.Activity.ProductDescription;
import com.easyshopuser.R;

import java.util.ArrayList;
import java.util.HashMap;

public class Flash_DealAdapter extends RecyclerView.Adapter<Flash_DealAdapter.Myview>
{


    Context context;
    ArrayList<HashMap<String, String>> dataList=new ArrayList<>();
    public Flash_DealAdapter(Context context, ArrayList<HashMap<String, String>> dataList)
    {
        this.context = context;
        this.dataList=dataList;
    }



    @NonNull
    @Override
    public Flash_DealAdapter.Myview onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.flash_deal_list, viewGroup, false);
        return new Flash_DealAdapter.Myview(v);

    }

    @Override
    public void onBindViewHolder(@NonNull final Flash_DealAdapter.Myview holder, int position) {
        final HashMap<String,String> map=dataList.get(position);

        Glide
                .with(context)
                .load(map.get("image"))
                .placeholder(R.drawable.suit)
                .into(holder.img);
        holder.price.setPaintFlags(holder.price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        holder.price.setText("Rs."+""+map .get("price"));
        holder.p_name.setText(""+map .get("name"));

        Double per=(((Double.parseDouble(map.get("price"))-Double.parseDouble(map.get("special_price")))*100)/Double.parseDouble(map.get("price")));
        holder.discount.setText(Math.round( per)+"%");

        holder.spcial_price.setText("Rs "+map.get("special_price"));

        holder.flash_deal_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, ProductDescription.class);
                intent.putExtra("product_id",map.get("product_id"));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
        if(map.get("TotalRating").isEmpty())
            holder.ratingBar.setRating(0);
        else
            holder.ratingBar.setRating(Float.parseFloat(map.get("TotalRating")));

    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class Myview extends RecyclerView.ViewHolder {
        LinearLayout flash_deal_list;
        ImageView img;
        TextView discount,price,spcial_price,p_name;
        RatingBar ratingBar;

        public Myview(@NonNull View itemView) {
            super(itemView);
            flash_deal_list=itemView.findViewById(R.id.flash_deal_list);
            img=itemView.findViewById(R.id.img);
            p_name=itemView.findViewById(R.id.p_name);
            discount=itemView.findViewById(R.id.discount);
            price=itemView.findViewById(R.id.price);
            spcial_price=itemView.findViewById(R.id.spcial_price);
            ratingBar=itemView.findViewById(R.id.ratingbar);

        }
    }
}


