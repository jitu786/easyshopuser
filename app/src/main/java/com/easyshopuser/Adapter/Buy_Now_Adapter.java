package com.easyshopuser.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.easyshopuser.R;

import java.util.ArrayList;
import java.util.HashMap;

public class Buy_Now_Adapter  extends RecyclerView.Adapter<Buy_Now_Adapter.Myview>
{


    Context context;
    ArrayList<HashMap<String, String>> dataList=new ArrayList<>();
    public Buy_Now_Adapter(Context context, ArrayList<HashMap<String, String>> dataList)
    {
        this.context = context;
        this.dataList=dataList;

    }


    @NonNull
    @Override
    public Buy_Now_Adapter.Myview onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.buy_now_list, viewGroup, false);
        return new Buy_Now_Adapter.Myview(v);

    }

    @Override
    public void onBindViewHolder(@NonNull final Buy_Now_Adapter.Myview holder, int position)
    {
        HashMap<String,String> map=dataList.get(position);
        holder.qty.setText(map.get("quantity"));
        holder.txtorderNo.setText(map.get(""+position+1));
        holder.txtprice.setText("Rs "+map.get("price"));
        //  holder.txtproductname.setText(Html.fromHtml(Utils.html2text("description")));
        holder.txtCategoryname.setText(map.get("name"));

        Glide
                .with(context)
                .load(map.get("image"))
                .placeholder(R.drawable.imgcatlist)
//.error(R.drawable.load)
                .into(holder.image);

    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }


    public class Myview extends RecyclerView.ViewHolder {
        TextView txtorderNo,txtCategoryname,txtproductname,qty,txtprice;
        ImageView image;
        public Myview(@NonNull View itemView) {
            super(itemView);
            txtorderNo=itemView.findViewById(R.id.txtorderNo);
            txtCategoryname=itemView.findViewById(R.id.txtCategoryname);
            txtproductname=itemView.findViewById(R.id.txtproductname);
            qty=itemView.findViewById(R.id.qty);
            txtprice=itemView.findViewById(R.id.txtprice);
            image=itemView.findViewById(R.id.image);
        }
    }
}



