package com.easyshopuser.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.easyshopuser.Adapter.CustomerAddressRecycleAdapter;
import com.easyshopuser.R;
import com.easyshopuser.generalfiles.ExecuteWebServerUrl;
import com.easyshopuser.generalfiles.GeneralFunctions;
import com.easyshopuser.generalfiles.Utils;
import com.easyshopuser.view.GenerateAlertBox;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static com.easyshopuser.generalfiles.Utils.closeLoader;

public class Shipping_Address extends AppCompatActivity implements CustomerAddressRecycleAdapter.OnItemClickListener {

    ImageView sa_back;
    RelativeLayout new_address;
    RecyclerView recycle_address;
    GeneralFunctions generalFunc;
    ArrayList<HashMap<String, String>> dataList = new ArrayList<>();
    CustomerAddressRecycleAdapter adapter;
    View layout_cart_empty;
    LinearLayout nodata;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shipping__address);
        initialize();

        recycle_address=findViewById(R.id.recycle_address);
        recycle_address.setLayoutManager(new LinearLayoutManager(this));
        adapter = new CustomerAddressRecycleAdapter(getActContext(), dataList, generalFunc, false);
        adapter.setOnItemClickListener(this);
        recycle_address.setAdapter(adapter);
        loadCustomerAddresses();

        new_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),Edit_Shipping_Address.class));
            }
        });

        sa_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                overridePendingTransition( R.anim.left_to_right, R.anim.right_to_left );
            }
        });
    }
    public void initialize()
    {


        generalFunc=new GeneralFunctions(this);
        new_address=findViewById(R.id.new_address);
        // edit_address=findViewById(R.id.edit_address);
        sa_back=findViewById(R.id.sa_back);
        layout_cart_empty=findViewById(R.id.layout_cart_empty);
        nodata=findViewById(R.id.nodata);
    }

    private Context getActContext()
    {
        return  this;
    }

    private void loadCustomerAddresses() {
      /*  if (errorView.getVisibility() == View.VISIBLE) {
            errorView.setVisibility(View.GONE);
        }
        if (loading.getVisibility() != View.VISIBLE) {
            loading.setVisibility(View.VISIBLE);
        }
        noAddressArea.setVisibility(View.GONE);
        addAddressBtn.setVisibility(View.GONE);*/
        dataList.clear();
        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("type", "loadCustomerAddress");
        parameters.put("customer_id", generalFunc.getMemberId());

        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(String responseString) {
                Log.d("data",responseString);
                if (responseString != null && !responseString.equals("")) {

                    closeLoader();

                    loadData(responseString);
                } else {
                    adapter.notifyDataSetChanged();
                }
            }
        });
        exeWebServer.execute();
    }

    String registerAddress = "";

    public void loadData(String responseString) {
        if (responseString != null && !responseString.equals("")) {

            boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);
            if (isDataAvail == true) {

                JSONArray msgArr = generalFunc.getJsonArray(Utils.message_str, responseString);
                registerAddress = generalFunc.getJsonValue("registerAddress", responseString);

                if (msgArr != null) {
                    for (int i = 0; i < msgArr.length(); i++) {

                        JSONObject obj_temp = generalFunc.getJsonObject(msgArr, i);

                        HashMap<String, String> map = new HashMap<>();


                        map.put("address_id", generalFunc.getJsonValue("address_id", obj_temp).trim());
                        map.put("customer_id", generalFunc.getJsonValue("customer_id", obj_temp).trim());
                        map.put("firstname", generalFunc.getJsonValue("firstname", obj_temp).trim());
                        map.put("lastname", generalFunc.getJsonValue("lastname", obj_temp).trim());
                        map.put("company", generalFunc.getJsonValue("company", obj_temp).trim());
                        map.put("address_1", generalFunc.getJsonValue("address_1", obj_temp).trim());
                        map.put("address_2", generalFunc.getJsonValue("address_2", obj_temp).trim());
                        map.put("city", generalFunc.getJsonValue("city", obj_temp).trim());
                        map.put("postcode", generalFunc.getJsonValue("postcode", obj_temp).trim());
                        map.put("country_id", generalFunc.getJsonValue("country_id", obj_temp).trim());
                        map.put("zone_id", generalFunc.getJsonValue("zone_id", obj_temp).trim());
                        map.put("custom_field", generalFunc.getJsonValue("custom_field", obj_temp).trim());
                        map.put("region_name", generalFunc.getJsonValue("region_name", obj_temp).trim());
                        map.put("country_name", generalFunc.getJsonValue("country_name", obj_temp).trim());
                        map.put("state_name", generalFunc.getJsonValue("state_name", obj_temp).trim());

                        if (registerAddress.equals(generalFunc.getJsonValue("address_id", obj_temp).trim())) {
                            map.put("registerAddress", "yes");
                        }
                        map.put("city_name", generalFunc.getJsonValue("city_name", obj_temp).trim());


                        dataList.add(map);

                    }
                    Log.d("data_size",dataList.size()+"");


                    //Toast.makeText(this, "Address Id : "+registerAddress, Toast.LENGTH_SHORT).show();
                    adapter.notifyDataSetChanged();
                }
                // addAddressBtn.setVisibility(View.VISIBLE);
            } else {
                adapter.notifyDataSetChanged();

                if (dataList.size()==0)
                {
                    nodata.setVisibility(View.GONE);
                    layout_cart_empty.setVisibility(View.VISIBLE);

                }
                /*  noAddressTxtView.setText(generalFunc.getJsonValue(Utils.message_str, responseString));
                noAddressArea.setVisibility(View.VISIBLE);
                addAddressBtn.setVisibility(View.GONE);*/
            }
        } else {
            //generateErrorView();
        }
    }

    @Override
    public void onItemClickList(View v, int btnType, int position) {


        if(btnType==1)
        {
            removeSelectedAddress(position);
        }
        else if(btnType==2)
        {
            editSelectedAddress(position);
        }
        else if(btnType==3)
        {
            Intent intent = new Intent();
            intent.putExtra("DATA", dataList.get(position));
            setResult(RESULT_OK, intent);
            Shipping_Address.super.onBackPressed();

        }
    }
    public void removeSelectedAddress(final int position) {
        final GenerateAlertBox generateAlert = new GenerateAlertBox(getActContext());
        generateAlert.setCancelable(false);
        generateAlert.setBtnClickList(new GenerateAlertBox.HandleAlertBtnClick() {
            @Override
            public void handleBtnClick(int btn_id) {
                if (btn_id == 1) {
                    generateAlert.closeAlertBox();
                    confirmRemoveAddress(position);
                } else if (btn_id == 0) {
                    generateAlert.closeAlertBox();
                }
            }
        });
        generateAlert.setContentMessage("Confirm", "Do you want to remove the selected address?");
        generateAlert.setPositiveBtn("YES");
        generateAlert.setNegativeBtn("NO");
        generateAlert.showAlertBox();
    }

    public void confirmRemoveAddress(int position) {
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("type", "deleteCustomerAddress");
        parameters.put("customer_id", generalFunc.getMemberId());
        parameters.put("address_id", dataList.get(position).get("address_id"));


        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.setIsDeviceTokenGenerate(true, "vDeviceToken");
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(final String responseString) {

                Utils.printLog("ResponseData", "Data::" + responseString);

                if (responseString != null && !responseString.equals("")) {
                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);

                    if (isDataAvail) {
                        loadCustomerAddresses();
                    } else {
                        generalFunc.showGeneralMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));
                    }

                } else {
                    generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();
    }

    public void editSelectedAddress(int position) {

        Intent int_edit_address = new Intent(Shipping_Address.this, Edit_Shipping_Address.class);
        int_edit_address.putExtra("ADDRESS_DATA", dataList.get(position));

        startActivityForResult(int_edit_address, Utils.ADD_ADDRESS_REQ_CODE);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Utils.ADD_ADDRESS_REQ_CODE && resultCode == RESULT_OK) {
            loadCustomerAddresses();
        }
    }

}

