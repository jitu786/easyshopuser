package com.easyshopuser.Activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.easyshopuser.R;
import com.easyshopuser.generalfiles.ExecuteWebServerUrl;
import com.easyshopuser.generalfiles.GeneralFunctions;
import com.easyshopuser.generalfiles.Utils;
import com.easyshopuser.utils.SharedPref;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.WebDialog;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.HashMap;

public class Login extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    ImageView cancle;
    Button create_account;
    LoginWithGoogle loginWithGoogle;
    GoogleApiClient mGoogleApiClient;
    ImageView google, facebook, twitter;
    CallbackManager mCallbackManager;
    String account_name;
    Button signin;
    public GeneralFunctions generalFunc;
    String DeviceToken = "", email = "";
    EditText email_id, password;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    TextView forgotpassword, show;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);
        FacebookSdk.setApplicationId(getResources().getString(R.string.facebook_app_id));
        FacebookSdk.sdkInitialize(getActContext());
        WebDialog.setWebDialogTheme(R.style.FBDialogtheme);
        DeviceToken = FirebaseInstanceId.getInstance().getToken();
        Log.d("data",DeviceToken);

        generalFunc = new GeneralFunctions(getActContext());
        email_id = findViewById(R.id.email_id);
        password = findViewById(R.id.password);
        signin = findViewById(R.id.signin);
        facebook = findViewById(R.id.facebook);
        mCallbackManager = CallbackManager.Factory.create();
        google = findViewById(R.id.google);
        forgotpassword = findViewById(R.id.forgotpassword);
        show = findViewById(R.id.show);
        configureSignIn();


        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (checkvalidation())
                    userlogin();
            }
        });

        show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showPassword(password, show);
            }
        });
        forgotpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(Login.this, Forget_Password.class);
                startActivity(intent);
            }
        });

        create_account = findViewById(R.id.create_account);
        create_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), Register.class);
                startActivity(intent);
                overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
            }
        });
/* cancle = findViewById(R.id.cancle);
cancle.setOnClickListener(new View.OnClickListener() {
@Override
public void onClick(View view) {

onBackPressed();
overridePendingTransition(R.anim.slide_in_down, R.anim.slide_out_down);
}
});
*/
        facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new LoginWithFacebook(getActContext(), mCallbackManager);
            }
        });


        google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                loginWithGoogle = new LoginWithGoogle(getActContext(), DeviceToken);

            }
        });



    }

    private void showPassword(EditText password, TextView show) {

        if (show.getText().toString().equals("Show")) {
            show.setText("Hide");
            password.setInputType(InputType.TYPE_CLASS_TEXT);
            Typeface type = Typeface.createFromAsset(getAssets(),"font/montserratregular.ttf");
            password.setTypeface(type);
        } else {
            show.setText("Show");
            Typeface type = Typeface.createFromAsset(getAssets(),"font/montserratregular.ttf");

            password.setInputType(InputType.TYPE_CLASS_TEXT |
                    InputType.TYPE_TEXT_VARIATION_PASSWORD);
            password.setTypeface(type);
        }
    }

    private void userlogin() {

        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("type", "signIn");
        parameters.put("email",email_id.getText().toString() );
        parameters.put("password", password.getText().toString());

        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(final String responseString) {
                Utils.printLog("ResponseData", "Data::" + responseString);
                if (responseString != null && !responseString.equals("")) {
                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);
                    if (isDataAvail) {
                        generalFunc.storeUserData(generalFunc.getJsonValue(Utils.message_str, responseString));
                        generalFunc.storedata("last_message_id",generalFunc.getJsonValue("last_message_id",responseString));
                        generalFunc.storeUserData(generalFunc.getJsonValue(Utils.message_str, responseString));
                        String country_id = generalFunc.getJsonValue("country_id", responseString);
                        SharedPref sharedPref=new SharedPref();
                        SharedPref.WriteSharePrefrence(getActContext(),"userCode",generalFunc.getJsonValue("userCode",responseString));
                        Toast.makeText(Login.this, "You are login sucessfully in EASYSHOP", Toast.LENGTH_SHORT).show();

                        Intent intent=new Intent(getActContext(), Dashboard.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        getActContext().startActivity(intent);
                        finish();

/* GenerateAlertBox alertBox= generalFunc.showGeneralMessage("Sucessfully","You are login sucessfully in EASYSHOP" );
alertBox.setBtnClickList(new GenerateAlertBox.HandleAlertBtnClick() {
@Override
public void handleBtnClick(int btn_id) {
if(btn_id==1)
{
Intent intent=new Intent(getActContext(), Dashboard.class);
intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
getActContext().startActivity(intent);
finish();
//finishAffinity();
}
}
});*/
                    }
                    else
                        generalFunc.showGeneralMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));

                } else {
                    generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();
    }
    private boolean checkvalidation() {

        if (email_id.getText().toString().isEmpty()) {
            email_id.setError("Email is Required");
            return false;
        } else if (!email_id.getText().toString().matches(emailPattern)) {
            email_id.setError("Email should be in correct format");
            return false;
        }

        if (password.getText().toString().isEmpty()) {
            password.setError("Password is Required");
            return false;
        }

        return true;
    }




    private void configureSignIn() {

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this).enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Utils.GOOGLE_SIGN_IN_REQ_CODE) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            loginWithGoogle.handleSignInResult(result);
        }

        mCallbackManager.onActivityResult(requestCode, resultCode, data);


    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }




    private Context getActContext() {
        return Login.this;
    }
}