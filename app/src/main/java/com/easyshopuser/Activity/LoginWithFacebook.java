package com.easyshopuser.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;

import com.easyshopuser.generalfiles.ExecuteWebServerUrl;
import com.easyshopuser.generalfiles.GeneralFunctions;
import com.easyshopuser.generalfiles.Utils;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;

public class LoginWithFacebook implements FacebookCallback<LoginResult> {
    Context mContext;



    Activity appLoginAct;
    GeneralFunctions generalFunc;
    CallbackManager mCallbackManager;
    private FirebaseAuth mAuth;

    public LoginWithFacebook(Context mContext, CallbackManager mCallbackManager) {
        this.mContext = mContext;
        this.mCallbackManager = mCallbackManager;

        appLoginAct = (Activity) mContext;
        mAuth = FirebaseAuth.getInstance();
        generalFunc = new GeneralFunctions(mContext);
        initializeFacebookLogin();
    }

    public void initializeFacebookLogin() {

        LoginButton loginButton = new LoginButton(mContext);

// loginButton.setReadPermissions(Arrays.asList("public_profile", "email", "user_friends", "user_about_me"));
        loginButton.setReadPermissions(Arrays.asList("public_profile", "email"));

        loginButton.registerCallback(mCallbackManager, this);
        loginButton.performClick();
    }



    @Override
    public void onSuccess(LoginResult loginResult) {

        Utils.showLoader(mContext);
        GraphRequest request = GraphRequest.newMeRequest(
                loginResult.getAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(
                            JSONObject me,
                            GraphResponse response) {
// Application code
                        Utils.closeLoader();

                        if (response.getError() != null) {
// handle error
                            Utils.printLog("onError", "onError:" + response.getError());
                            generalFunc.showGeneralMessage("", "Please try again later.");

                        } else {

                            String email_str = generalFunc.getJsonValue("email", me.toString());
//String name_str = generalFunc.getJsonValue("name", me.toString());
                            String first_name_str = generalFunc.getJsonValue("first_name", me.toString());
                            String last_name_str = generalFunc.getJsonValue("last_name", me.toString());
                            String fb_id_str = generalFunc.getJsonValue("id", me.toString());

//                            Toast.makeText(mContext, ""+email_str, Toast.LENGTH_SHORT).show();

                            if(email_str.trim().isEmpty()||email_str==null)
                            {
                                Toast.makeText(mContext, "Please add your emailid in facebook account to access the app through the facebook", Toast.LENGTH_SHORT).show();
                            }
                            else
                            {
                                registerUser(first_name_str, last_name_str,"" , "", "", "", email_str, fb_id_str);
                                generalFunc.logOUTFrmFB();
                            }
                        }
                    }


                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,first_name,last_name,email");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void registerUser(final String first_name_str, final String last_name_str, String s, String s1, String s2, String s3, final String email_str, String fb_id_str) {

        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("type", "LoginWithSocialAcc");
        parameters.put("firstName", first_name_str);
        parameters.put("lastName", last_name_str);
        parameters.put("mobile", s);
        parameters.put("country", s1);
        parameters.put("country_id", s2 );
        parameters.put("password", s3);
        parameters.put("email", email_str);
        parameters.put("AppID", fb_id_str);
        parameters.put("loginType", "Facebook");
        parameters.put("fb_token", FirebaseInstanceId.getInstance().getToken());





        Log.w("check",parameters.toString());
        Log.d("check",parameters.toString());
        Utils.printLog("ResponseData",parameters.toString());

        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(mContext, true, generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(final String responseString) {
                Utils.printLog("ResponseData", "Data::" + responseString);
                if (responseString != null && !responseString.equals("")) {
                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);

                    if (isDataAvail) {
                        if(generalFunc.getJsonValue("password",responseString).equals("")){
                            Bundle bundle=new Bundle();
                            bundle.putString("codeVerification","codeVerification");
                            bundle.putString("userId",generalFunc.getJsonValue(Utils.message_str, responseString));
                            bundle.putString("firstName", first_name_str);
                            bundle.putString("lastName",last_name_str);
                            generalFunc.startActivity(Reset_Password.class,bundle);

                        }else {
                            generalFunc.storeUserData(generalFunc.getJsonValue(Utils.message_str, responseString));
                            Toast.makeText(mContext, "You are login sucessfully.", Toast.LENGTH_SHORT).show();
                            Intent intent=new Intent(mContext,Dashboard.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            mContext.startActivity(intent);
                        }
                        ActivityCompat.finishAffinity((Activity) mContext);

                    } else {
                        generalFunc.showGeneralMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));
                        Toast.makeText(mContext, "Data not available..", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    generalFunc.showError();
                    Toast.makeText(mContext, "Response misssing..", Toast.LENGTH_SHORT).show();
                }
            }
        });
        exeWebServer.execute();
    }


    @Override
    public void onCancel() {

    }

    @Override
    public void onError(FacebookException error) {
        Log.d("error",error.toString());
        Toast.makeText(mContext, "facebook error..", Toast.LENGTH_SHORT).show();
    }
}
