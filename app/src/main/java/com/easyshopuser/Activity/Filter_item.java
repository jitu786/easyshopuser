package com.easyshopuser.Activity;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.easyshopuser.R;
import com.easyshopuser.generalfiles.GeneralFunctions;
import com.easyshopuser.helper.StartActProcess;
import com.easyshopuser.utils.SharedPref;

import org.florescu.android.rangeseekbar.RangeSeekBar;

public class Filter_item  extends AppCompatActivity implements View.OnClickListener{


    Spinner colorbox,sizebox;
    LinearLayout price_layout;
    String[] COLOURLIST = {"SELECT","BLUE", "GREEN", "PINK", "RED", "YELLOW", "WHITE", "BLACK"};
    String[] SIZELIST = {"SELECT","S", "M", "L", "XX"};
    Integer c_pos = -1,s_pos=-1;
    TextView min_txt,max_txt;
    String max="2000",min="0";
    GeneralFunctions generalFunc;
    String response="";
    TextView btn_apply;
    TextView pd_back;
    int gmin,gmax;
    TextView clear;
    ImageView cancle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);

        generalFunc=new GeneralFunctions(Filter_item.this);
        min_txt=findViewById(R.id.min_txt);
        max_txt=findViewById(R.id.max_txt);
        btn_apply=findViewById(R.id.btn_apply);
        price_layout=findViewById(R.id.price_layout);
        clear=findViewById(R.id.po_title);
        response= SharedPref.ReadSharePrefrence(Filter_item.this,"userdata");
        pd_back=findViewById(R.id.pd_back);

        colorbox=findViewById(R.id.colorbox);
        sizebox=findViewById(R.id.sizebox);
        cancle=findViewById(R.id.cancle);

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, COLOURLIST);
        final ArrayAdapter<String> arrayAdapter2 = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, SIZELIST);



        cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        colorbox.setAdapter(arrayAdapter);
        sizebox.setAdapter(arrayAdapter2);

        arrayAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        arrayAdapter2.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);

        colorbox.setAdapter(arrayAdapter);
        sizebox.setAdapter(arrayAdapter2);

        colorbox.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                c_pos=position;

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sizebox.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                s_pos=position;
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        RangeSeekBar<Integer> rangeSeekBar = new RangeSeekBar<>(this);

        btn_apply.setOnClickListener(this);
        clear.setOnClickListener(this);


        if (getIntent().getStringExtra("count").equals("1"))
        {
            price_layout.setVisibility(View.VISIBLE);

            min_txt.setText(getIntent().getStringExtra("minprice"));
            max_txt.setText(getIntent().getStringExtra("maxprice"));
            gmin= Integer.parseInt(getIntent().getStringExtra("minprice"));
            gmax= Integer.parseInt(getIntent().getStringExtra("maxprice"));
            Log.e("sf(getIntent)",getIntent().getStringExtra("maxprice")+""+getIntent().getStringExtra("minprice"));
//rangeSeekBar.setRangeValues(gmin,gmax);
            min= String.valueOf(gmin);
            max= String.valueOf(gmax);

            rangeSeekBar.setRangeValues(gmin, gmax);
            rangeSeekBar.setSelectedMinValue(gmin);
            rangeSeekBar.setSelectedMaxValue(gmax);

/*
rangeSeekBar.setSelectedMaxValue(Integer.valueOf(max));
rangeSeekBar.setSelectedMinValue(Integer.valueOf(min));
*/
        }
        else
        {


            price_layout.setVisibility(View.GONE);
        }


        RangeSeekBar rangeSeekBarTextColorWithCode = (RangeSeekBar) findViewById(R.id.rangeSeekBarTextColorWithCode);

        rangeSeekBarTextColorWithCode.setTextAboveThumbsColorResource(android.R.color.holo_blue_bright);

        rangeSeekBarTextColorWithCode.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener() {
            @Override
            public void onRangeSeekBarValuesChanged(RangeSeekBar bar, Object minValue, Object maxValue) {
                max=String.valueOf(maxValue);
                min=String.valueOf(minValue);


            }
        });



    }

    @Override
    public void onClick(View view) {

        if(view.getId()==btn_apply.getId())
        {
            Bundle bn = new Bundle();
            bn.putString("minprice",min);
            if(max.equals("2000")&&min.equals("0"))
                bn.putString("maxprice","0");
            else
                bn.putString("maxprice",max);
            bn.putString("count","1");
            if(c_pos!=0)
                bn.putString("color",COLOURLIST[c_pos]);
            if(s_pos!=0)
                bn.putString("size",SIZELIST[s_pos]);
            new StartActProcess(getActContext()).setOkResult(bn);
            finish();
        }
        else if (view.getId()==clear.getId())
        {
            Bundle bn=new Bundle();
            bn.putString("minprice","");
            bn.putString("maxprice","");
            bn.putString("count","0");

            Log.e("date",bn.toString());
            new StartActProcess(getActContext()).setOkResult(bn);
            finish();
        }
    }

    public Context getActContext() {
        return Filter_item.this;
    }
}
