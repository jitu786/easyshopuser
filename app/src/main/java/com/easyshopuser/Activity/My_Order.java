package com.easyshopuser.Activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.easyshopuser.Adapter.Order_Adapter;
import com.easyshopuser.R;
import com.easyshopuser.generalfiles.ExecuteWebServerUrl;
import com.easyshopuser.generalfiles.GeneralFunctions;
import com.easyshopuser.generalfiles.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static com.easyshopuser.generalfiles.Utils.closeLoader;
import static com.easyshopuser.generalfiles.Utils.firebaseToken;

public class My_Order extends AppCompatActivity implements Order_Adapter.OnItemClickListener {

    RecyclerView order_list;
    Order_Adapter adapter;
    ImageView pd_back;
    TextView po_title;
    ArrayList<HashMap<String, String>> dataList = new ArrayList<>();
    GeneralFunctions generalFunc;
    View empty_state;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my__order);

        initialize();
        pd_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        @SuppressLint("WrongConstant") LinearLayoutManager layoutManager = new LinearLayoutManager(My_Order.this, LinearLayoutManager.VERTICAL, false);
        order_list.setLayoutManager(layoutManager);
        adapter = new Order_Adapter(this,dataList);
        adapter.setOnItemClickListener(this);
        order_list.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        loadCustomerOrders();
    }
    public void initialize()
    {
        generalFunc=new GeneralFunctions(this);
        empty_state=findViewById(R.id.empty_state);
        po_title=findViewById(R.id.po_title);
        pd_back=findViewById(R.id.pd_back);

        if(getIntent().getStringExtra("status")!=null) {
            po_title.setText("Recently Purchased Items");
        }
        else
            po_title.setText("My Order");
        order_list=findViewById(R.id.order_list);

    }
    private void loadCustomerOrders() {
        dataList.clear();
        adapter.notifyDataSetChanged();


        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("type", "getUserOrders");
        parameters.put("customer_id", generalFunc.getMemberId());
        if(getIntent().getStringExtra("status")!=null) {
            parameters.put("orderStatus", getIntent().getStringExtra("status"));
            parameters.put("limit","5");
        }
        else {
            parameters.put("orderStatus", "");
            parameters.put("limit","");
        }
        parameters.put("customer", "");


        Log.d("orderData",parameters.toString().replace(",","&"));
        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(String responseString) {
                if (responseString != null && !responseString.equals("")) {
                    closeLoader();
                    loadData(responseString);
                } else {
// generateErrorView();
                }
            }
        });
        exeWebServer.execute();
    }
    public void loadData(String responseString) {
        if (responseString != null && !responseString.equals("")) {

            boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);

            if (isDataAvail == true) {

                JSONArray msgArr = generalFunc.getJsonArray(Utils.message_str, responseString);

                if (msgArr != null) {
                    for (int i = 0; i < msgArr.length(); i++) {

                        JSONObject obj_temp = generalFunc.getJsonObject(msgArr, i);

                        HashMap<String, String> map = new HashMap<>();
                        map.put("order_id", generalFunc.getJsonValue("order_id", obj_temp).trim());
                        map.put("order_product_id", generalFunc.getJsonValue("order_product_id", obj_temp).trim());
                        map.put("orderStatus", generalFunc.getJsonValue("orderStatus", obj_temp).trim());
                        map.put("order_status_id", generalFunc.getJsonValue("order_status_id", obj_temp).trim());
                        map.put("product_id", generalFunc.getJsonValue("product_id", obj_temp).trim());
                        map.put("productName", generalFunc.getJsonValue("productName", obj_temp).trim());
                        map.put("productModel", generalFunc.getJsonValue("productModel", obj_temp).trim());
                        map.put("productQuantity", generalFunc.getJsonValue("productQuantity", obj_temp).trim());
                        map.put("productPrice", generalFunc.getJsonValue("productPrice", obj_temp).trim());
                        map.put("totalPrice", generalFunc.getJsonValue("totalPrice", obj_temp).trim());
                        map.put("total", generalFunc.getJsonValue("total", obj_temp).trim());
                        map.put("image", generalFunc.getJsonValue("image", obj_temp).trim());
                        map.put("option", generalFunc.getJsonValue("option", obj_temp).trim());
                        map.put("date_added", generalFunc.getJsonValue("date_added", obj_temp).trim());
                        map.put("payment_code", generalFunc.getJsonValue("payment_code", obj_temp).trim());
                        map.put("product_option_name_color", generalFunc.getJsonValue("product_option_name_color", obj_temp).trim());
                        map.put("product_option_value_color", generalFunc.getJsonValue("product_option_value_color", obj_temp).trim());
                        map.put("product_option_name_size", generalFunc.getJsonValue("product_option_name_size", obj_temp).trim());
                        map.put("product_option_value_size", generalFunc.getJsonValue("product_option_value_size", obj_temp).trim());
                        map.put("order_status_id", generalFunc.getJsonValue("order_status_id", obj_temp).trim());
                        map.put("firstname", generalFunc.getJsonValue("firstname", obj_temp).trim());
                        map.put("lastname", generalFunc.getJsonValue("lastname", obj_temp).trim());
                        map.put("email", generalFunc.getJsonValue("email", obj_temp).trim());
                        map.put("telephone", generalFunc.getJsonValue("telephone", obj_temp).trim());
                        map.put("shipping_address_1", generalFunc.getJsonValue("shipping_address_1", obj_temp).trim());
                        if(getIntent().getStringExtra("status")!=null)
                        {
                            if(generalFunc.getJsonValue("orderStatus", obj_temp).trim().equals("Complete"))
                            {
                                dataList.add(map);
                            }
                        }
                        else
                            dataList.add(map);
                    }
                    adapter.notifyDataSetChanged();
                }
            } else {
                if(dataList.size()==0)
                {
                    adapter.notifyDataSetChanged();
                    order_list.setVisibility(View.GONE);
                    empty_state.setVisibility(View.VISIBLE);
                }
/*noOrdersTxtView.setText(generalFunc.getJsonValue(Utils.message_str, responseString));
noOrdersTxtView.setVisibility(View.VISIBLE);*/
            }
        } else {
//generateErrorView();
        }
    }

    @Override
    public void onItemClickList(View v, int position, int btnType, int qty) {
        if(btnType==1)
        {
            Intent int_edit_address = new Intent(My_Order.this, My_Order_Detail.class);
            int_edit_address.putExtra("oderDATA", dataList.get(position));
            startActivity(int_edit_address);

        }
    }

    @Override
    public void onBackPressed() {

        Intent intent=new Intent(My_Order.this,Dashboard.class);
        startActivity(intent);
        finish();
    }
}
