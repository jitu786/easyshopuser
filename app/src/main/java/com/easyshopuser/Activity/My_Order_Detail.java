package com.easyshopuser.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.easyshopuser.R;
import com.easyshopuser.generalfiles.ExecuteWebServerUrl;
import com.easyshopuser.generalfiles.GeneralFunctions;
import com.easyshopuser.generalfiles.Utils;
import com.easyshopuser.view.GenerateAlertBox;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class My_Order_Detail extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    TextView po_title,txtreturnorder,txtaddress,txtemail,txtmobile,txtname,txt_add_his;
    ImageView pd_back;
    LinearLayout view_seller_transaction;
    HashMap<String, String> OrderData = null;
    ImageView product_image;
    Spinner spinner_o_history;
    CheckBox check_notify_customer;
    GeneralFunctions generalFunc;
    String order_status_id;
    int position=0;
    EditText edtComment;
    ArrayList<HashMap<String, String>> name=new ArrayList<>();
    ArrayAdapter aa;
    ArrayList<HashMap<String, String>> dataList = new ArrayList<>();
    ArrayList<String> dataList_name = new ArrayList<>();
    String flag="";
    TextView txtproductname,txtprice,txtorderid,txtorderdate,txtprice1,txtshiping_rate,txtgrandtotal;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my__order__detail);

        initialize();
        pd_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });



    }

    @Override
    public void onBackPressed() {
        if(getIntent().getStringExtra("flag")!=null)
        {
            Intent intent=new Intent(My_Order_Detail.this,ViewProductTransactionActivity.class);
            startActivity(intent);
            finish();
        }
        else
        {
            super.onBackPressed();
        }

    }

    public void initialize()
    {
        view_seller_transaction=findViewById(R.id.view_seller_transaction);
        edtComment=findViewById(R.id.edtComment);
        txt_add_his=findViewById(R.id.txt_add_his);
        check_notify_customer=findViewById(R.id.check_notify_customer);
        generalFunc=new GeneralFunctions(this);
        spinner_o_history=findViewById(R.id.spinner_o_history);
        spinner_o_history.setOnItemSelectedListener(this);
//Creating the ArrayAdapter instance having the country list
        aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item,dataList_name);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//Setting the ArrayAdapter data on the Spinner
        spinner_o_history.setAdapter(aa);
        txtaddress=findViewById(R.id.txtaddress);
        txtemail=findViewById(R.id.txtemail);
        txtmobile=findViewById(R.id.txtmobile);
        txtname=findViewById(R.id.txtname);

        OrderData = (HashMap<String, String>) getIntent().getSerializableExtra("oderDATA");
        po_title=findViewById(R.id.po_title);
        pd_back=findViewById(R.id.pd_back);
        po_title.setText("Order Detail");
        txtreturnorder=findViewById(R.id.txtreturnorder);
        product_image=findViewById(R.id.product_image);
        txtproductname=findViewById(R.id.txtproductname);
        txtprice=findViewById(R.id.txtprice);
        txtorderid=findViewById(R.id.txtorderid);
        txtorderdate=findViewById(R.id.txtorderdate);
        txtprice1=findViewById(R.id.txtprice1);
        txtshiping_rate=findViewById(R.id.txtshiping_rate);
        txtgrandtotal=findViewById(R.id.txtgrandtotal);

        if(getIntent().getStringExtra("flag")!=null)
        {
            txtreturnorder.setVisibility(View.GONE);
            view_seller_transaction.setVisibility(View.VISIBLE);
            getOrderStatus();
        }

        if(OrderData!=null)
        {
            txtorderdate.setText(OrderData.get("date_added"));
            txtorderid.setText(OrderData.get("order_id"));
            txtprice1.setText("Rs "+OrderData.get("totalPrice"));
            float value=Float.parseFloat(OrderData.get("totalPrice").replace(",",""))+5;
            txtprice.setText("Rs "+OrderData.get("totalPrice"));
            txtgrandtotal.setText("Rs "+value+"");

            txtproductname.setText(OrderData.get("productName"));
            txtshiping_rate.setText("5");

            Glide.with(this)
                    .load(OrderData.get("image"))
                    .placeholder(R.drawable.watch)
                    .into(product_image);

            txtreturnorder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(OrderData!=null) {


                        if (flag.equals("7"))
                        {
                            cancelorder();
                        }
                        else {
                            if (!OrderData.get("orderStatus").equals("Return")) {
                                Intent int_edit_address = new Intent(My_Order_Detail.this, ReturnOrderActivity.class);
                                int_edit_address.putExtra("oderDATA", OrderData);
                                startActivity(int_edit_address);
                            }
                        }
                    }
                }
            });

            if (OrderData.get("orderStatus").equals("Processing") || OrderData.get("orderStatus").equals("Shipped") || OrderData.get("orderStatus").equals("Pending"))
            {
                flag="7";
                txtreturnorder.setText("Request for the order cancellation");
                txtreturnorder.setVisibility(View.VISIBLE);
            }

            if(OrderData.get("orderStatus").equals("Complete"))
            {

                txtreturnorder.setVisibility(View.VISIBLE);

            }
            txtaddress.setText(OrderData.get("shipping_address_1"));
            txtname.setText(OrderData.get("firstname")+" "+OrderData.get("lastname"));
            txtemail.setText(OrderData.get("email"));
            txtmobile.setText(OrderData.get("telephone"));

            txt_add_his.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    changeStatus();
                }
            });
        }


    }

    private void cancelorder() {

        final GenerateAlertBox generateAlert = new GenerateAlertBox(My_Order_Detail.this);
        generateAlert.setCancelable(false);
        generateAlert.setBtnClickList(new GenerateAlertBox.HandleAlertBtnClick() {
            @Override
            public void handleBtnClick(int btn_id) {
                if (btn_id == 1) {
                    generateAlert.closeAlertBox();
                    order_status_id="7";
                    changeStatus();
                    Toast.makeText(My_Order_Detail.this, "Your order is cancelled sucessfully.", Toast.LENGTH_SHORT).show();

                } else if (btn_id == 0) {
                    generateAlert.closeAlertBox();
                }
            }
        });
        generateAlert.setContentMessage("Confirm", "Do you want to cancel the order?");
        generateAlert.setPositiveBtn("YES");
        generateAlert.setNegativeBtn("NO");
        generateAlert.showAlertBox();

    }

    public void getOrderStatus()
    {
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("type", "getOrderStatus");

        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(My_Order_Detail.this, true, generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(final String responseString) {
                Utils.printLog("ResponseData", "Data::" + responseString);
                if (responseString != null && !responseString.equals("")) {
                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);
                    if (isDataAvail)
                    {
                        JSONArray msgArr = generalFunc.getJsonArray(Utils.message_str, responseString);
                        dataList.clear();
                        if (msgArr != null)
                        {
                            for (int i = 0; i < msgArr.length(); i++) {
                                JSONObject obj_service = generalFunc.getJsonObject(msgArr, i);
                                HashMap<String, String> dataMap_products = new HashMap<>();
                                if(OrderData.get("orderStatus").equals(generalFunc.getJsonValue("name", String.valueOf(obj_service))))
                                {
                                    order_status_id=generalFunc.getJsonValue("order_status_id", String.valueOf(obj_service));
                                    position=i;
                                }
                                dataMap_products.put("order_status_id",generalFunc.getJsonValue("order_status_id", String.valueOf(obj_service)));
                                dataMap_products.put("language_id",generalFunc.getJsonValue("language_id", String.valueOf(obj_service)));
                                dataMap_products.put("name",generalFunc.getJsonValue("name", String.valueOf(obj_service)));

                                dataList_name.add(generalFunc.getJsonValue("name", String.valueOf(obj_service)));
                                dataList.add(dataMap_products);
                            }

                            aa.notifyDataSetChanged();
                            spinner_o_history.setSelection(position);

                        }
                    }
                    else
                        generalFunc.showGeneralMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));
                } else
                {
                    generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();
    }public void changeStatus()
    {
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("type", "changeStatusOfOrder");
        parameters.put("order_id", OrderData.get("order_id"));
        parameters.put("order_status_id", order_status_id);
        parameters.put("product_id", OrderData.get("product_id"));
        parameters.put("productName", OrderData.get("productName"));
        parameters.put("seller_id", generalFunc.getMemberId());
        parameters.put("comment", edtComment.getText().toString());
        parameters.put("isSeller", "1");

        Log.e("data",parameters.toString());
        if(check_notify_customer.isChecked())
            parameters.put("customer_notify", "1");
        else
            parameters.put("customer_notify", "0");


        Log.d("data",parameters.toString());
        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(My_Order_Detail.this, true, generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(final String responseString) {
                Utils.printLog("ResponseData", "Data::" + responseString);
                if (responseString != null && !responseString.equals("")) {
                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);
                    if (isDataAvail)
                    {
                        GenerateAlertBox alertBox= generalFunc.showGeneralMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));
                        alertBox.setBtnClickList(new GenerateAlertBox.HandleAlertBtnClick() {
                            @Override
                            public void handleBtnClick(int btn_id) {
                                if(btn_id==1)
                                {
                                    Intent intent=new Intent(My_Order_Detail.this,My_Order.class);
                                    startActivity(intent);
                                    finish();
                                }
                            }
                        });
                    }
                    else
                        generalFunc.showGeneralMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));
                } else
                {
                    generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
    {
        order_status_id=dataList.get(i).get("order_status_id");
        //Toast.makeText(this, ""+dataList.get(i).get("order_status_id"), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
