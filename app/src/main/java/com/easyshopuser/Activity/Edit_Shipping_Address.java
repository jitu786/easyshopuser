package com.easyshopuser.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.easyshopuser.R;
import com.easyshopuser.generalfiles.ExecuteWebServerUrl;
import com.easyshopuser.generalfiles.GeneralFunctions;
import com.easyshopuser.generalfiles.Utils;
import com.easyshopuser.helper.SelectCountryActivity;
import com.easyshopuser.helper.SelectStateActivity;
import com.easyshopuser.helper.StartActProcess;

import java.util.HashMap;

public class Edit_Shipping_Address extends AppCompatActivity {

    ImageView cancle;
    LinearLayout country_lyt,state;
    EditText txtlastname,txtname,txtaddress2,txtaddress1,txtcity,txtpincode;
    TextView txtstatname,txtcountryname,txtsubmit;
    String iCountryId = "99";
    String zone_id = "";
    String region_id = "0";
    String cid = "";
    GeneralFunctions generalFunc;
    boolean isRegisterUser=false;
    boolean isSeller=false;
    boolean isCountrySelected = false;
    boolean isStateSelected = false;
    boolean isCity1Selected = false;
    HashMap<String, String> ADDRESS_DATA = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit__shipping__address);

        initialize();
    }
    public void initialize()
    {
        ADDRESS_DATA = (HashMap<String, String>) getIntent().getSerializableExtra("ADDRESS_DATA");
        generalFunc=new GeneralFunctions(this);
        txtsubmit=findViewById(R.id.submit);
        txtcountryname=findViewById(R.id.txtcountryname);
        txtlastname=findViewById(R.id.txtlastname);
        txtname=findViewById(R.id.txtname);
        txtaddress2=findViewById(R.id.txtaddress2);
        txtcity=findViewById(R.id.txtcity);
        txtaddress1=findViewById(R.id.txtaddress1);
        txtpincode=findViewById(R.id.txtpincode);
        country_lyt=findViewById(R.id.country);
        state=findViewById(R.id.state);
        cancle=findViewById(R.id.cancle);
        txtstatname=findViewById(R.id.txtstatname);

        txtcountryname.setText("India");
        if(ADDRESS_DATA!=null)
        {
            txtcountryname.setText(ADDRESS_DATA.get("country_name"));
            txtstatname.setText(ADDRESS_DATA.get("state_name"));
            txtlastname.setText(ADDRESS_DATA.get("lastname"));
            txtname.setText(ADDRESS_DATA.get("firstname"));
            txtaddress1.setText(ADDRESS_DATA.get("address_1"));
            txtaddress2.setText(ADDRESS_DATA.get("address_2"));
            txtpincode.setText(ADDRESS_DATA.get("postcode"));
            iCountryId=ADDRESS_DATA.get("country_id");
            zone_id=ADDRESS_DATA.get("zone_id");
            txtcity.setText(ADDRESS_DATA.get("city"));

        }

        cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        /*country_lyt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new StartActProcess(getActContext()).startActForResult(SelectCountryActivity.class, Utils.SELECT_COUNTRY_REQ_CODE);
            }
        });*/

        state.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!iCountryId.equals("0")) {
                    Bundle bundleState = new Bundle();
                    bundleState.putString("iCountryId", iCountryId);
                    bundleState.putString("type", "stateList");
                    new StartActProcess(getActContext()).startActForResult(SelectStateActivity.class, bundleState, Utils.SELECT_STATE_REQ_CODE);
                }
                else
                    Toast.makeText(Edit_Shipping_Address.this, "Select country.", Toast.LENGTH_SHORT).show();
            }
        });

        txtsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(checkValidation())
                    addCustomerAddress();
            }
        });

    }

    private boolean checkValidation()
    {

        if(txtname.getText().toString().trim().isEmpty())
        {
            txtname.setError("Firstname is required.");
            return false;
        }
        else if(txtlastname.getText().toString().trim().isEmpty())
        {
            txtlastname.setError("Lastname is required.");
            return false;
        }
        else if(txtaddress1.getText().toString().isEmpty())
        {
            txtaddress1.setError("Address is required.");
            return false;
        }
        else if(txtcountryname.getText().toString().isEmpty())
        {
            Toast.makeText(this, "Please  Select Country.", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(txtstatname.getText().toString().trim().isEmpty())
        {
            Toast.makeText(this, "Please Select State.", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(txtcity.getText().toString().trim().isEmpty())
        {
            txtcity.setError("City is required.");
            return false;
        }
        else if(txtpincode.getText().toString().trim().isEmpty())
        {
            txtpincode.setError("Pincode is required.");
            return false;
        }
        return true;
    }

    private Context getActContext() {
        return this;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Utils.SELECT_COUNTRY_REQ_CODE && resultCode == RESULT_OK && data != null) {
// vCountryCode = data.getStringExtra("vCountryCode");
// vPhoneCode = data.getStringExtra("vPhoneCode");
            if (!iCountryId.equals(data.getStringExtra("iCountryId"))) {
                iCountryId = data.getStringExtra("iCountryId");

                zone_id = "";
                isStateSelected = false; //State is region
                isCity1Selected = false;

                String vCountry = data.getStringExtra("vCountry");
                isCountrySelected = true;
                cid = "";
                txtcountryname.setText(vCountry);

            }

        }if (requestCode == Utils.SELECT_STATE_REQ_CODE && resultCode == RESULT_OK && data != null) {
// vCountryCode = data.getStringExtra("vCountryCode");
// vPhoneCode = data.getStringExtra("vPhoneCode");
            zone_id = data.getStringExtra("zone_id");
            String vState = data.getStringExtra("zone_name");
            isStateSelected = true;
            isCity1Selected = true;

            txtstatname.setText(vState);

        }
/*if (requestCode == Utils.SELECT_REGION_REQ_CODE && resultCode == RESULT_OK && data != null) {
// vCountryCode = data.getStringExtra("vCountryCode");
// vPhoneCode = data.getStringExtra("vPhoneCode");
region_id = data.getStringExtra("region_id");
String vState = data.getStringExtra("zone_name");
isStateSelected = true;
isCity1Selected = true;

//cid="";

//makeCheckpointList();

regionBox.setText(vState);
// cityBox1.setText(vState);

}*/
    }
    public void addCustomerAddress() {
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("type", "addCustomerAddress");
        parameters.put("customer_id", generalFunc.getMemberId());
        parameters.put("firstname", txtname.getText().toString());
        parameters.put("lastname", Utils.getText(txtlastname));
        parameters.put("company", "");
        parameters.put("address_1", Utils.getText(txtaddress1));
        parameters.put("address_2", Utils.getText(txtaddress2));
// parameters.put("city", Utils.getText(cityBox));
        parameters.put("postcode", Utils.getText(txtpincode));
// parameters.put("mobile", Utils.getText(mobileBox));
        parameters.put("country", txtcountryname.getText().toString());
        parameters.put("state", txtstatname.getText().toString());
        parameters.put("city", txtcity.getText().toString());


        parameters.put("country_id", iCountryId);
        parameters.put("zone_id", zone_id);
        parameters.put("registerUser", "yes");


        if (ADDRESS_DATA != null) {
            parameters.put("address_id", ADDRESS_DATA.get("address_id"));
        }

        Utils.printLog("ResponseData", "Data::" + parameters);

        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.setIsDeviceTokenGenerate(true, "vDeviceToken");
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(final String responseString) {

                Utils.printLog("ResponseData", "Data::" + responseString);

                if (responseString != null && !responseString.equals("")) {
                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);

                    if (isDataAvail) {
//Utils.countryToDisplay = Utils.getCountryName(iCountryId);
                        // generalFunc.showGeneralMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));
                        // finish();

                        Toast.makeText(Edit_Shipping_Address.this, ""+generalFunc.getJsonValue(Utils.message_str, responseString), Toast.LENGTH_SHORT).show();

                        Intent intent=new Intent(Edit_Shipping_Address.this,Shipping_Address.class);
                        startActivity(intent);


                    } else {
                        generalFunc.showGeneralMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));
                    }

                } else {
                    generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();
    }


}
