package com.easyshopuser.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.easyshopuser.R;
import com.easyshopuser.generalfiles.ExecuteWebServerUrl;
import com.easyshopuser.generalfiles.GeneralFunctions;
import com.easyshopuser.generalfiles.Utils;
import com.easyshopuser.view.GenerateAlertBox;
import com.stripe.model.Fee;

import java.util.ArrayList;
import java.util.HashMap;

public class Feedback extends AppCompatActivity {

    EditText email_id,message;
    ImageView pd_back;
    TextView po_title;
    GeneralFunctions generalFunc;
    Button submit;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        initialize();

        pd_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed(); }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkvalidation())
                {
                    submitfeedback();
                }


            }
        });


    }

    private void submitfeedback() {

        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("type", "addFeedback");
        parameters.put("customer_id", generalFunc.getMemberId());
        parameters.put("admin_id", "3");
        parameters.put("email", email_id.getText().toString());
        parameters.put("feedback_message",message.getText().toString());


        Log.d("abc",parameters.toString());
        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(final String responseString) {

                Utils.printLog("ResponseData", "Data::" + responseString);

                if (responseString != null && !responseString.equals("")) {
                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);

                    if (isDataAvail) {
                        Toast.makeText(Feedback.this, ""+generalFunc.getJsonValue(Utils.message_str, responseString), Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent(Feedback.this,Dashboard.class);
                        startActivity(intent);
                        finish();

                    } else {
                        Toast.makeText(Feedback.this, ""+generalFunc.getJsonValue(Utils.message_str, responseString), Toast.LENGTH_SHORT).show();
                        //generalFunc.showGeneralMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));
                    }

                } else {
                    generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();

    }

    private Context getActContext() {

        return Feedback.this;
    }

    private boolean checkvalidation() {

        if (email_id.getText().toString().isEmpty())
        {
            email_id.setError("Email-id is required.");
            return  false;
        }
        else if (!email_id.getText().toString().matches(emailPattern)) {
            email_id.setError("Email should be in correct format");
            return false;
        }
        if (message.getText().toString().isEmpty())
        {
            message.setError("Feedback is required.");
            return  false;
        }


        return  true;
    }

    private void initialize() {

        generalFunc=new GeneralFunctions(Feedback.this);
        pd_back=findViewById(R.id.pd_back);
        po_title=findViewById(R.id.po_title);
        po_title.setText("FeedBack");
        email_id=findViewById(R.id.email_id);
        message=findViewById(R.id.message);
        submit=findViewById(R.id.submit);



    }
}

