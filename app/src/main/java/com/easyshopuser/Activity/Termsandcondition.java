package com.easyshopuser.Activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.easyshopuser.R;

public class Termsandcondition extends AppCompatActivity {


    ImageView pd_back;
    TextView po_title;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_conditons);

        pd_back=findViewById(R.id.pd_back);
        po_title=findViewById(R.id.po_title);

        po_title.setText("Terms And Conditions");

        pd_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
                finish();
            }
        });
    }

}
