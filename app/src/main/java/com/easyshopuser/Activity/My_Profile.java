package com.easyshopuser.Activity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.easyshopuser.R;
import com.easyshopuser.generalfiles.ExecuteWebServerUrl;
import com.easyshopuser.generalfiles.GeneralFunctions;
import com.easyshopuser.generalfiles.Utils;
import com.easyshopuser.view.GenerateAlertBox;

import org.json.JSONObject;

import java.util.HashMap;

public class My_Profile  extends AppCompatActivity {

    ImageView cancle;
    TextView edt_address_title, submit;
    GeneralFunctions generalFunc;
    EditText firstname, lastname, mobileno, email;
    String response = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my__profile);
        initialize();


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (chekvalidation()) {

                    updatedata();
                }
            }
        });
        cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private boolean chekvalidation() {
        if (firstname.getText().toString().isEmpty()) {
            firstname.setError("First name is Required");
            return false;
        }
        if (lastname.getText().toString().isEmpty()) {
            lastname.setError("Last name is Required");
            return false;
        }
        if (mobileno.getText().toString().isEmpty()) {
            mobileno.setError("Mobile No. is Required");
            return false;
        }
        else  if (mobileno.getText().toString().trim().length()!=10)
        {
            mobileno.setError("Phone number should be 10 digit.");
            return false;
        }
        return true;
    }

    private Context getActContext() {

        return My_Profile.this;
    }

    public void initialize()
    {

        submit=findViewById(R.id.submit);
        cancle=findViewById(R.id.cancle);
        edt_address_title=findViewById(R.id.edt_address_title);
        edt_address_title.setText("My Profile");
        generalFunc=new GeneralFunctions(My_Profile.this);
        firstname=findViewById(R.id.firstname);
        lastname=findViewById(R.id.lastname);
        mobileno=findViewById(R.id.mobileno);
        email=findViewById(R.id.email);
        getUserInfo();

//Toast.makeText(this, ""+generalFunc.getMemberId(), Toast.LENGTH_SHORT).show();
    }
    private void getUserInfo() {

        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("type", "getUserInfo");
        parameters.put("customer_id", generalFunc.getMemberId());

        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(String responseString) {

                if (responseString != null && !responseString.equals("")) {

                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);
                    if (isDataAvail == true) {
                        JSONObject obj_msg = generalFunc.getJsonObject(Utils.message_str, responseString);
                        firstname.setText(generalFunc.getJsonValue("firstname",obj_msg));
                        lastname.setText(generalFunc.getJsonValue("lastname",obj_msg));
                        email.setText(generalFunc.getJsonValue("email",obj_msg));
                        mobileno.setText(generalFunc.getJsonValue("telephone",obj_msg));
                    }
                }
            }
        });
        exeWebServer.execute();
    }


    private void updatedata() {

        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("type", "updateProfileInformation");
        parameters.put("customer_id", generalFunc.getMemberId());
        parameters.put("firstName", Utils.getText(firstname));
        parameters.put("lastName", Utils.getText(lastname));
        parameters.put("telephone", Utils.getText(mobileno));
        parameters.put("country", "");
        parameters.put("country_id", "");
        parameters.put("email", Utils.getText(email));
        parameters.put("customer_group_id", "");


        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(final String responseString) {

                Utils.printLog("ResponseData", "Data::" + responseString);

                if (responseString != null && !responseString.equals("")) {
                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);

                    if (isDataAvail) {

                        generalFunc.storedata("COUNTRY_ID", "");
                        //generalFunc.setCountryName(Utils.getCountryName(""));

                        final GenerateAlertBox generateAlert = new GenerateAlertBox(getActContext());
                        Toast.makeText(My_Profile.this, "Your profile is updated sucessfully", Toast.LENGTH_SHORT).show();
                        //   Toast.makeText(My_Profile.this, ""+generalFunc.getJsonValue(Utils.message_str, responseString), Toast.LENGTH_SHORT).show();
                        finish();
                        /*generateAlert.setCancelable(false);
                        generateAlert.setBtnClickList(new GenerateAlertBox.HandleAlertBtnClick() {
                            @Override
                            public void handleBtnClick(int btn_id) {
                                if (btn_id == 1) {
                                    finish();
//backImgView.performClick();
                                }
                            }
                        });
                        generateAlert.setContentMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));
                        generateAlert.setPositiveBtn("Ok");
                        generateAlert.showAlertBox();*/

                    } else {
                        generalFunc.showGeneralMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));
                    }

                } else {
                    generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();
    }

}
