package com.easyshopuser.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.easyshopuser.Fragment.ProductMoreInfoFragment;
import com.easyshopuser.Fragment.SpecificationFragment;
import com.easyshopuser.R;
import com.google.android.material.tabs.TabLayout;

public class ProductSpecification  extends AppCompatActivity {

    TextView po_title;
    ImageView pd_back;
    TabLayout simpleTabLayout;
    FrameLayout pd_FrameLayout;
    String product_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_specification);

        initialize();
    }
    public void initialize()
    {
        pd_back=findViewById(R.id.pd_back);
        po_title=findViewById(R.id.po_title);
        po_title.setText("Product Details");
        Intent intent=getIntent();
        product_id=intent.getStringExtra("product_id");

        pd_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        pd_FrameLayout=findViewById(R.id.pd_FrameLayout);

        simpleTabLayout=findViewById(R.id.simpleTabLayout);
        simpleTabLayout.setTabGravity(TabLayout.GRAVITY_CENTER);
        simpleTabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        TabLayoutView();
    }


    public void TabLayoutView()
    {

/* FragmentManager fm = getSupportFragmentManager();
FragmentTransaction ft = fm.beginTransaction();
ft.replace(R.id.pd_FrameLayout, new SpecificationFragment());
ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
ft.commit();*/


        FragmentManager fragmentManager = getSupportFragmentManager();
        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        final SpecificationFragment myFragment = new SpecificationFragment();
        Bundle b = new Bundle();
        b.putString("product_id", product_id);
        myFragment.setArguments(b);
        fragmentTransaction.add(R.id.pd_FrameLayout, myFragment).commit();

        TabLayout.Tab firstTab = simpleTabLayout.newTab();
        firstTab.setText("Specification"); // set the Text for the first Tab
        simpleTabLayout.addTab(firstTab);
// add the tab at in the TabLayout
// Create a new Tab named "Second"

        TabLayout.Tab secondTab = simpleTabLayout.newTab();
        secondTab.setText("More Info");
        simpleTabLayout.addTab(secondTab); // add the tab in the TabLayout







        simpleTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                switch (tab.getPosition()) {
                    case 0:
                        FragmentManager fragmentManager = getSupportFragmentManager();
                        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        final SpecificationFragment myFragment = new SpecificationFragment();
                        Bundle b = new Bundle();
                        b.putString("product_id", product_id);
                        myFragment.setArguments(b);
                        fragmentTransaction.add(R.id.pd_FrameLayout, myFragment).commit();
                        break;
                    case 1:

                        FragmentManager fm = getSupportFragmentManager();
                        final FragmentTransaction ft = fm.beginTransaction();
                        final ProductMoreInfoFragment mf = new ProductMoreInfoFragment();
                        Bundle b1 = new Bundle();
                        b1.putString("product_id", product_id);
                        mf.setArguments(b1);
                        ft.add(R.id.pd_FrameLayout, mf).commit();
                        simpleTabLayout.getTabAt(1).select();
                        break;

                }

// set Fragmentclass Arguments







/* FragmentManager fm = getSupportFragmentManager();
FragmentTransaction ft = fm.beginTransaction();
ft.replace(R.id.flashdeal_FrameLayout, fragment);
ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
ft.commit();*/


            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


    }



}
