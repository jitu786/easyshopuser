package com.easyshopuser.Activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.easyshopuser.Adapter.Order_Return_Adapter;
import com.easyshopuser.R;
import com.easyshopuser.generalfiles.ExecuteWebServerUrl;
import com.easyshopuser.generalfiles.GeneralFunctions;
import com.easyshopuser.generalfiles.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class Order_Return extends AppCompatActivity implements Order_Return_Adapter.OnItemClickListener{

    RecyclerView product_return_list;
    TextView po_title;
    ImageView pd_back;
    Order_Return_Adapter adapter;
    GeneralFunctions generalFunc;
    View empty_state;
    ArrayList<HashMap<String, String>> dataList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order__return);

        initialize();
    }
    public void initialize()
    {
        generalFunc=new GeneralFunctions(this);
        product_return_list=findViewById(R.id.product_return_list);
        po_title=findViewById(R.id.po_title);
        pd_back=findViewById(R.id.pd_back);
        empty_state=findViewById(R.id.empty_state);
        po_title.setText("Order Return");
        pd_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        @SuppressLint("WrongConstant") LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        product_return_list.setLayoutManager(layoutManager);
        adapter = new Order_Return_Adapter(this,dataList);
        product_return_list.setAdapter(adapter);
        adapter.setOnItemClickListener(this);
        adapter.notifyDataSetChanged();
        getAllReturnOrder();
    }
    public void getAllReturnOrder()
    {
        dataList.clear();
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("type", "getAllReturnOrder");
//parameters.put("customer_id",generalFunc.getMemberId());
        parameters.put("customer_id",generalFunc.getMemberId());



        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(Order_Return.this, true, generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(final String responseString) {
                Utils.printLog("ResponseData", "Data::" + responseString);
                if (responseString != null && !responseString.equals(""))
                {
                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);
                    if (isDataAvail)
                    {
                        JSONArray msgArr = generalFunc.getJsonArray(Utils.message_str, responseString);

                        if (msgArr != null)
                        {
                            for (int i = 0; i < msgArr.length(); i++) {
                                JSONObject obj_service = generalFunc.getJsonObject(msgArr, i);
                                HashMap<String, String> dataMap_products = new HashMap<>();
                                dataMap_products.put("return_id",generalFunc.getJsonValue("return_id", String.valueOf(obj_service)));
                                dataMap_products.put("order_id",generalFunc.getJsonValue("order_id", String.valueOf(obj_service)));
                                dataMap_products.put("product_id",generalFunc.getJsonValue("product_id", String.valueOf(obj_service)));
                                dataMap_products.put("customer_id",generalFunc.getJsonValue("customer_id", String.valueOf(obj_service)));
                                dataMap_products.put("firstname",generalFunc.getJsonValue("firstname", String.valueOf(obj_service)));
                                dataMap_products.put("lastname",generalFunc.getJsonValue("lastname", String.valueOf(obj_service)));
                                dataMap_products.put("email",generalFunc.getJsonValue("email", String.valueOf(obj_service)));
                                dataMap_products.put("telephone",generalFunc.getJsonValue("telephone", String.valueOf(obj_service)));
                                dataMap_products.put("product",generalFunc.getJsonValue("product", String.valueOf(obj_service)));
                                dataMap_products.put("model",generalFunc.getJsonValue("model", String.valueOf(obj_service)));
                                dataMap_products.put("quantity",generalFunc.getJsonValue("quantity", String.valueOf(obj_service)));
                                dataMap_products.put("opened",generalFunc.getJsonValue("opened", String.valueOf(obj_service)));
                                dataMap_products.put("return_reason_id",generalFunc.getJsonValue("return_reason_id", String.valueOf(obj_service)));
                                dataMap_products.put("return_action_id",generalFunc.getJsonValue("return_action_id", String.valueOf(obj_service)));
                                dataMap_products.put("return_status_id",generalFunc.getJsonValue("return_status_id", String.valueOf(obj_service)));
                                dataMap_products.put("comment",generalFunc.getJsonValue("comment", String.valueOf(obj_service)));
                                dataMap_products.put("date_ordered",generalFunc.getJsonValue("date_ordered", String.valueOf(obj_service)));
                                dataMap_products.put("date_added",generalFunc.getJsonValue("date_added", String.valueOf(obj_service)));
                                dataMap_products.put("date_modified",generalFunc.getJsonValue("date_modified", String.valueOf(obj_service)));
                                dataMap_products.put("return_status",generalFunc.getJsonValue("return_status", String.valueOf(obj_service)));
                                dataMap_products.put("return_reason",generalFunc.getJsonValue("return_reason", String.valueOf(obj_service)));
                                dataMap_products.put("return_action",generalFunc.getJsonValue("return_action", String.valueOf(obj_service)));
                                dataMap_products.put("image",generalFunc.getJsonValue("image", String.valueOf(obj_service)));
                                dataList.add(dataMap_products);
                            }
                            adapter.notifyDataSetChanged();

                        }
                    }
                    else {
                        adapter.notifyDataSetChanged();
                        if(dataList.size()==0)
                        {
                            adapter.notifyDataSetChanged();
                            product_return_list.setVisibility(View.GONE);
                            empty_state.setVisibility(View.VISIBLE);
                        }
//generalFunc.showGeneralMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));
                    }
                } else
                {
                    generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();

    }

    @Override
    public void onItemClickList(View v, int position, int btnType, int qty) {

        if(btnType==1)
        {
            Intent int_edit_address = new Intent(Order_Return.this, Order_Return_Detail.class);
            int_edit_address.putExtra("oderDATA", dataList.get(position));
            startActivity(int_edit_address);

        }
    }

}
