package com.easyshopuser.Activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.easyshopuser.Adapter.RecentViewedAdapter;
import com.easyshopuser.R;
import com.easyshopuser.generalfiles.ExecuteWebServerUrl;
import com.easyshopuser.generalfiles.GeneralFunctions;
import com.easyshopuser.generalfiles.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class SearchProductsActivity extends AppCompatActivity implements TextWatcher {

    ImageView backImgView;
    EditText searchBox;

    RecyclerView productsRecyclerView;

    GeneralFunctions generalFunc;

    TextView noProductsTxtView;

    String currentSearchQuery;
    ProgressBar loading;
    //ErrorView errorView;

    RecentViewedAdapter adapter;
    ArrayList<HashMap<String, String>> dataList = new ArrayList<>();

    GridLayoutManager mGridLayoutManager;
    ImageView listChangeImgView;

    boolean mIsLoading = false;
    boolean isNextPageAvailable = false;

    int next_page_str = 1;

    String CURRENT_PRODUCT_DISPLAY_MODE = "GRID";
    boolean grid=true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        setContentView(R.layout.activity_search_products);

        generalFunc = new GeneralFunctions(getActContext());

        backImgView = (ImageView) findViewById(R.id.backImgView);
        noProductsTxtView = (TextView) findViewById(R.id.noProductsTxtView);
        searchBox = (EditText) findViewById(R.id.searchBox);
        productsRecyclerView = (RecyclerView) findViewById(R.id.productsRecyclerView);
        listChangeImgView = (ImageView) findViewById(R.id.listChangeImgView);
        listChangeImgView.setVisibility(View.VISIBLE);

        loading = (ProgressBar) findViewById(R.id.loading);
        //errorView = (ErrorView) findViewById(R.id.errorView);

        adapter = new RecentViewedAdapter(getActContext(), dataList);

        productsRecyclerView.setAdapter(adapter);

        productsRecyclerView.setNestedScrollingEnabled(false);

        backImgView.setOnClickListener(new setOnClickList());
        //searchBox.setFloatingLabel(MaterialEditText.FLOATING_LABEL_NONE);
        searchBox.addTextChangedListener(this);

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        int width = metrics.widthPixels;
        int height = metrics.heightPixels;

        mGridLayoutManager = new GridLayoutManager(this, 2);
        if(!grid)
            productsRecyclerView.setLayoutManager(new LinearLayoutManager(getActContext()));
        else
            productsRecyclerView.setLayoutManager(mGridLayoutManager);


        listChangeImgView.setOnClickListener(new setOnClickList());

        productsRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int visibleItemCount = recyclerView.getLayoutManager().getChildCount();
                int totalItemCount = recyclerView.getLayoutManager().getItemCount();
                int firstVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();

                int lastInScreen = firstVisibleItemPosition + visibleItemCount;
                if ((lastInScreen == totalItemCount) && !(mIsLoading) && isNextPageAvailable == true) {

                    mIsLoading = true;
                    //          adapter.addFooterView();

                    findProducts(true, currentSearchQuery);

                } else if (isNextPageAvailable == false) {
                    //            adapter.removeFooterView();
                }
            }
        });

        setLabels();

        if (getIntent().getStringExtra("PRODUCT_NAME") != null && !getIntent().getStringExtra("PRODUCT_NAME").equals("")) {
            searchBox.setText(getIntent().getStringExtra("PRODUCT_NAME"));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();


        //  getWishListData();
    }

    public void setLabels() {
        searchBox.setHint("Search products");
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        currentSearchQuery = "" + charSequence;
        findProducts(false, currentSearchQuery);
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    @Override
    public void onBackPressed() {
        Utils.hideKeyboard((Activity) getActContext());
        super.onBackPressed();
    }

    ExecuteWebServerUrl exeServerUrlTask;

    public void findProducts(final boolean isFromRecurring, final String searchQuery) {

        if (isFromRecurring == false) {
            dataList.clear();
            adapter.notifyDataSetChanged();
            next_page_str = 1;
        }

        if (searchQuery.trim().equals("")) {
            if (loading.getVisibility() == View.VISIBLE) {
                loading.setVisibility(View.GONE);
            }
            next_page_str = 1;
            //adapter.removeFooterView();
            isNextPageAvailable = false;
            return;
        }

        if (exeServerUrlTask != null) {
            exeServerUrlTask.cancel();
            exeServerUrlTask = null;
        }

/*
        if (errorView.getVisibility() == View.VISIBLE) {
            errorView.setVisibility(View.GONE);
        }
*/

        if (loading.getVisibility() != View.VISIBLE && isFromRecurring == false) {
            loading.setVisibility(View.VISIBLE);
        }

        if (noProductsTxtView.getVisibility() != View.GONE) {
            noProductsTxtView.setVisibility(View.GONE);
        }

        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("type", "searchProducts");
        parameters.put("searchQuery", searchQuery);
        parameters.put("page", "" + next_page_str);
        parameters.put("customer_id", generalFunc.getMemberId());

//        parameters.put("country_id", Utils.getCountryCode(Utils.countryToDisplay));


        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
//        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.setIsDeviceTokenGenerate(true, "vDeviceToken");
        exeServerUrlTask = exeWebServer;
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(final String responseString) {

                Utils.printLog("ResponseData", "Data::" + responseString);

                if (!currentSearchQuery.equals(searchQuery)) {
                    return;
                }

                if (responseString != null && !responseString.equals("")) {

                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);

                    if (isFromRecurring == false) {
                        dataList.clear();
                        adapter.notifyDataSetChanged();
                    }

                    if (isDataAvail == true) {
                        JSONArray msgArr = generalFunc.getJsonArray(Utils.message_str, responseString);
                        JSONArray wishListDataArr = generalFunc.getJsonArray("UserWishListData", responseString);
                        ArrayList<String> wishListProductIdsList = new ArrayList<>();
                        if (wishListDataArr != null) {
                            for (int i = 0; i < wishListDataArr.length(); i++) {

                                JSONObject obj_temp = generalFunc.getJsonObject(wishListDataArr, i);

                                wishListProductIdsList.add(generalFunc.getJsonValue("product_id", obj_temp));
                            }
                        }

                        if (msgArr != null)
                        {

                            for (int i = 0; i < msgArr.length(); i++) {
                                JSONObject obj_temp = generalFunc.getJsonObject(msgArr, i);
                                String productImg = generalFunc.getJsonValue("image", obj_temp);
                                String productName = generalFunc.getJsonValue("name", obj_temp);
                                String special_price = generalFunc.getJsonValue("special_price", obj_temp);
                                String special_date_end = generalFunc.getJsonValue("special_date_end", obj_temp);
                                String productDes = generalFunc.getJsonValue("description", obj_temp);
                                String productId = generalFunc.getJsonValue("product_id", obj_temp);
                                String price = generalFunc.getJsonValue("price", obj_temp);
                                String catId = generalFunc.getJsonValue("category_id", obj_temp);
                                String product_type = generalFunc.getJsonValue("product_type", obj_temp);
                                String seller_id = generalFunc.getJsonValue("seller_id", obj_temp);
                                String seller_name = generalFunc.getJsonValue("seller_name", obj_temp);
                                String store_country = generalFunc.getJsonValue("store_country", obj_temp);

                                String TotalRating = generalFunc.getJsonValue("TotalRating", obj_temp);
                                String TotalRatingCount = generalFunc.getJsonValue("TotalRatingCount", obj_temp);


                                HashMap<String, String> dataMap_products = new HashMap<>();
                                dataMap_products.put("seller_country", store_country);
                                dataMap_products.put("seller_id", seller_id);
                                dataMap_products.put("seller_name", seller_name);
                                dataMap_products.put("product_type", product_type);
                                dataMap_products.put("name", productName);
                                dataMap_products.put("special_price", special_price);
                                dataMap_products.put("special_date_end", special_date_end);
                                dataMap_products.put("category_id", catId);
                                dataMap_products.put("product_id", productId);
                                dataMap_products.put("price", price);
                                dataMap_products.put("description", Utils.html2text(productDes));
                                dataMap_products.put("image", productImg);
                                dataMap_products.put("TotalRating", TotalRating);
                                dataMap_products.put("TotalRatingCount", TotalRatingCount);
                                dataMap_products.put("isWishlisted", wishListProductIdsList.contains(productId) ? "Yes" : "No");

                                // dataMap_products.put("TYPE", "" + MainPageCategoryRecycleAdapter.TYPE_ITEM);
                                dataList.add(dataMap_products);
                            }
                        }

                        adapter.notifyDataSetChanged();
                        isNextPageAvailable = true;
                        next_page_str = next_page_str + 1;
                        //adapter.removeFooterView();
                    } else {
                        if (next_page_str < 2) {

                            noProductsTxtView.setText(generalFunc.getJsonValue(Utils.message_str, responseString));
                            noProductsTxtView.setVisibility(View.VISIBLE);
                        }
                        isNextPageAvailable = false;

                        // adapter.removeFooterView();
                    }

                    mIsLoading = false;

                    closeLoader();
                } else {
                    if (isFromRecurring == false) {

                        generateErrorView();
                    }
                }
            }
        });
        exeWebServer.execute();
    }

    public class setOnClickList implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.backImgView:
                    onBackPressed();
                    break;
                case R.id.listChangeImgView:
                    if (CURRENT_PRODUCT_DISPLAY_MODE.equals("LIST")) {
                        productsRecyclerView.setLayoutManager(mGridLayoutManager);
                        CURRENT_PRODUCT_DISPLAY_MODE = "GRID";
                        // adapter.CURRENT_PRODUCT_DISPLAY_MODE = "GRID";
                        adapter.notifyDataSetChanged();
                        productsRecyclerView.setAdapter(adapter);
                        listChangeImgView.setImageResource(R.mipmap.ic_view_grid);
                        grid=true;
                    } else {
                        productsRecyclerView.setLayoutManager(new LinearLayoutManager(getActContext()));
                        CURRENT_PRODUCT_DISPLAY_MODE = "LIST";
                        // adapter.CURRENT_PRODUCT_DISPLAY_MODE = "LIST";
                        adapter.notifyDataSetChanged();
                        productsRecyclerView.setAdapter(adapter);
                        listChangeImgView.setImageResource(R.mipmap.ic_view_list);
                        grid=false;
                    }
                    break;

            }
        }
    }

    public void closeLoader() {
        if (loading.getVisibility() == View.VISIBLE) {
            loading.setVisibility(View.GONE);
        }
    }

    public void generateErrorView() {

        closeLoader();

        //generalFunc.generateErrorView(errorView, "Error", "Please check your internet connection and try again");
/*
        if (errorView.getVisibility() != View.VISIBLE) {
            errorView.setVisibility(View.VISIBLE);
        }
        errorView.setOnRetryListener(new ErrorView.RetryListener() {
            @Override
            public void onRetry() {
                findProducts(false, currentSearchQuery);
            }
        });*/
    }

    public Context getActContext() {
        return SearchProductsActivity.this;
    }
}

