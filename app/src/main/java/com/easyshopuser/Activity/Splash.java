package com.easyshopuser.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.easyshopuser.R;
import com.easyshopuser.generalfiles.ExecuteWebServerUrl;
import com.easyshopuser.generalfiles.GeneralFunctions;
import com.easyshopuser.generalfiles.Utils;
import com.easyshopuser.utils.SharedPref;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;
import java.util.HashMap;

public class Splash  extends AppCompatActivity {

    ImageView logo,iot,order;
    ArrayList<HashMap<String, String>> dataList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        setFireBase(this);
        initialize();
    }
    public void initialize()
    {
        GeneralFunctions.printHashKey(getActContext());
        logo=findViewById(R.id.logo);
        Animation animation= AnimationUtils.loadAnimation(Splash.this,R.anim.zoom_out);
        logo.startAnimation(animation);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }
            @Override
            public void onAnimationEnd(Animation animation) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        SharedPref.WriteSharePrefrence1(getActContext(),"newarrival",dataList);
                        SharedPref.WriteSharePrefrence1(getActContext(),"trading",dataList);
                        startActivity(new Intent(getApplicationContext(), Dashboard.class));
                    }
                }, 2000); }
            @Override
            public void onAnimationRepeat(Animation animation) { }
        });
    }

    private Context getActContext() {
        return this;
    }

    public static void setFireBase(Context context) {

        GeneralFunctions generalFunc = new GeneralFunctions(context);

        //SharedPreferences sharedPreferences=getSharedPreferences(getString(R.string.FCM_PREF),MODE_PRIVATE);
        String token = FirebaseInstanceId.getInstance().getToken();

        //Utils.isTokenSet=sharedPreferences.getBoolean("isTokenSet",false);

        if (token == null) {
            Utils.printLog(">>> Token", "No Token");
            return;
        }

        //Utils.printLog(">>> Token", FirebaseInstanceId.getInstance().getToken());

        HashMap<String, String> param = new HashMap<>();
        param.put("type", "insertTokenId");
        param.put("token_id", token);

        if (generalFunc.isUserLoggedIn()) {
            param.put("customer_id", generalFunc.getMemberId());
            Utils.printLog(">>> Member Id", generalFunc.getMemberId());
        } else {
            param.put("customer_id", "");
        }

        ExecuteWebServerUrl executeWebServerUrl = new ExecuteWebServerUrl(param);
        executeWebServerUrl.execute();

    }

}

