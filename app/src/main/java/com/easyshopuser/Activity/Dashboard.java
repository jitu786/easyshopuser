package com.easyshopuser.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.easyshopuser.Fragment.AccountFragment;
import com.easyshopuser.Fragment.CategoryFragment;
import com.easyshopuser.Fragment.FlashDealFragment;
import com.easyshopuser.Fragment.Home;
import com.easyshopuser.R;
import com.easyshopuser.generalfiles.ExecuteWebServerUrl;
import com.easyshopuser.generalfiles.GeneralFunctions;
import com.easyshopuser.generalfiles.Utils;
import com.easyshopuser.utils.SharedPref;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.HashMap;

public class Dashboard extends AppCompatActivity {

    ImageView navigation;
    RelativeLayout dashboard_cart;
    ImageView navigation_menu;
    LinearLayout Dashboardtool,tool;
    BottomNavigationView bottomNavigation;
    GeneralFunctions generalFunc;
    TextView txtCartcount;

    public String  userCart="0";


    BottomNavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.home:
                            tool=findViewById(R.id.tool);
                            tool.setVisibility(View.VISIBLE);
                            getSupportFragmentManager().beginTransaction().replace(R.id.dashboard_content, new Home()).commit();
                            return true;
                        case R.id.category:

                            tool=findViewById(R.id.tool);
                            tool.setVisibility(View.VISIBLE);
                            getSupportFragmentManager().beginTransaction().replace(R.id.dashboard_content, new CategoryFragment()).commit();
                            return true;
                        case R.id.deals:

                            tool=findViewById(R.id.tool);
                            tool.setVisibility(View.VISIBLE);
                            getSupportFragmentManager().beginTransaction().replace(R.id.dashboard_content, new FlashDealFragment()).commit();
                            return true;
                        case R.id.notification:
                            startActivity(new Intent(getApplicationContext(),My_Message.class));
                            return true;
                        case R.id.myaccount:

                            tool=findViewById(R.id.tool);
                            tool.setVisibility(View.GONE);
                            getSupportFragmentManager().beginTransaction().replace(R.id.dashboard_content, new AccountFragment()).commit();
                            return true;
                    }
                    return false;
                }
            };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        initialize();
    }
    public void initialize()
    {

        getSupportFragmentManager().beginTransaction().add(R.id.dashboard_content, new Home()).commit();
        txtCartcount=findViewById(R.id.txtCartcount);

        bottomNavigation = findViewById(R.id.bottom_navigation);
        bottomNavigation.setOnNavigationItemSelectedListener(navigationItemSelectedListener);
        dashboard_cart=findViewById(R.id.dashboard_cart);
        navigation_menu=findViewById(R.id.navigation_menu);
        generalFunc=new GeneralFunctions(Dashboard.this);
        tool=findViewById(R.id.tool);
        tool.setVisibility(View.VISIBLE);
        getUserCartCount(generalFunc.getMemberId(),getActContext());

        navigation_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i2 = new Intent(getApplicationContext(), NavigationList.class);
                startActivity(i2);
                overridePendingTransition( R.anim.slide_in_down, R.anim.slide_out_down );

            }
        });
        dashboard_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!generalFunc.getMemberId().isEmpty()) {
                    Intent i2 = new Intent(getApplicationContext(), Add_To_Cart.class);
                    startActivity(i2);
                    overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
                }
                else {

                    Toast.makeText(Dashboard.this, "Login is required.", Toast.LENGTH_SHORT).show();


                }


            }
        });
    }


    public Context getActContext() {

        return  Dashboard.this;
    }
    public void getUserCartCount(String memberId, final Context context)
    {
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("type", "getUserCartCount");
        parameters.put("customer_id",memberId);
        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse()
        {
            @Override
            public void setResponse(final String responseString) {
                Utils.printLog("ResponseData", "Data::" + responseString);
                if (responseString != null && !responseString.equals("")) {
                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);
                    if (isDataAvail)
                    {
                        GeneralFunctions functions=new GeneralFunctions(context);
                        SharedPref.WriteSharePrefrence(context,"cart",functions.getJsonValue("message",responseString));
                        if(txtCartcount!=null)
                            txtCartcount.setText(functions.getJsonValue("message",responseString));
                    }
                    else {
                    }
                    //generalFunc.showGeneralMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));
                } else
                {

                    //generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();

    }


}

