package com.easyshopuser.Activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.easyshopuser.Adapter.AdapterRecycler;
import com.easyshopuser.Adapter.CatAdapter;
import com.easyshopuser.Adapter.ProductRatingBarAdapter;
import com.easyshopuser.Adapter.ViewPagerAdapter;
import com.easyshopuser.Model.ProductCategoriesData;
import com.easyshopuser.R;
import com.easyshopuser.bannerslider.banners.Banner;
import com.easyshopuser.bannerslider.banners.RemoteBanner;
import com.easyshopuser.bannerslider.events.OnBannerClickListener;
import com.easyshopuser.bannerslider.views.BannerSlider;
import com.easyshopuser.generalfiles.ExecuteWebServerUrl;
import com.easyshopuser.generalfiles.GeneralFunctions;
import com.easyshopuser.generalfiles.Utils;
import com.easyshopuser.helper.StartActProcess;
import com.easyshopuser.utils.SharedPref;
import com.easyshopuser.view.GenerateAlertBox;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class ProductDescription extends AppCompatActivity {

    String dproduct_id="",dcust_id="";
    List imageUrl;
    LinearLayout pdviewall;
    TextView po_title;
    ImageView pd_navigation,pd_back,img,img_like;
    TextView add_to_cart;
    CatAdapter adapter;
    View reviewLayout;
    RelativeLayout cart;
    LinearLayout lyt_time,discount_lyt;
    RecyclerView productRatingRecyclerView;
    String cat_id;
    RelativeLayout rating_review;
    TextView buy_now,p_name,p_price,p_des,p_avialibility,p_brand,txtsp_price,discount_txt;
    GeneralFunctions generalFunc;
    ArrayList<HashMap<String, String>> dataList = new ArrayList<>();
    ArrayList<HashMap<String, String>> imagdata = new ArrayList<>();
    String product_id="";
    CardView like_card;
    ArrayList<HashMap<String, String>> list_ratings = new ArrayList<>();
    ProductRatingBarAdapter ratingAdapter;
    SimpleRatingBar simpleRatingBar;
    String shareData;
    ArrayList<String> itemcolor=new ArrayList<>();
    ArrayList<String> itemsize=new ArrayList<>();
    int position_type=0,item_position=0;
    ArrayAdapter<String> adpater_items;
    String arr[]={""},option_id_color="",option_value_id_color="",option_id_size="",option_value_id_size="";
    String color[];
    String size[];
    List<Object> list;
    AppCompatSpinner choice_type,spinner_data;
    RecyclerView categoriesList;
    View categoriesListView;
    RelativeLayout rela_serach,relatedProductsArea;
    TextView txtCartcount;
    TextView hour,second,minute,day;
    RelativeLayout mainLayout;
    BannerSlider bannerSlider;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_description);

        imageUrl = new ArrayList();
        initialize();

        buy_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

              /*  Intent i2 = new Intent(getApplicationContext(), Product_Option.class);
                startActivity(i2);
                overridePendingTransition( R.anim.left_to_right, R.anim.right_to_left );*/
                if(!generalFunc.getMemberId().isEmpty())
                    addItemToCart(true);
                else {

                    Toast.makeText(ProductDescription.this, "Login is required.", Toast.LENGTH_SHORT).show();


                }


            }
        });

        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i2 = new Intent(getApplicationContext(), Add_To_Cart.class);
                startActivity(i2);
                overridePendingTransition( R.anim.left_to_right, R.anim.right_to_left );

            }
        });

        add_to_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!generalFunc.getMemberId().isEmpty())
                    addItemToCart(false);
                else {


                    Toast.makeText(ProductDescription.this, "Login is required", Toast.LENGTH_SHORT).show();
                  /*  GenerateAlertBox alertBox= generalFunc.showGeneralMessage("", "Login Required.");
                    alertBox.setBtnClickList(new GenerateAlertBox.HandleAlertBtnClick() {
                        @Override
                        public void handleBtnClick(int btn_id) {
                            if(btn_id==1)
                            {
                                Intent intent=new Intent(getActContext(), Login.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        }
                    });*/
                }

            }
        });

        like_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!generalFunc.getMemberId().isEmpty())
                    addItemToWishList();
                else
                    generalFunc.showGeneralMessage("","Login Required.");
            }
        });

        pd_navigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i2 = new Intent(getApplicationContext(), NavigationList.class);
                startActivity(i2);
                overridePendingTransition( R.anim.slide_in_down, R.anim.slide_out_down );

            }
        });

        pdviewall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent(getApplicationContext(),ProductSpecification.class);
                intent.putExtra("product_id",product_id);
                startActivity(intent);
            }
        });

        simpleRatingBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!generalFunc.getMemberId().isEmpty())
                    AddRatingFunction();
                else {
                    GenerateAlertBox alertBox= generalFunc.showGeneralMessage("", "Login Required.");
                    alertBox.setBtnClickList(new GenerateAlertBox.HandleAlertBtnClick() {
                        @Override
                        public void handleBtnClick(int btn_id) {
                            if(btn_id==1)
                            {
                                Intent intent=new Intent(getActContext(), Login.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);

                            }
                        }
                    });

                }

            }
        });


        choice_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                if(position>0)
                {
                    for (int i = 0; i < list.size(); i++)
                    {
                        ProductCategoriesData data = (ProductCategoriesData) list.get(i);
                        if (itemcolor.get(position).contains(data.getOption_value_name())) {
                            option_id_color = data.getProduct_option_id();
                            option_value_id_color = data.getProduct_option_value_id();
                        }
                    }
                    Log.d("jitu color",option_id_color+" "+option_value_id_color);

                }
                else
                {

                    option_value_id_color="";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {
            }
        });



        spinner_data.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                if(position>0) {
                    for (int i = 0; i < list.size(); i++) {
                        ProductCategoriesData data = (ProductCategoriesData) list.get(i);
                        if (itemsize.get(position).contains(data.getOption_value_name())) {
                            option_id_size= data.getProduct_option_id();
                            option_value_id_size = data.getProduct_option_value_id();
                        }
                    }
                    Log.d("jitu size",option_id_size+" "+option_value_id_size);

                }
                else
                {
                    option_id_size="";
                    option_value_id_size="";
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

                //called spinner item change event

            }
        });

       /* img_like1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!generalFunc.getMemberId().isEmpty())
                    addItemToWishList();
                else
                    generalFunc.showGeneralMessage("","Login Required.");
            }
        });*/

        /*rela_serach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                (new StartActProcess(getActContext())).startAct(SearchProductsActivity.class);
            }
        });*/

    }
    public void initialize()
    {
        discount_txt=findViewById(R.id.discount);
        discount_lyt=findViewById(R.id.discount_lyt);
        bannerSlider = findViewById(R.id.bannerSlider);
        mainLayout=findViewById(R.id.mainLayout);
        day=findViewById(R.id.day);
        lyt_time=findViewById(R.id.spcial_time);
        hour=findViewById(R.id.hour);
        minute=findViewById(R.id.minute);
        second=findViewById(R.id.second);
        txtCartcount=findViewById(R.id.txtCartcount);
        generalFunc=new GeneralFunctions(ProductDescription.this);
        new Dashboard().getUserCartCount(generalFunc.getMemberId(),ProductDescription.this);
        txtCartcount.setText(SharedPref.ReadSharePrefrence(getActContext(),"cart"));
        txtsp_price=findViewById(R.id.sp_price);
        list = new ArrayList<>();
        pdviewall=findViewById(R.id.pdviewall);
        po_title=findViewById(R.id.po_title);
        categoriesListView = findViewById(R.id.categoriesListView);
        categoriesList = findViewById(R.id.categoriesList);
        choice_type=findViewById(R.id.choice_type);
        spinner_data=findViewById(R.id.spinner_data);
        productRatingRecyclerView = findViewById(R.id.productRatingRecyclerView);
        rating_review=findViewById(R.id.rating_review);
        buy_now=findViewById(R.id.buy_now);

        cart=findViewById(R.id.dashboard_cart);
        add_to_cart=findViewById(R.id.add_to_cart);
        pd_back=findViewById(R.id.pd_back);
        pd_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        pd_navigation=findViewById(R.id.pd_navigation);
        p_des=findViewById(R.id.p_des);
        p_name=findViewById(R.id.p_name);
        p_price=findViewById(R.id.p_price);
        p_avialibility=findViewById(R.id.p_avialibility);
        p_brand=findViewById(R.id.p_brand);
        img_like=findViewById(R.id.img_like);
        like_card=findViewById(R.id.like);
        simpleRatingBar = findViewById(R.id.totalAvgRating);

        if(getIntent()!=null) {
            product_id = getIntent().getStringExtra("product_id");
            getProductInfo();
            getCategoryData();
        }
        po_title.setText("Product Description");
        img=findViewById(R.id.img);
        productRatingRecyclerView.setLayoutManager(new LinearLayoutManager(getActContext()));
        ratingAdapter = new ProductRatingBarAdapter(getActContext(), list_ratings, generalFunc, false);
        productRatingRecyclerView.setAdapter(ratingAdapter);
        productRatingRecyclerView.setNestedScrollingEnabled(false);



    }
    public void setBannerData(List<Banner> banners) {
        bannerSlider.setBanners(banners);

        bannerSlider.setOnBannerClickListener(new OnBannerClickListener() {
            @Override
            public void onClick(int position) {
                setNestedBannerData(imageUrl,position);
            }
        });
    }
    private void setNestedBannerData(List imageUrl, int position) {

        AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActContext());
        View mView = getLayoutInflater().inflate(R.layout.view_pager, null);
        ViewPager viewPager = mView.findViewById(R.id.viewPager);

        if (this.imageUrl != null) {
            ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getActContext(), imageUrl,position);
            viewPager.setAdapter(viewPagerAdapter);
            mBuilder.setView(mView);
            AlertDialog mDialog = mBuilder.create();
            mDialog.show();
        } else {

            Toast.makeText(ProductDescription.this, "No Url Found", Toast.LENGTH_SHORT).show();
        }
    }
    public void getProductInfo()
    {
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("type", "getProductInfo");
        parameters.put("product_id", product_id);
        parameters.put("customer_id", generalFunc.getMemberId());


        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(final String responseString) {
                Utils.printLog("ResponseData", "Data::" + responseString);
                if (responseString != null && !responseString.equals("")) {
                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);
                    if (isDataAvail) {
                        mainLayout.setVisibility(View.VISIBLE);
                        JSONObject obj_cat = generalFunc.getJsonObject(Utils.message_str, responseString);


                        String name = generalFunc.getJsonValue("name", obj_cat);
                        String quantity = generalFunc.getJsonValue("quantity", obj_cat);
                        String store_name = generalFunc.getJsonValue("store_name", obj_cat);
                        String store_country = generalFunc.getJsonValue("store_country", obj_cat);
                        String description = generalFunc.getJsonValue("description", obj_cat);
                        String image = generalFunc.getJsonValue("image", obj_cat);
                        String price = generalFunc.getJsonValue("price", obj_cat);
                        String sp_price = generalFunc.getJsonValue("special_price", obj_cat);
                        String product_id = generalFunc.getJsonValue("product_id", obj_cat);
                        String  category_id = generalFunc.getJsonValue("category_id", obj_cat);
                        String  seller_name = generalFunc.getJsonValue("seller_name", obj_cat);
                        String  shipping = generalFunc.getJsonValue("shipping", obj_cat);
                        String  points = generalFunc.getJsonValue("points", obj_cat);
                        String  weight = generalFunc.getJsonValue("weight", obj_cat);
                        String  viewed = generalFunc.getJsonValue("viewed", obj_cat);
                        String  stock_status_id = generalFunc.getJsonValue("stock_status_id", obj_cat);
                        String  TotalRating = generalFunc.getJsonValue("TotalRating", obj_cat);
                        String  TotalRatingCount = generalFunc.getJsonValue("TotalRatingCount", obj_cat);
                        String  discount = generalFunc.getJsonValue("discount", obj_cat);
                        String  SHARE_DATA = generalFunc.getJsonValue("SHARE_DATA", obj_cat);
                        String  title = generalFunc.getJsonValue("title", obj_cat);
                        String  seller_id = generalFunc.getJsonValue("seller_id", obj_cat);
                        String  model = generalFunc.getJsonValue("model", obj_cat);
                        String  date_end_format = generalFunc.getJsonValue("date_end_format", obj_cat);
                        JSONArray msgArr = generalFunc.getJsonArray("MoreImages", obj_cat);

                        /*if (!discount.isEmpty())
                        {
                            discount_lyt.setVisibility(View.VISIBLE);
                            discount_txt.setText(discount);
                        }
                        else
                        {
                            discount_lyt.setVisibility(View.GONE);
                        }*/
                        List<Banner> banners = new ArrayList<>();
                        if (!generalFunc.getJsonValue("image", obj_cat).equals("")) {
                            if (msgArr != null && msgArr.length() > 0) {

                                for (int i = 0; i < msgArr.length(); i++) {
                                    JSONObject img = generalFunc.getJsonObject(msgArr, i);
                                    HashMap<String, String> map = new HashMap<>();
                                    map.put("image", generalFunc.getJsonValue("image", img));
                                    String imgUrl = generalFunc.getJsonValue("image", img);
                                    imagdata.add(map);
                                    imageUrl.add(imgUrl);
                                    banners.add(new RemoteBanner(imgUrl));

                                }
                            }
                            else if (!generalFunc.getJsonValue("image", obj_cat).equals("")) {
                                imageUrl.add(generalFunc.getJsonValue("image", obj_cat));
                                banners.add(new RemoteBanner(generalFunc.getJsonValue("image", obj_cat)));
                            }
                            setBannerData(banners);
                        }
                        shareData=SHARE_DATA;
                        //p_des.setText(description);
                        p_des.setText(Html.fromHtml(Utils.html2text(description)));

                        p_name.setText(name);
                        if(sp_price.isEmpty())
                        {
                            p_price.setText("Rs."+price);
                            lyt_time.setVisibility(View.GONE);

                        }
                        else
                        {
                            txtsp_price.setVisibility(View.VISIBLE);
                            txtsp_price.setText("Rs."+sp_price);
                            lyt_time.setVisibility(View.VISIBLE);
                            p_price.setPaintFlags(p_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                            p_price.setText("Rs."+price);
                            runThred(date_end_format);

                        }

                        p_brand.setText(model);
                        cat_id=category_id;
                        try {
                            if(Integer.parseInt(quantity)>0)
                                p_avialibility.setText("Availaible");
                            else
                                p_avialibility.setText("Not Availaible");
                        }catch (Exception e){}
                       /* if(imagdata.size()>0) {
                            Glide.with(getActContext())
                                    .load(imagdata.get(0).get("image"))
                                    //.diskCacheStrategy(DiskCacheStrategy.NONE)
                                    .placeholder(R.drawable.img)
                                    //.skipMemoryCache(true)
                                    .into(img);
                        }
                        else
                        {
                            Glide.with(getActContext())
                                    .load(image)
                                    //.diskCacheStrategy(DiskCacheStrategy.NONE)
                                    .placeholder(R.drawable.img)
                                    //.skipMemoryCache(true)
                                    .into(img);
                        }*/

                        JSONArray wishListDataArr = generalFunc.getJsonArray("UserWishListData", responseString);
                        ArrayList<String> wishListProductIdsList = new ArrayList<>();
                        if (wishListDataArr != null) {
                            for (int i = 0; i < wishListDataArr.length(); i++) {

                                JSONObject obj_temp = generalFunc.getJsonObject(wishListDataArr, i);

                                wishListProductIdsList.add(generalFunc.getJsonValue("product_id", obj_temp));
                            }
                        }

                        if (wishListProductIdsList.contains(getIntent().getStringExtra("product_id"))) {
                            img_like.setImageResource(R.mipmap.ic_fav_fill);
                            //img_like1.setImageResource(R.mipmap.ic_fav_fill);
                        }
                        findReviews();

                        //JsonArray msg =generalFunc.getJsonArray("MoreImages",);

                    }
                    else
                        generalFunc.showGeneralMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));

                } else {
                    generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();
    }

    private Context getActContext()
    {
        return  ProductDescription.this;
    }

    public void addItemToWishList() {

        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("type", "addProductToWishList");
        parameters.put("product_id", getIntent().getStringExtra("product_id"));
        parameters.put("category_id",cat_id);
        parameters.put("customer_id", "" + generalFunc.getMemberId());

        Utils.printLog("WishListParameters::", "::" + parameters.toString());
        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.setIsDeviceTokenGenerate(true, "vDeviceToken");
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(final String responseString) {

                Utils.printLog("ResponseData", "Data::" + responseString);

                if (responseString != null && !responseString.equals("")) {
                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);

                    if (isDataAvail) {
                        if (generalFunc.getJsonValue("isDelete", responseString).equalsIgnoreCase("yes"))
                        {
                            Toast.makeText(ProductDescription.this, "Product is deleted sucessfully from your wishlist.", Toast.LENGTH_SHORT).show();
                            img_like.setImageResource(R.mipmap.ic_fav_border);
                            //img_like1.setImageResource(R.mipmap.ic_fav_border);
                        } else {
                            Toast.makeText(ProductDescription.this, "Product is added sucessfully to your wishlist.", Toast.LENGTH_SHORT).show();
                            img_like.setImageResource(R.mipmap.ic_fav_fill);
                            //img_like1.setImageResource(R.mipmap.ic_fav_fill);
                        }
                    }
                    //            generalFunc.showGeneralMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));
                } else {
                    generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();
    }
    private void AddRatingFunction() {
        if (!generalFunc.isUserLoggedIn()) {

            //  generalFunc.startActivity(AppLoginActivity.class);
            return;
        }
        reviewLayout = findViewById(R.id.review_layout);
        reviewLayout.setVisibility(View.VISIBLE);
        final EditText title =  findViewById(R.id.review_title);
        final EditText description =  findViewById(R.id.review_description);
        final TextView submit =  findViewById(R.id.review_submit);



        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!description.getText().toString().equals(""))
                    addReviewToDB(title.getText().toString(), description.getText().toString());
                else
                    Toast.makeText(getActContext(), "Please share your experience to rate the product.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void addReviewToDB(String title, String description) {

        if(generalFunc.getMemberId().isEmpty()) {
            generalFunc.showGeneralMessage("", "Login Required.");
            return;
        }

        String rating = String.valueOf(simpleRatingBar.getRating());
        HashMap<String, String> param = new HashMap<>();

        param.put("type", "addProductReview");
        param.put("author", title);
        //param.put("title",title);
        param.put("text", description);
        param.put("rating", rating);
        param.put("customer_id", generalFunc.getMemberId());
        param.put("product_id", getIntent().getStringExtra("product_id"));

        ExecuteWebServerUrl url = new ExecuteWebServerUrl(param);
        url.setLoaderConfig(this, true, generalFunc);
        url.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(String responseString) {
                boolean isDataValid = GeneralFunctions.checkDataAvail("action", responseString);
                if (isDataValid) {
                    findReviews();
                    reviewLayout.setVisibility(View.GONE);
                    Toast.makeText(getActContext(), generalFunc.getJsonValue(Utils.message_str, responseString), Toast.LENGTH_SHORT).show();
                }
            }
        });
        url.execute();
    }
    public void findReviews() {
        list_ratings.clear();
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("type", "getAllReviewsOfProduct");
        parameters.put("customer_id", "" + generalFunc.getMemberId());
        parameters.put("product_id", getIntent().getStringExtra("product_id"));
        parameters.put("isLimited", "Yes");

        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(getActContext(), false, generalFunc);
//        exeWebServer.setIsDeviceTokenGenerate(true, "vDeviceToken");
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(final String responseString) {

                Utils.printLog("RatingResponseData", "Data::" + responseString);

                if (responseString != null && !responseString.equals("")) {
                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);

                    if (isDataAvail) {
                        JSONArray obj_arr = generalFunc.getJsonArray("message", responseString);
                        //((MTextView) findViewById(R.id.totalReviewsTxtView)).setText(generalFunc.getJsonValue("TotalRatingCount", responseString) + " Reviews");
                        //((MTextView) findViewById(R.id.totalRatingsReviewsTxtView)).setText(generalFunc.getJsonValue("TotalRating", responseString));
                        if (obj_arr != null) {

                            for (int i = 0; i < obj_arr.length(); i++) {

                                JSONObject obj_temp = generalFunc.getJsonObject(obj_arr, i);

                                HashMap<String, String> data_map = new HashMap<>();
                                data_map.put("text", generalFunc.getJsonValue("text", obj_temp));
                                data_map.put("rating", generalFunc.getJsonValue("rating", obj_temp));
                                data_map.put("date_added", generalFunc.getJsonValue("date_added", obj_temp));
                                data_map.put("customer_name", generalFunc.getJsonValue("customer_name", obj_temp));
                                data_map.put("TYPE", "" + ProductRatingBarAdapter.TYPE_ITEM);
                                data_map.put("product_id", generalFunc.getJsonValue("product_id", obj_temp));
                                dproduct_id=generalFunc.getJsonValue("product_id", obj_temp);
                                data_map.put("customer_id", generalFunc.getJsonValue("customer_id", obj_temp));
                                dcust_id=generalFunc.getJsonValue("customer_id", obj_temp);

                                list_ratings.add(data_map);
                            }

                            ratingAdapter.notifyDataSetChanged();

                            if (dproduct_id.equals(product_id)  && dcust_id.equals(generalFunc.getMemberId()))
                            {
                                simpleRatingBar.setVisibility(View.GONE);
                            }
                            else
                            {
                                simpleRatingBar.setVisibility(View.VISIBLE);
                            }
                            if (list_ratings.size() > 0 && obj_arr.length() > 0) {
                                rating_review.setVisibility(View.VISIBLE);
                            } else {
                                //(findViewById(R.id.ratingArea)).setVisibility(View.GONE);
                            }
                        } else {
                            //(findViewById(R.id.ratingArea)).setVisibility(View.GONE);
                        }
                    } else {
                        //(findViewById(R.id.ratingArea)).setVisibility(View.GONE);
                    }
                } else {

                    //(findViewById(R.id.ratingArea)).setVisibility(View.GONE);
                }
            }
        });
        exeWebServer.execute();
    }
    public void addItemToCart(final boolean isGotoPlaceOrder)
    {
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("type", "addProductToCart");
        parameters.put("product_id", getIntent().getStringExtra("product_id"));
//        parameters.put("category_id", getIntent().getStringExtra("category_id"));
        parameters.put("quantity", "1");
        parameters.put("customer_id", "" + generalFunc.getMemberId());

      /*  if(color!=null&&size!=null)
        {
            if (checkOptopnSelect())
            {
                parameters.put("product_option_id_color", option_id_color);
                parameters.put("product_option_value_id_color", option_value_id_color);
                parameters.put("product_option_id_size", option_id_size);
                parameters.put("product_option_value_id_size", option_value_id_size);
            }
            else
            {
                generalFunc.ErrorOption();
                return;
            }
        }*/
        Log.d("paramter",parameters.toString());
        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.setIsDeviceTokenGenerate(true, "vDeviceToken");
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(final String responseString) {

//                Log.e(TAG, "setResponse: "+responseString );
                Utils.printLog("ResponseData", "Data::" + responseString);

                if (responseString != null && !responseString.equals(""))
                {
                    Toast.makeText(ProductDescription.this, "Product is added sucessfully to your cart.", Toast.LENGTH_SHORT).show();
                    new Dashboard().getUserCartCount(generalFunc.getMemberId(),ProductDescription.this);
                    txtCartcount.setText(SharedPref.ReadSharePrefrence(getActContext(),"cart"));
                    if (isGotoPlaceOrder) {
                        new StartActProcess(getActContext()).startAct(Place_Order.class);
                    } else {
                        new StartActProcess(getActContext()).startAct(Add_To_Cart.class);

                        /*GenerateAlertBox alertBox= generalFunc.showGeneralMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));
                        alertBox.setBtnClickList(new GenerateAlertBox.HandleAlertBtnClick() {
                            @Override
                            public void handleBtnClick(int btn_id) {
                            }
                        });*/

                    }

                } else {
                    generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();
    }
    private void getCategoryData() {

        HashMap<String, String> parameters = new HashMap<String, String>();
        //  parameters.put("type", "getProductCategoryData");
        parameters.put("type", "getProductOptions");
        parameters.put("product_id", getIntent().getStringExtra("product_id"));
        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(String responseString) {

                if (responseString != null && !responseString.equals(""))
                {
                    int c=0,s=0;
                    if (generalFunc.getJsonValue(Utils.action_str, responseString).equals("1")) {
                        JSONArray msgArr = generalFunc.getJsonArray(Utils.message_str, responseString);
                        if (msgArr != null) {
                            for (int i = 0; i < msgArr.length(); i++) {
                                JSONObject obj_temp = generalFunc.getJsonObject(msgArr, i);
                                ProductCategoriesData data = new ProductCategoriesData();
                                if(!generalFunc.getJsonValue("option_name", obj_temp).equalsIgnoreCase("Checkbox")) {
                                    data.setOption_value_name(generalFunc.getJsonValue("option_value_name", obj_temp));
                                    data.setPrice_prefix(generalFunc.getJsonValue("price_prefix", obj_temp));
                                    data.setOption_name(generalFunc.getJsonValue("option_name", obj_temp));
                                    data.setPrice(generalFunc.getJsonValue(ProductCategoriesData.s.price, obj_temp));
                                    data.setProduct_option_id(generalFunc.getJsonValue("product_option_id", obj_temp));
                                    data.setProduct_option_value_id(generalFunc.getJsonValue("product_option_value_id", obj_temp));
                                    if (c == 0) {

                                        itemsize.add("select size");
                                        itemcolor.add("select color");

                                    }
                                    if (generalFunc.getJsonValue("option_name", obj_temp).equalsIgnoreCase("Select")) {
                                        //  color[c]=generalFunc.getJsonValue("option_value_name", obj_temp)+"("+data.getPrice_prefix()+""+Utils.formatPrice(data.getPrice())+")";
                                        itemcolor.add(generalFunc.getJsonValue("option_value_name", obj_temp) + "(" + data.getPrice_prefix() + "$" + (data.getPrice()) + ")");

                                    } else {
                                        //size[s]=generalFunc.getJsonValue("option_value_name", obj_temp)+"("+data.getPrice_prefix()+""+Utils.formatPrice(data.getPrice())+")";
                                        itemsize.add(generalFunc.getJsonValue("option_value_name", obj_temp) + "(" + data.getPrice_prefix() + "$" +(data.getPrice()) + ")");
                                    }
                                    c=c+1;
                                }

                             /*   data.setId(generalFunc.getJsonValue(ProductCategoriesData.s.id, obj_temp));
                                data.setProduct_id(generalFunc.getJsonValue(ProductCategoriesData.s.product_id, obj_temp));
                                data.setSize(generalFunc.getJsonValue(ProductCategoriesData.s.size, obj_temp));
                                data.setColor(generalFunc.getJsonValue(ProductCategoriesData.s.color, obj_temp));
                                data.setPrice(generalFunc.getJsonValue(ProductCategoriesData.s.price, obj_temp));
*/
                                list.add(data);
                            }
                            setArray();
                            setArrayAdaper();
                            setNoDataArea(false);
                            setAdapter();

                        } else {
                            setNoDataArea(true);
                        }
                    } else {
                        setNoDataArea(true);
                    }
                } else {
                    generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();
    }
    void setNoDataArea(boolean isNoDataArea) {
        if (isNoDataArea) {
            categoriesListView.setVisibility(View.GONE);
        } else {
            categoriesListView.setVisibility(View.VISIBLE);
        }
    }
    private void setAdapter() {
        AdapterRecycler adapter = new AdapterRecycler(this, list, R.layout.design_product_category_data_list);
        categoriesList.setAdapter(adapter);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        categoriesList.setLayoutManager(mLayoutManager);

        adapter.setOnBind(new AdapterRecycler.OnBind() {
            @Override
            public void onBindViewHolder(View view, int position) {

                TextView size = view.findViewById(R.id.size);
                TextView color = view.findViewById(R.id.color);
                TextView price = view.findViewById(R.id.price);
                View container = view.findViewById(R.id.container);

                ProductCategoriesData data = (ProductCategoriesData) list.get(position);


                size.setText(data.getOption_name());
                color.setText(data.getOption_value_name());
                price.setText(data.getPrice_prefix()+"$"+(data.getPrice()));

                container.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v)
                    {
                        final GenerateAlertBox generateAlert = new GenerateAlertBox(getActContext());
                        generateAlert.setCancelable(false);
                        generateAlert.setBtnClickList(new GenerateAlertBox.HandleAlertBtnClick() {
                            @Override
                            public void handleBtnClick(int btn_id)
                            {
                                if (btn_id == 1)
                                {
                                    generateAlert.closeAlertBox();
                                    addItemToCart(false);
                                } else if (btn_id == 0) {
                                    generateAlert.closeAlertBox();
                                }
                            }
                        });
                        generateAlert.setContentMessage("Confirm", "Are you sure, you want to add selected product to cart?");
                        generateAlert.setPositiveBtn("YES");
                        generateAlert.setNegativeBtn("NO");
                        generateAlert.showAlertBox();
                    }
                });
            }
        });
    }

    private void setArrayAdaper()
    {
        ArrayAdapter<String> adpater_color;
        adpater_color = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, color);
        choice_type.setAdapter(adpater_color);

        adpater_items = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, size);
        spinner_data.setAdapter(adpater_items);
    }
    private void setArray() {

        color=new String[itemcolor.size()];
        for(int i=0;i<itemcolor.size();i++)
            color[i]=itemcolor.get(i);
        size=new String[itemsize.size()];
        for(int i=0;i<itemsize.size();i++)
            size[i]=itemsize.get(i);

        if(color.length==1)
            choice_type.setVisibility(View.GONE);
        if(size.length==1)
            spinner_data.setVisibility(View.GONE);

    }

    private boolean checkOptopnSelect()
    {
        if(color.length>0||size.length>0)
        {
            if(color.length>1)
            {
                if(option_id_color.equals(""))
                    return false;
            }
            if(size.length>1)
            {
                if(option_id_size.equals(""))
                    return false;
            }
        }
        return true;
    }
    public void printDifference(Date startDate, Date endDate){

        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        System.out.println("startDate : " + startDate);
        System.out.println("endDate : "+ endDate);
        System.out.println("different : " + different);

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        //long elapsedDays = different / daysInMilli;
        //different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        System.out.printf(
                "%d hours, %d minutes, %d seconds%n",
                elapsedHours, elapsedMinutes, elapsedSeconds);

        day.setText(""+((elapsedHours/24))+"\nDays");
        elapsedHours=elapsedHours%24;
        hour.setText(elapsedHours+"\nHours");
        minute.setText(elapsedMinutes+"\nMin");
        second.setText(elapsedSeconds+"\nSec");

        Log.d("databound",elapsedHours+""+ elapsedMinutes+""+elapsedSeconds);


    }
    public void runThred(final String date_end_format)
    {
        Log.d("databound","callrun"+date_end_format);
        this.date_end_format=date_end_format;
        timerHandler.postDelayed(timerRunnable, 0);


    }
    String date_end_format="";
    final Handler timerHandler = new Handler();
    Runnable timerRunnable = new Runnable() {

        @Override
        public void run()
        {
            Calendar c = Calendar.getInstance();
            System.out.println("Current time => "+c.getTime());

            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String startDate = df.format(c.getTime());
            Log.d("databound","runthread"+date_end_format+"  "+startDate);
            try {
                Date end_Date= df.parse(date_end_format+" 00:00:00");
                Date start_date= df.parse(startDate);
                //Log.d("databound12","runthread"+startDate.toString()+"  "+end_Date.);
                printDifference(start_date,end_Date);
            } catch (ParseException e) {
                Log.d("databound","runthreaderror"+e.getMessage());
                e.printStackTrace();
            }
            timerHandler.postDelayed(this, 500);

        }
    };

}