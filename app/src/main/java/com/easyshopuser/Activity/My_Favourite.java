package com.easyshopuser.Activity;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.easyshopuser.Adapter.Favourite_Adapter;
import com.easyshopuser.R;
import com.easyshopuser.generalfiles.ExecuteWebServerUrl;
import com.easyshopuser.generalfiles.GeneralFunctions;
import com.easyshopuser.generalfiles.Utils;
import com.easyshopuser.view.GenerateAlertBox;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class My_Favourite extends AppCompatActivity implements Favourite_Adapter.OnItemClickListener {
    RecyclerView fav_list;
    Favourite_Adapter adapter;
    ImageView pd_back;
    TextView po_title;
    GeneralFunctions generalFunc;
    ArrayList<HashMap<String, String>> dataList = new ArrayList<>();
    View empty_state;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my__favourite);


        initialize();

        pd_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        po_title.setText("My Wishlist");
    }

    public void initialize() {
// generalFunc = new GeneralFunctions(this);

        fav_list = findViewById(R.id.fav_list);
        po_title = findViewById(R.id.po_title);
        pd_back = findViewById(R.id.pd_back);
        generalFunc=new GeneralFunctions(this);

        empty_state=findViewById(R.id.empty_state);
        @SuppressLint("WrongConstant") GridLayoutManager layoutManager=new GridLayoutManager(getApplicationContext(),2, GridLayoutManager.VERTICAL,false);
        fav_list.setLayoutManager(layoutManager);
        adapter = new Favourite_Adapter(this,dataList);//, dataList
        fav_list.setAdapter(adapter);
        adapter.setOnItemClickListener(this);
        adapter.notifyDataSetChanged();
        getFavouriteList();

    }

    @Override
    public void onItemClickList(View v, final int position, int btnType, int qty) {


        if(btnType==1)
        {
/*
            final GenerateAlertBox generateAlert = new GenerateAlertBox(My_Favourite.this);
            generateAlert.setCancelable(false);
            generateAlert.setBtnClickList(new GenerateAlertBox.HandleAlertBtnClick() {
                @Override
                public void handleBtnClick(int btn_id) {
                    if (btn_id == 1) {

                        generateAlert.closeAlertBox();
                    } else if (btn_id == 0) {
                        generateAlert.closeAlertBox();
                    }
                }
            });
            generateAlert.setContentMessage("Confirm", "Do you want to delete the product from your wishlist?");
            generateAlert.setPositiveBtn("YES");
            generateAlert.setNegativeBtn("NO");
            generateAlert.showAlertBox();
*/


            AlertDialog.Builder properties = new AlertDialog.Builder(My_Favourite.this);
            properties.setMessage("Do you want to delete the product from your wishlist?");
            properties.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    addItemToWishList(position);

                }
            });
            properties.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            AlertDialog alertDialog = properties.create();
            alertDialog.show();
            alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorPrimary));
            alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimary));
        }

    }



    public void getFavouriteList()
    {
        dataList.clear();
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("type", "getUserWishList");
        parameters.put("customer_id",generalFunc.getMemberId());



        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(My_Favourite.this, true, generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(final String responseString) {
                Utils.printLog("ResponseData", "Data::" + responseString);
                if (responseString != null && !responseString.equals(""))
                {
                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);
                    if (isDataAvail)
                    {
                        JSONArray msgArr = generalFunc.getJsonArray(Utils.message_str, responseString);

                        if (msgArr != null)
                        {
                            for (int i = 0; i < msgArr.length(); i++) {
                                JSONObject obj_service = generalFunc.getJsonObject(msgArr, i);
                                HashMap<String, String> dataMap_products = new HashMap<>();
                                dataMap_products.put("name",generalFunc.getJsonValue("name", String.valueOf(obj_service)));
                                dataMap_products.put("description",generalFunc.getJsonValue("description", String.valueOf(obj_service)));
                                dataMap_products.put("image",generalFunc.getJsonValue("image", String.valueOf(obj_service)));
                                dataMap_products.put("quantity",generalFunc.getJsonValue("quantity", String.valueOf(obj_service)));
                                dataMap_products.put("price",generalFunc.getJsonValue("price", String.valueOf(obj_service)));
                                dataMap_products.put("product_id",generalFunc.getJsonValue("product_id", String.valueOf(obj_service)));
                                dataList.add(dataMap_products);
                            }
                            adapter.notifyDataSetChanged();

                        }
                    }
                    else {

                        if(dataList.size()==0)
                        {
                            adapter.notifyDataSetChanged();
                            fav_list.setVisibility(View.GONE);
                            empty_state.setVisibility(View.VISIBLE);
                        }

// generalFunc.showGeneralMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));
                    }
                } else
                {
                    generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();
    }

    public void addItemToWishList(int position) {
        GeneralFunctions generalFunctions = new GeneralFunctions(this);
        if (!generalFunctions.isUserLoggedIn()) {
//generalFunctions.startActivity(AppLoginActivity.class);
            return;
        }
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("type", "addProductToWishList");
        parameters.put("product_id", dataList.get(position).get("product_id"));
        parameters.put("customer_id", "" + generalFunc.getMemberId());

        Utils.printLog("WishListParameters::", "::" + parameters.toString());
        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(My_Favourite.this, true, generalFunc);
        exeWebServer.setIsDeviceTokenGenerate(true, "vDeviceToken");
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(final String responseString) {

                Utils.printLog("ResponseData", "Data::" + responseString);

                if (responseString != null && !responseString.equals("")) {
                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);


                    Toast.makeText(My_Favourite.this, "" + generalFunc.getJsonValue(Utils.message_str, responseString), Toast.LENGTH_SHORT).show();
                    getFavouriteList();
                  /*  if (isDataAvail) {
                        GenerateAlertBox alertBox= generalFunc.showGeneralMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));
                        alertBox.setBtnClickList(new GenerateAlertBox.HandleAlertBtnClick() {
                            @Override
                            public void handleBtnClick(int btn_id) {
                                if(btn_id==1)

                            }
                        });
*/

                } else {
                    generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();
    }

}
