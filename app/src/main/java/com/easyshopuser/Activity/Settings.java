package com.easyshopuser.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.easyshopuser.R;

public class Settings extends AppCompatActivity {
    ImageView pd_back;
    TextView po_title;
    LinearLayout termsandcondition,contackus;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        initialize();
        pd_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        po_title.setText("Setting");

        termsandcondition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(Settings.this,Termsandcondition.class);
                startActivity(intent);
                finish();
            }
        });

        contackus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Settings.this,Contackus.class);
                startActivity(intent);
                finish();
            }
        });

    }
    public void initialize()
    {
        pd_back=findViewById(R.id.pd_back);
        po_title=findViewById(R.id.po_title);
        termsandcondition=findViewById(R.id.termsandcondition);
        contackus=findViewById(R.id.contackus);
    }

}

