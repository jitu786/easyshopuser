package com.easyshopuser.Activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.easyshopuser.Adapter.Message_Adapter;
import com.easyshopuser.R;
import com.easyshopuser.generalfiles.ExecuteWebServerUrl;
import com.easyshopuser.generalfiles.GeneralFunctions;
import com.easyshopuser.generalfiles.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class My_Message  extends AppCompatActivity {

    RecyclerView message_list;
    Message_Adapter adapter;
    TextView po_title;
    String response;
    ImageView pd_back;
    GeneralFunctions generalFunc;
    ArrayList<HashMap<String, String>> dataList = new ArrayList<>();
    LinearLayout data;
    LinearLayout null_data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my__message);
        initialize();
        po_title.setText("My Messages");
        pd_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    public void initialize()
    { generalFunc=new GeneralFunctions(this);

        data=findViewById(R.id.data);
        null_data=findViewById(R.id.null_data);
        pd_back=findViewById(R.id.pd_back);
        po_title=findViewById(R.id.po_title);
        po_title.setText("My Messages");
        pd_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        message_list=findViewById(R.id.message_list);
        @SuppressLint("WrongConstant") LinearLayoutManager layoutManager = new LinearLayoutManager(My_Message.this, LinearLayoutManager.VERTICAL, false);
        message_list.setLayoutManager(layoutManager);
        adapter = new Message_Adapter(this,dataList);
        message_list.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        getMessages();
    }
    public void getMessages()
    {
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("type", "getMessages");
        //parameters.put("customer_id","11");
        parameters.put("customer_id",generalFunc.getMemberId());
        Log.e("cust_id",""+generalFunc.getMemberId());



        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(My_Message.this, true, generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(final String responseString) {
                Utils.printLog("ResponseData", "Data::" + responseString);
                if (responseString != null && !responseString.equals("")) {
                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);
                    if (isDataAvail)
                    {
                        JSONArray msgArr = generalFunc.getJsonArray(Utils.message_str, responseString);
                        dataList.clear();
                        if (msgArr != null)
                        {
                            for (int i = 0; i < msgArr.length(); i++) {
                                JSONObject obj_service = generalFunc.getJsonObject(msgArr, i);
                                HashMap<String, String> dataMap_products = new HashMap<>();
                                dataMap_products.put("message_id",generalFunc.getJsonValue("message_id", String.valueOf(obj_service)));
                                dataMap_products.put("customer_id",generalFunc.getJsonValue("customer_id", String.valueOf(obj_service)));
                                dataMap_products.put("title",generalFunc.getJsonValue("title", String.valueOf(obj_service)));
                                dataMap_products.put("message",generalFunc.getJsonValue("message", String.valueOf(obj_service)));
                                dataMap_products.put("date",generalFunc.getJsonValue("date", String.valueOf(obj_service)));
                                dataList.add(dataMap_products);
                            }
                            adapter.notifyDataSetChanged();

                        }
                    }
                    else
                    if (dataList.size()==0)
                    {
                        data.setVisibility(View.GONE);
                        null_data.setVisibility(View.VISIBLE);


                    }
                    //generalFunc.showGeneralMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));
                } else
                {
                    // generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();


    }
}