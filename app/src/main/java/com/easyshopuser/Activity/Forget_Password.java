package com.easyshopuser.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.easyshopuser.R;
import com.easyshopuser.generalfiles.ExecuteWebServerUrl;
import com.easyshopuser.generalfiles.GeneralFunctions;
import com.easyshopuser.generalfiles.Utils;
import com.easyshopuser.view.GenerateAlertBox;

import java.util.HashMap;

public class Forget_Password  extends AppCompatActivity {

    TextView po_title;
    ImageView pd_back;
    Button reset_password;
    EditText email;
    GeneralFunctions generalFunc;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget__password);

        initialize();
        reset_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (chekvalidation())
                {
                    sendotp();
                }
            }
        });

        pd_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

    }

    private boolean chekvalidation() {

        if (email.getText().toString().isEmpty())
        {
            email.setError("Email Id Is Required");
            return false;
        }
        return true;
    }
    public void initialize()
    {
        reset_password=findViewById(R.id.reset_password);
        pd_back=findViewById(R.id.pd_back);
        po_title=findViewById(R.id.po_title);
        po_title.setText("Forgot Password");
        email=findViewById(R.id.email);
        generalFunc=new GeneralFunctions(Forget_Password.this);
    }

    private void sendotp() {

        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("type", "sendResetPassword");
        parameters.put("vEmail",email.getText().toString() );

        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(final String responseString) {
                Utils.printLog("ResponseData", "Data::" + responseString);
                if (responseString != null && !responseString.equals("")) {
                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);
                    if (isDataAvail) {
                        GenerateAlertBox alertBox= generalFunc.showGeneralMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));
                        alertBox.setBtnClickList(new GenerateAlertBox.HandleAlertBtnClick() {
                            @Override
                            public void handleBtnClick(int btn_id) {
                                if(btn_id==1)
                                {
                                    Toast.makeText(Forget_Password.this, ""+generalFunc.getJsonValue("VerificationCode",responseString), Toast.LENGTH_SHORT).show();
                                    Intent intent=new Intent(getActContext(), NewPassword.class);
                                    intent.putExtra("VerificationCode",generalFunc.getJsonValue("VerificationCode",responseString));
                                    intent.putExtra("email",email.getText().toString());
                                    getActContext().startActivity(intent);
                                }
                            }
                        });
                    }
                    else
                        generalFunc.showGeneralMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));

                } else {
                    generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();
    }

    private Context getActContext() {

        return Forget_Password.this;
    }
}
