package com.easyshopuser.Activity;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.easyshopuser.Adapter.Cart_Adapter;
import com.easyshopuser.R;
import com.easyshopuser.generalfiles.ExecuteWebServerUrl;
import com.easyshopuser.generalfiles.GeneralFunctions;
import com.easyshopuser.generalfiles.Utils;
import com.easyshopuser.view.GenerateAlertBox;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class Add_To_Cart  extends AppCompatActivity implements Cart_Adapter.OnItemClickListener{

    ImageView pd_back;
    RecyclerView cart_list;
    Cart_Adapter adapter;
    TextView place_order_cart;
    GeneralFunctions generalFunc;
    ArrayList<HashMap<String, String>> dataList = new ArrayList<>();
    TextView txtTotalPrice;
    LinearLayout place_order,with_data;
    View layout_cart_empty;
    Button shopNowBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add__to__cart);

        initialize();
        place_order_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),Place_Order.class));
            }
        });

        @SuppressLint("WrongConstant") LinearLayoutManager layoutManager = new LinearLayoutManager(Add_To_Cart.this, LinearLayoutManager.VERTICAL, false);
        cart_list.setLayoutManager(layoutManager);
        adapter = new Cart_Adapter(this,dataList);
        cart_list.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        adapter.setOnItemClickListener(this);


        pd_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i2 = new Intent(getApplicationContext(), Dashboard.class);
                i2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i2);
                overridePendingTransition( R.anim.left_to_right, R.anim.right_to_left );

            }
        });

        if(generalFunc.isUserLoggedIn())
            getUserCart();

        shopNowBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(Add_To_Cart.this, Dashboard.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent i2 = new Intent(getApplicationContext(), Dashboard.class);
        i2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i2);
        overridePendingTransition( R.anim.left_to_right, R.anim.right_to_left );
    }

    public void initialize()
    {
        shopNowBtn=findViewById(R.id.shopNowBtn);
        place_order=findViewById(R.id.place_order);
        with_data=findViewById(R.id.with_data);
        layout_cart_empty=findViewById(R.id.layout_cart_empty);
        txtTotalPrice=findViewById(R.id.txtTotalPrice);
        place_order_cart=findViewById(R.id.place_order_cart);
        cart_list=findViewById(R.id.cart_list);
        pd_back=findViewById(R.id.pd_back);
        generalFunc=new GeneralFunctions(this);

    }

    public void getUserCart()
    {
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("type", "getUserCart");
        parameters.put("customer_id", generalFunc.getMemberId());
        Log.d("cartData",parameters.toString());
        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
//        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.setIsDeviceTokenGenerate(true, "vDeviceToken");
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(final String responseString) {

                Utils.printLog("ResponseData", "Data::" + responseString);


                if (responseString != null && !responseString.equals("")) {

                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);

                    dataList.clear();
                    adapter.notifyDataSetChanged();
                    if (isDataAvail) {
                        JSONArray msgArr = generalFunc.getJsonArray(Utils.message_str, responseString);

                        dataList.clear();
                        adapter.notifyDataSetChanged();

                        //                   layout_payment.setVisibility(View.VISIBLE);
                        txtTotalPrice.setText("Rs "+(generalFunc.getJsonValue("TotalPrice", responseString)));

                        if (msgArr != null) {

                            for (int i = 0; i < msgArr.length(); i++) {
                                JSONObject obj_cart = generalFunc.getJsonObject(msgArr, i);

                                String productImg = generalFunc.getJsonValue("image", obj_cart);
                                String productName = generalFunc.getJsonValue("name", obj_cart);
                                String productDes = generalFunc.getJsonValue("description", obj_cart);
                                String productId = generalFunc.getJsonValue("product_id", obj_cart);
                                String price = generalFunc.getJsonValue("price", obj_cart);
                                String category_id = generalFunc.getJsonValue("category_id", obj_cart);
                                String cart_id = generalFunc.getJsonValue("cart_id", obj_cart);
                                String quantity = generalFunc.getJsonValue("quantity", obj_cart);
                                String option = generalFunc.getJsonValue("option", obj_cart);
                                String product_option_name_color = generalFunc.getJsonValue("product_option_name_color", obj_cart);
                                String product_option_value_color = generalFunc.getJsonValue("product_option_value_color", obj_cart);
                                String product_option_name_size = generalFunc.getJsonValue("product_option_name_size", obj_cart);
                                String product_option_value_size = generalFunc.getJsonValue("product_option_value_size", obj_cart);

                                Utils.printLog("quantity", "::" + quantity);
                                HashMap<String, String> dataMap_products = new HashMap<>();
                                dataMap_products.put("name", productName);
                                dataMap_products.put("cart_id", cart_id);
                                dataMap_products.put("category_id", category_id);
                                dataMap_products.put("product_id", productId);
                                dataMap_products.put("price", price);
                                dataMap_products.put("description", Utils.html2text(productDes));
                                // dataMap_products.put("description",productDes);
                                dataMap_products.put("image", productImg);
                                dataMap_products.put("quantity", quantity);
                                //        dataMap_products.put("TYPE", "" + CartRecyclerAdapter.TYPE_ITEM);
                                dataMap_products.put("option", option);
                                dataMap_products.put("product_option_name_color", product_option_name_color);
                                dataMap_products.put("product_option_value_color", product_option_value_color);
                                dataMap_products.put("product_option_name_size", product_option_name_size);
                                dataMap_products.put("product_option_value_size", product_option_value_size);


                                dataList.add(dataMap_products);

                            }
                            adapter.notifyDataSetChanged();
                        }
                    } else {
                        if(dataList.size()==0)
                        {
                            with_data.setVisibility(View.GONE);
                            place_order.setVisibility(View.GONE);
                            layout_cart_empty.setVisibility(View.VISIBLE);
                        }
                        //  layout_cart_empty.setVisibility(View.VISIBLE);
                    }
                    //closeLoader();
                } else {
                    //generateErrorView();
                }

            }
        });
        exeWebServer.execute();

    }

    @Override
    public void onItemClickList(View v, final int position, int btnType, int qty) {
        if(btnType==1)
        {
/*
            final GenerateAlertBox generateAlert = new GenerateAlertBox(Add_To_Cart.this);
            generateAlert.setCancelable(false);
            generateAlert.setBtnClickList(new GenerateAlertBox.HandleAlertBtnClick() {
                @Override
                public void handleBtnClick(int btn_id) {
                    if (btn_id == 1) {
                        deleteItemFromCart(position);
                        generateAlert.closeAlertBox();
                    } else if (btn_id == 0) {
                        generateAlert.closeAlertBox();
                    }
                }
            });
            generateAlert.setContentMessage("Confirm", "Do you want to delete the product from your cart?");
            generateAlert.setPositiveBtn("YES");
            generateAlert.setNegativeBtn("NO");
            generateAlert.showAlertBox();
*/

            AlertDialog.Builder properties = new AlertDialog.Builder(Add_To_Cart.this);
            properties.setMessage("Do you want to delete the product from your cart?");
            properties.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    deleteItemFromCart(position);
                }
            });
            properties.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            AlertDialog alertDialog = properties.create();
            alertDialog.show();
            alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorPrimary));
            alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimary));


        }
        else if(btnType==2)
        {
            moveProductToWishList(position);
        }
        else if(btnType==3)
            changeQuantity(qty,position);
        else if(btnType==4)
        {

            Intent intent=new Intent(this,ProductDescription.class);
            intent.putExtra("product_id",dataList.get(position).get("product_id"));
            startActivity(intent);
        }
    }
    public void deleteItemFromCart(int position) {
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("type", "deleteItemFromCart");
        parameters.put("product_id", dataList.get(position).get("product_id"));
        parameters.put("category_id", dataList.get(position).get("category_id"));
        parameters.put("cart_id", dataList.get(position).get("cart_id"));
        parameters.put("customer_id", "" + generalFunc.getMemberId());

        Utils.printLog("deleteFromCartParameters", "::" + parameters.toString());
        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(this, true, generalFunc);
        exeWebServer.setIsDeviceTokenGenerate(true, "vDeviceToken");
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(final String responseString) {

                Utils.printLog("ResponseData", "Data::" + responseString);

                if (responseString != null && !responseString.equals("")) {
                    Toast.makeText(Add_To_Cart.this, "Product is deleted sucessfully from your cart.", Toast.LENGTH_SHORT).show();
                    // generalFunc.showGeneralMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));
                    getUserCart();
                } else {
                    generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();
    }
    public void moveProductToWishList(int position) {

        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("type", "moveItemToWishList");
        parameters.put("product_id", dataList.get(position).get("product_id"));
        parameters.put("cart_id", dataList.get(position).get("cart_id"));
        parameters.put("customer_id", "" + generalFunc.getMemberId());

        Utils.printLog("changeQuantityParameters", "::" + parameters.toString());
        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(this, true, generalFunc);
        exeWebServer.setIsDeviceTokenGenerate(true, "vDeviceToken");
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(final String responseString) {

                Utils.printLog("ResponseData", "Data::" + responseString);

                if (responseString != null && !responseString.equals("")) {


                    generalFunc.showGeneralMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));
                    //generalFunc.showGeneralMessage("", "Item added to wish list.. ");

                    /*DealsActivity da=new DealsActivity();
                    da.addItemToWishList(dataList.get(deletePosition).get("product_id"),
                            dataList.get(deletePosition).get("category_id"),
                            deletePosition);
                    deleteItemFromCart(deletePosition);
                    //addDrawer.findUserCartCount();*/
                    getUserCart();
                } else {
                    generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();
    }

    public void changeQuantity(int quantity, int position) {
        Utils.printLog("quantity", ":m:" + quantity);
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("type", "changeQuantity");
        parameters.put("product_id", dataList.get(position).get("product_id"));
        parameters.put("category_id", dataList.get(position).get("category_id"));
        parameters.put("cart_id", dataList.get(position).get("cart_id"));
        parameters.put("customer_id", "" + generalFunc.getMemberId());
        parameters.put("quantity", "" + quantity);

        Utils.printLog("changeQuantityParameters", "::" + parameters.toString());
        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(this, true, generalFunc);
        exeWebServer.setIsDeviceTokenGenerate(true, "vDeviceToken");
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(final String responseString) {

                Utils.printLog("ResponseData", "Data::" + responseString);

                if (responseString != null && !responseString.equals("")) {
                    Toast.makeText(Add_To_Cart.this, "Product quantity is updated sucessfully.", Toast.LENGTH_SHORT).show();
                    //  generalFunc.showGeneralMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));

                    getUserCart();
                } else {
                    generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();
    }

}

