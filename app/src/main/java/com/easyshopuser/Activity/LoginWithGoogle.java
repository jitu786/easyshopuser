package com.easyshopuser.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import com.easyshopuser.generalfiles.ExecuteWebServerUrl;
import com.easyshopuser.generalfiles.GeneralFunctions;
import com.easyshopuser.generalfiles.Utils;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.HashMap;

public class LoginWithGoogle implements GoogleApiClient.OnConnectionFailedListener {
    Context mContext;
    GoogleSignInClient googleSignInClient;
    Activity appLoginAct;
    public static GoogleApiClient mGoogleApiClient;
    GeneralFunctions generalFunc;
    String deviceToken;


    public LoginWithGoogle(Context mContext, String deviceToken) {
        this.mContext = mContext;
        appLoginAct = (Activity) mContext;
        this.deviceToken = deviceToken;
        generalFunc = new GeneralFunctions(mContext);

        initializeGoogleLogin();
    }

    public void initializeGoogleLogin() {


        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
//.enableAutoManage(appLoginAct, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        mGoogleApiClient.connect();

        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        appLoginAct.startActivityForResult(signInIntent, Utils.GOOGLE_SIGN_IN_REQ_CODE);
    }

    public void handleSignInResult(GoogleSignInResult result) {


        if (result.isSuccess()) {
            GoogleSignInAccount acct = result.getSignInAccount();


            String firstName = acct.getFamilyName();
            String lastName = acct.getGivenName();
            String email = acct.getEmail();
            String id = acct.getId();



            if (email != null && !email.trim().isEmpty()) {
                registerUser(firstName, lastName, "", "", "", "", email, id);

                //     Toast.makeText(mContext, ""+email, Toast.LENGTH_SHORT).show();
            }
        } else {
            Log.d("google_data", "" + result.getStatus());
        }
    }

    private void registerUser(final String firstName, final String lastName, String s, String s1, String s2, String s3, final String email, String id) {

        Toast.makeText(mContext, ""+email, Toast.LENGTH_SHORT).show();
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("type", "LoginWithSocialAcc");
        parameters.put("firstName", firstName);
        parameters.put("lastName", lastName);
        parameters.put("mobile", s);
        parameters.put("country", s1);
        parameters.put("country_id", s2);
        parameters.put("password", s3);
        parameters.put("email", email);
        parameters.put("AppID", id);
        parameters.put("loginType", "Facebook");
        parameters.put("fb_token", FirebaseInstanceId.getInstance().getToken());//Utils.firebaseToken);

        Log.w("check",parameters.toString());
        Log.d("check",parameters.toString());
        Utils.printLog("ResponseData",parameters.toString());

        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(mContext, true, generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(final String responseString) {
                Utils.printLog("ResponseData", "Data::" + responseString);
                if (responseString != null && !responseString.equals("")) {
                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);

                    if (isDataAvail) {
                        SignOut();
                        if(generalFunc.getJsonValue("password",responseString).equals("")){
                            Bundle bundle=new Bundle();
                            bundle.putString("codeVerification","codeVerification");
                            bundle.putString("userId",generalFunc.getJsonValue(Utils.message_str, responseString));
                            bundle.putString("firstName", firstName);
                            bundle.putString("lastName", lastName);
                            generalFunc.startActivity(Reset_Password.class,bundle);

                        }else {
                            generalFunc.storeUserData(generalFunc.getJsonValue(Utils.message_str, responseString));
                            Toast.makeText(mContext, "You are login sucessfully.", Toast.LENGTH_SHORT).show();
                            Intent intent=new Intent(mContext,Dashboard.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            mContext.startActivity(intent);

                        }
                        ActivityCompat.finishAffinity((Activity) mContext);
                    } else {
                   //     generalFunc.showGeneralMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));
                    }

                } else {
                    generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();

    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d("google_data", "" + connectionResult.getErrorMessage());

    }

    public static void SignOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient);
    }


}

