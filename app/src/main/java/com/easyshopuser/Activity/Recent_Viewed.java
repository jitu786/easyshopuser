package com.easyshopuser.Activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.easyshopuser.Adapter.RecentViewedAdapter;
import com.easyshopuser.R;
import com.easyshopuser.generalfiles.ExecuteWebServerUrl;
import com.easyshopuser.generalfiles.GeneralFunctions;
import com.easyshopuser.generalfiles.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class Recent_Viewed extends AppCompatActivity {
    RecyclerView recent_viewed_list;
    RecentViewedAdapter adapter;
    ImageView pd_back;
    TextView po_title;
    GeneralFunctions generalFunc;
    ArrayList<HashMap<String, String>> dataList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recent__viewed);

        initialize();
    }
    public void initialize()
    {
        generalFunc=new GeneralFunctions(this);

        po_title=findViewById(R.id.po_title);
        pd_back=findViewById(R.id.pd_back);
        pd_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        po_title.setText("Recently Viewed Items");

        recent_viewed_list=findViewById(R.id.recent_viewed_list);
        @SuppressLint("WrongConstant") GridLayoutManager layoutManager_trendingItem=new GridLayoutManager(getApplicationContext(),2, GridLayoutManager.VERTICAL,false);
        recent_viewed_list.setLayoutManager(layoutManager_trendingItem);
        adapter = new RecentViewedAdapter(this,dataList);
        recent_viewed_list.setAdapter(adapter);
        recent_viewed_list.setNestedScrollingEnabled(false);
        adapter.notifyDataSetChanged();
        getRecentViewedList();
    }
    public void getRecentViewedList()
    {

        HashMap<String, String> param = new HashMap<>();
        param.put("type", "getRecentViewedProduct");
        param.put("customer_id", generalFunc.getMemberId());
        param.put("limit","10");
//param.put("country", Utils.getCountryCode(Utils.countryToDisplay));

        ExecuteWebServerUrl executeWebServerUrl = new ExecuteWebServerUrl(param);
        executeWebServerUrl.setLoaderConfig(this,true,generalFunc);
        executeWebServerUrl.execute();
        executeWebServerUrl.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(String responseString) {
                Utils.printLog("ResponseRecent", "RES : " + responseString);

                if (responseString != null && !responseString.equals("")) {


                    JSONArray message = generalFunc.getJsonArray(Utils.message_str, responseString);
                    String action = generalFunc.getJsonValue("action", responseString);

                    if (message.length() > 0) {
                        for (int i = 0; i < message.length(); i++) {
                            JSONObject msgObj = generalFunc.getJsonObject(message, i);
//JSONObject msgObj = generalFunc.getJsonObject();
                            JSONArray wishListDataArr = generalFunc.getJsonArray("UserWishListData", responseString);
                            ArrayList<String> wishListProductIdsList = new ArrayList<>();
                            if (wishListDataArr != null) {
                                for (int j = 0; j < wishListDataArr.length(); j++) {

                                    JSONObject obj_temp = generalFunc.getJsonObject(wishListDataArr, j);
                                    wishListProductIdsList.add(generalFunc.getJsonValue("product_id", String.valueOf(obj_temp)));
                                }
                            }
                            if (msgObj != null) {

                                HashMap<String, String> dataMap_products = new HashMap<>();
                                String name= generalFunc.getJsonValue("name", String.valueOf(msgObj));
                                String description= generalFunc.getJsonValue("description", String.valueOf(msgObj));
                                String product_id=generalFunc.getJsonValue("product_id", String.valueOf(msgObj));
                                String image=generalFunc.getJsonValue("image", String.valueOf(msgObj));
                                String shipping=generalFunc.getJsonValue("shipping", String.valueOf(msgObj));
                                String points=generalFunc.getJsonValue("points", String.valueOf(msgObj));
                                String weight=generalFunc.getJsonValue("weight", String.valueOf(msgObj));
                                String viewed=generalFunc.getJsonValue("viewed", String.valueOf(msgObj));
                                String stock_status_id=generalFunc.getJsonValue("stock_status_id", String.valueOf(msgObj));
                                String price = generalFunc.getJsonValue("price", String.valueOf(msgObj));
                                String seller_id=generalFunc.getJsonValue("seller_id", String.valueOf(msgObj));
                                String seller_name=generalFunc.getJsonValue("seller_name", String.valueOf(msgObj));
                                String store_country=generalFunc.getJsonValue("store_country", String.valueOf(msgObj));
                                String special_price=generalFunc.getJsonValue("special_price", String.valueOf(msgObj));
                                String TotalRating = generalFunc.getJsonValue("TotalRating",String.valueOf(msgObj));
                                String TotalRatingCount = generalFunc.getJsonValue("TotalRatingCount",String.valueOf(msgObj));


                                dataMap_products.put("name", name);
                                dataMap_products.put("description", description);
                                dataMap_products.put("product_id", product_id);
                                dataMap_products.put("image", image);
                                dataMap_products.put("shipping", shipping);
                                dataMap_products.put("points", points);
                                dataMap_products.put("weight", weight);
                                dataMap_products.put("viewed", viewed);
                                dataMap_products.put("stock_status_id", stock_status_id);
                                dataMap_products.put("price", price);
                                dataMap_products.put("seller_id", seller_id);
                                dataMap_products.put("seller_name", seller_name);
                                dataMap_products.put("store_country", store_country);
                                dataMap_products.put("special_price", special_price);
                                dataMap_products.put("TotalRating", TotalRating);
                                dataMap_products.put("TotalRatingCount", TotalRatingCount);

                                dataList.add(dataMap_products);

                            }
                        }
                        Utils.printLog("ListData", dataList.toString());

                    }
                }

                adapter.notifyDataSetChanged();
            }
        });



    }
}