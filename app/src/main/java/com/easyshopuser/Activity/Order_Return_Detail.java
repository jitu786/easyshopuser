package com.easyshopuser.Activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.easyshopuser.R;

public class Order_Return_Detail extends AppCompatActivity {
    ImageView pd_back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order__return__detail);

        pd_back=findViewById(R.id.pd_back);
// initialize();
        pd_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

}
