package com.easyshopuser.Activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.easyshopuser.Adapter.Buy_Now_Adapter;
import com.easyshopuser.R;
import com.easyshopuser.generalfiles.ExecuteWebServerUrl;
import com.easyshopuser.generalfiles.GeneralFunctions;
import com.easyshopuser.generalfiles.Utils;
import com.easyshopuser.helper.StartActProcess;
import com.easyshopuser.view.GenerateAlertBox;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.easyshopuser.generalfiles.Utils.closeLoader;

public class Place_Order extends AppCompatActivity {

    RecyclerView buy_now_list;
    TextView po_title,confirm_order,txttotal,txtgrandtotal,txtshiping_rate,txtname,txtaddress,txtpayment;
    ImageView pd_back;
    Buy_Now_Adapter adapter;
    float shippingRate=0;
    LinearLayout user_detail,payment_type;
    GeneralFunctions generalFunc;
    ArrayList<HashMap<String, String>> dataList = new ArrayList<>();
    public static List<Boolean> isSpecialProduct = new ArrayList<>();
    HashMap<String, String> selectedAddressData = null;
    String payment_id="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place__order);

        initialize();

        confirm_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkData();
            }
        });

        payment_type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(selectedAddressData!=null) {
                    selectedAddressData.put("amount",txtgrandtotal.getText().toString().replace("Rs",""));
                    Intent intent = new Intent(Place_Order.this, Payment_Method.class);
                    intent.putExtra("DATA", selectedAddressData);
                    startActivityForResult(intent, Utils.PAYMENT_CODE);
                }
                else
                {
                    generalFunc.showGeneralMessage("", "Please select address");
                }

            }
        });



        @SuppressLint("WrongConstant") LinearLayoutManager layoutManager = new LinearLayoutManager(Place_Order.this, LinearLayoutManager.VERTICAL, false);
        buy_now_list.setLayoutManager(layoutManager);
        adapter = new Buy_Now_Adapter(this,dataList);
        buy_now_list.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        user_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent int_edit_address = new Intent(Place_Order.this, Shipping_Address.class);

                startActivityForResult(int_edit_address, Utils.ADD_ADDRESS_REQ_CODE);
            }
        });
        setShippingMethod();
    }

    public void checkData() {
        if (selectedAddressData == null) {
            generalFunc.showGeneralMessage("", "Please select address");
            return;
        }
        if(payment_id.isEmpty())
        {
            generalFunc.showGeneralMessage("","Please select payment");
            return;
        }

        MakePaymentFunction();
    }

    private void
    MakePaymentFunction() {
//        Toast.makeText(this, "Total Price " + totalPrice, Toast.LENGTH_SHORT).show();
        final GenerateAlertBox alertBox = new GenerateAlertBox(this);
        alertBox.setPositiveBtn("Confirm");
        alertBox.setNegativeBtn("Cancel");
        alertBox.setContentMessage("Alert", "Do you want to confirm the order?");
        alertBox.setBtnClickList(new GenerateAlertBox.HandleAlertBtnClick() {
            @Override
            public void handleBtnClick(int btn_id) {
                if (btn_id == 1) {
                    alertBox.closeAlertBox();
                    createOrder();
                } else if (btn_id == 0) {
                    alertBox.closeAlertBox();
                }
            }
        });
        alertBox.showAlertBox();

    }
    public void initialize()
    {
        txtname=findViewById(R.id.txtname);
        txtaddress=findViewById(R.id.txtaddress);
        txtpayment=findViewById(R.id.txtpayment);
        txttotal=findViewById(R.id.txttotal);
        txtgrandtotal=findViewById(R.id.txtgrandtotal);
        generalFunc=new GeneralFunctions(this);
        confirm_order=findViewById(R.id.confirm_order);
        payment_type=findViewById(R.id.payment_type);
        buy_now_list=findViewById(R.id.buy_now_list);
        user_detail=findViewById(R.id.user_detail);
        txtshiping_rate=findViewById(R.id.txtshiping_rate);

        pd_back=findViewById(R.id.pd_back);
        pd_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        po_title=findViewById(R.id.po_title);
        po_title.setText("Place Order");

        txtname.setText("Select Your Address");
        txtaddress.setVisibility(View.GONE);
    }

    public void getUserCart() {
        dataList.clear();
        adapter.notifyDataSetChanged();

        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("type", "getUserCart");
        parameters.put("customer_id", generalFunc.getMemberId());

        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
//        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.setIsDeviceTokenGenerate(true, "vDeviceToken");
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(final String responseString) {

                Utils.printLog("ResponseData", "Data::" + responseString);
                if (responseString != null && !responseString.equals("")) {


                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);

                    dataList.clear();
                    adapter.notifyDataSetChanged();
                    if (isDataAvail) {
                        JSONArray msgArr = generalFunc.getJsonArray(Utils.message_str, responseString);

                        dataList.clear();
                        adapter.notifyDataSetChanged();

                        //String totalPriceString=Utils.formatPrice(generalFunc.getJsonValue("TotalPrice", responseString));

                        txttotal.setText("Rs "+generalFunc.getJsonValue("TotalPriceOrig", responseString));
                        float actualTotalPrice = Float.parseFloat(generalFunc.getJsonValue("TotalPriceOrig", responseString));
                        txtgrandtotal.setText("Rs "+(actualTotalPrice + (int)shippingRate));

                        if (msgArr != null) {

                            isSpecialProduct.clear();

                            for (int i = 0; i < msgArr.length(); i++) {
                                JSONObject obj_cart = generalFunc.getJsonObject(msgArr, i);

                                String productImg = generalFunc.getJsonValue("image", obj_cart);
                                String productName = generalFunc.getJsonValue("name", obj_cart);
                                String productDes = generalFunc.getJsonValue("description", obj_cart);
                                String productId = generalFunc.getJsonValue("product_id", obj_cart);
                                String price = generalFunc.getJsonValue("price", obj_cart);
//                                String category_id = generalFunc.getJsonValue("category_id", obj_cart);
                                String cart_id = generalFunc.getJsonValue("cart_id", obj_cart);
                                String quantity = generalFunc.getJsonValue("quantity", obj_cart);
                                String special = generalFunc.getJsonValue("special", obj_cart);
                                String category_data_id = generalFunc.getJsonValue("category_data_id", obj_cart);
                                String size = generalFunc.getJsonValue("size", obj_cart);
                                String color = generalFunc.getJsonValue("color", obj_cart);

                                if (special.equals("yes")) {
                                    isSpecialProduct.add(true);
                                } else {
                                    isSpecialProduct.add(false);
                                }


                                Utils.printLog("quantity", "::" + quantity);
                                HashMap<String, String> dataMap_products = new HashMap<>();
                                dataMap_products.put("name", productName);
                                dataMap_products.put("cart_id", cart_id);
//                                dataMap_products.put("category_id", category_id);
                                dataMap_products.put("product_id", productId);
                                dataMap_products.put("price", price);
                                dataMap_products.put("description", Utils.html2text(productDes));
                                dataMap_products.put("image", productImg);
                                dataMap_products.put("quantity", quantity);
                                dataMap_products.put("category_data_id", category_data_id);
                                dataMap_products.put("size", size);
                                dataMap_products.put("color", color);

                                dataList.add(dataMap_products);

                            }
                            // createProductCountryList();

                            //loading.setVisibility(View.GONE);
                            adapter.notifyDataSetChanged();
                            //layout_payment.setVisibility(View.VISIBLE);
                        }
                    } else {
                        generalFunc.showGeneralMessage("", "Please try again later");
                    }
                    closeLoader();
                } else {
                    generalFunc.showGeneralMessage("", "Please try again later");
                }
            }
        });
        exeWebServer.execute();
    }
    private void setShippingMethod() {
        HashMap<String, String> param = new HashMap<>();
        param.put("type", "getShippingMethod");
        param.put("customer_id", generalFunc.getMemberId());

        ExecuteWebServerUrl executeWebServerUrl = new ExecuteWebServerUrl(param);
        executeWebServerUrl.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(String responseString) {
                if (!responseString.equals("") && responseString != null) {
                    boolean isDataValid = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);

                    if (isDataValid) {
                        shippingRate = Float.valueOf(generalFunc.getJsonValue(Utils.message_str, responseString));
                        txtshiping_rate.setText("Rs."+shippingRate);
                        getUserCart();
                    } else {
                        generalFunc.showGeneralMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));
                    }
                } else {
                    generalFunc.showError();
                }
            }
        });
        executeWebServerUrl.execute();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Utils.ADD_ADDRESS_REQ_CODE && resultCode == RESULT_OK) {
            txtaddress.setVisibility(View.VISIBLE);
            selectedAddressData = (HashMap<String, String>) data.getSerializableExtra("DATA");
            String Address1 =  "address1: "+ selectedAddressData.get("address_1") + " <br /> " + "address2: " +
                    selectedAddressData.get("address_2") + " <br /> " + "state: " + selectedAddressData.get("state_name") + "<br/>"
                    + "Country: " + selectedAddressData.get("country_name")+"<br/>"
                    + "Postcode: " + selectedAddressData.get("postcode")+"<br/>";

            txtaddress.setText(Html.fromHtml(generalFunc.capFully(Address1)));
            txtname.setText(selectedAddressData.get("firstname") + " " + selectedAddressData.get("lastname") );
        }
        else if(requestCode==Utils.PAYMENT_CODE&&resultCode==RESULT_OK)
        {
            Toast.makeText(this, "Your payment is received sucessfully", Toast.LENGTH_SHORT).show();
            payment_id=data.getStringExtra("payment_id");
            txtpayment.setText("Sucessfully paid select paymnet");
            Log.d("payment_id",payment_id);
        }
    }
    public void createOrder() {
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("type", "createOrder");
        parameters.put("customer_id", generalFunc.getMemberId());
        parameters.put("address_id", selectedAddressData.get("address_id"));
        parameters.put("PAYMENT_OPTION", "Online Payment");
        parameters.put("PAYMENT_CODE","Online");
        parameters.put("coupon", "");
        parameters.put("payment_id", payment_id);
        parameters.put("price", txtgrandtotal.getText().toString().replace("Rs",""));
        parameters.put("shipping_rate", String.valueOf(shippingRate));
        //Toast.makeText(this, String.valueOf(discountedPrice), Toast.LENGTH_SHORT).show();

        Log.d("abc",parameters.toString());
        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
//        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.setIsDeviceTokenGenerate(true, "vDeviceToken");
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(final String responseString) {

                Utils.printLog("CreateORDERResponseData", "Data::" + responseString);
//                Toast.makeText(PlaceOrderActivity.this, "" + responseString, Toast.LENGTH_SHORT).show();

                if (responseString != null && !responseString.equals(""))
                {

                    (new StartActProcess(getActContext())).setOkResult();
                    Dialog dialog = new Dialog(getActContext());
                    dialog.setContentView(R.layout.order_confirm_dialog);
                    dialog.setCancelable(false);
                    dialog.show();

                    TextView ok = dialog.findViewById(R.id.ok);
                    TextView dialog_msg = dialog.findViewById(R.id.dialog_msg);


                    ok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            startActivity(new Intent(getApplicationContext(),Dashboard.class));
                            overridePendingTransition( R.anim.left_to_right, R.anim.right_to_left );
                        }
                    });

                    String order_id = generalFunc.getJsonValue("order_id", responseString);
                } else {
                    generalFunc.showGeneralMessage("", "Please try again later");
                }
            }
        });
        exeWebServer.execute();
    }

    private Context getActContext() {
        return  this;
    }

}
