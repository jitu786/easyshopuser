package com.easyshopuser.Activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.easyshopuser.Adapter.CatProductListAdapter;
import com.easyshopuser.R;
import com.easyshopuser.generalfiles.ExecuteWebServerUrl;
import com.easyshopuser.generalfiles.GeneralFunctions;
import com.easyshopuser.generalfiles.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

public class Category_Product_List extends AppCompatActivity {

    RecyclerView catpro_list;
    CatProductListAdapter adapter;
    TextView po_title;
    ImageView pd_back;
    LinearLayout sorting_lay;
    ImageView filter;
    GeneralFunctions generalFunc;
    String category_id;
    LinearLayout withdata;
    int max=0,min=0;
    int flag=1;
    String count="",color_op="",size_op="";
    int select_view=1;
    EditText countfilter;
    View layout_cart_empty,thistool;
    boolean price_flag=false;
    ArrayList<HashMap<String, String>> dataList = new ArrayList<>();
    ArrayList<HashMap<String, String>> option_list = new ArrayList<>();
    String minprice="",maxprice="";
    boolean isPrice_flag=false,isColor=false,isSize=false;
    String field="post_id",order="desc";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category__product__list);
        initialize();
    }
    public void initialize()
    {
        generalFunc=new GeneralFunctions(getActContext());
        catpro_list=findViewById(R.id.catpro_list);
        filter=findViewById(R.id.filter);
        countfilter=findViewById(R.id.countfilter);
        countfilter.setText("0");

        @SuppressLint("WrongConstant") GridLayoutManager layoutManager_trendingItem=new GridLayoutManager(getApplicationContext(),2, GridLayoutManager.VERTICAL,false);
        catpro_list.setLayoutManager(layoutManager_trendingItem);
        adapter = new CatProductListAdapter(getApplicationContext(),dataList);
        catpro_list.setAdapter(adapter);
        catpro_list.setNestedScrollingEnabled(false);
        sorting_lay=findViewById(R.id.sorting_lay);
        layout_cart_empty=findViewById(R.id.layout_cart_empty);
        withdata=findViewById(R.id.withdata);
        thistool=findViewById(R.id.thistool);
        category_id=getIntent().getStringExtra("category_id");
//Toast.makeText(this, ""+category_id, Toast.LENGTH_SHORT).show();
        getProductCategoryWise();
        pd_back=findViewById(R.id.pd_back);
        pd_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        po_title=findViewById(R.id.po_title);
        po_title.setText("Category");
        sorting_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog dialog = new Dialog(Category_Product_List.this);
                dialog.setContentView(R.layout.sort_dialog);
                Window window = dialog.getWindow();
                WindowManager.LayoutParams wlp = window.getAttributes();
                window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                wlp.width = WindowManager.LayoutParams.MATCH_PARENT;
                wlp.gravity = Gravity.CENTER_VERTICAL;
                window.setAttributes(wlp);
                dialog.show();

                final RelativeLayout atoz,ztoa;

                atoz=dialog.findViewById(R.id.atoz);
                ztoa=dialog.findViewById(R.id.ztoa);


                atoz.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view)
                    {
                        atoz.setBackgroundColor(getResources().getColor(R.color.hint_txt_color));
                        ztoa.setBackgroundColor(getResources().getColor(R.color.white));

                        select_view=1;
                        dialog.dismiss();
                        getProductCategoryWiseasc();

                    }
                });

                ztoa.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ztoa.setBackgroundColor(getResources().getColor(R.color.hint_txt_color));
                        atoz.setBackgroundColor(getResources().getColor(R.color.white));
                        select_view=2;
                        adapter.filterList(dataList);
                        dialog.dismiss();
                        getProductCategoryWisedesc();
                    }
                });


            }
        });
        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                minprice= String.valueOf(min);
                maxprice= String.valueOf(max);

                if (countfilter.getText().toString().equals("1")) {
                    Intent intent = new Intent(getApplicationContext(), Filter_item.class);
                    intent.putExtra("count", "1");
                    intent.putExtra("minprice",minprice);
                    intent.putExtra("maxprice",maxprice);

                    Log.d("maxdata",maxprice);
                    Log.d("min",minprice);
                    //Log.e("putSL:","putSL:"+maxprice+""+minprice);
                    startActivityForResult(intent, 987);
                }
                else {

                    Intent intent=new Intent(getApplicationContext(), Filter_item.class);
                    intent.putExtra("count","0");
                    startActivityForResult(intent, 987);

                }


            }
        });

    }

    private void getProductCategoryWise()
    {
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("type", "getAllProductsOfCategory");
        parameters.put("category_id", category_id);
        parameters.put("country_id", "0");


        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(final String responseString) {
                Utils.printLog("ResponseData", "Data::" + responseString);
                if (responseString != null && !responseString.equals("")) {
                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);
                    if (isDataAvail) {

                        JSONArray msgArr = generalFunc.getJsonArray(Utils.message_str, responseString);

                        if (msgArr != null) {
                            for (int i = 0; i < msgArr.length(); i++) {
                                JSONObject obj_cat = generalFunc.getJsonObject(msgArr, i);
                                HashMap<String,String> hashMap=new HashMap<>();

                                String name = generalFunc.getJsonValue("name", obj_cat);
                                String seller_id = generalFunc.getJsonValue("seller_id", obj_cat);
                                String store_name = generalFunc.getJsonValue("store_name", obj_cat);
                                String store_country = generalFunc.getJsonValue("store_country", obj_cat);
                                String description = generalFunc.getJsonValue("description", obj_cat);
                                String image = generalFunc.getJsonValue("image", obj_cat);
                                String price = generalFunc.getJsonValue("price", obj_cat);
                                String product_id = generalFunc.getJsonValue("product_id", obj_cat);
                                String category_id = generalFunc.getJsonValue("category_id", obj_cat);
                                String seller_name = generalFunc.getJsonValue("seller_name", obj_cat);
                                String TotalRating = generalFunc.getJsonValue("TotalRating", obj_cat);

                                hashMap.put("name",name);
                                hashMap.put("seller_id",seller_id);
                                hashMap.put("store_name",store_name);
                                hashMap.put("description",description);
                                hashMap.put("store_country",store_country);
                                hashMap.put("price",price);
                                hashMap.put("product_id",product_id);
                                hashMap.put("category_id",category_id);
                                hashMap.put("seller_name",seller_name);
                                hashMap.put("image",image);
                                hashMap.put("TotalRating",TotalRating);

                                JSONArray option_data = generalFunc.getJsonArray("option_data",obj_cat);
                                if(option_data.length()!=0&&option_data!=null)
                                {
                                    for(int k=0;k<option_data.length();k++)
                                    {
                                        JSONObject data = generalFunc.getJsonObject(option_data, k);
                                        HashMap<String,String> map=new HashMap<>();
                                        map.put("product_id",product_id);
                                        map.put("option_value_name",generalFunc.getJsonValue("option_value_name",data));
                                        map.put("index_of_list",i+"");
                                        option_list.add(map);
                                    }
                                }

                                dataList.add(hashMap);
                            }
                            adapter.notifyDataSetChanged();
                        }

                    }
                    else

                    if (dataList.size()==0)
                    {
                        withdata.setVisibility(View.GONE);

                        layout_cart_empty.setVisibility(View.VISIBLE);

                    }

//                        generalFunc.showGeneralMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));

                } else {
                    //  generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();
    }



    private void getProductCategoryWiseasc()
    {
        dataList.clear();
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("type", "getAllProductsOfCategory");
        parameters.put("category_id", category_id);
        parameters.put("country_id", "0");


        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(final String responseString) {
                Utils.printLog("ResponseData", "Data::" + responseString);
                if (responseString != null && !responseString.equals("")) {
                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);
                    if (isDataAvail) {

                        JSONArray msgArr = generalFunc.getJsonArray(Utils.message_str, responseString);

                        if (msgArr != null) {
                            for (int i = 0; i < msgArr.length(); i++) {
                                JSONObject obj_cat = generalFunc.getJsonObject(msgArr, i);
                                HashMap<String,String> hashMap=new HashMap<>();

                                String name = generalFunc.getJsonValue("name", obj_cat);
                                String seller_id = generalFunc.getJsonValue("seller_id", obj_cat);
                                String store_name = generalFunc.getJsonValue("store_name", obj_cat);
                                String store_country = generalFunc.getJsonValue("store_country", obj_cat);
                                String description = generalFunc.getJsonValue("description", obj_cat);
                                String image = generalFunc.getJsonValue("image", obj_cat);
                                String price = generalFunc.getJsonValue("price", obj_cat);
                                String product_id = generalFunc.getJsonValue("product_id", obj_cat);
                                String category_id = generalFunc.getJsonValue("category_id", obj_cat);
                                String seller_name = generalFunc.getJsonValue("seller_name", obj_cat);
                                String TotalRating = generalFunc.getJsonValue("TotalRating", obj_cat);

                                hashMap.put("name",name);
                                hashMap.put("seller_id",seller_id);
                                hashMap.put("store_name",store_name);
                                hashMap.put("description",description);
                                hashMap.put("store_country",store_country);
                                hashMap.put("price",price);
                                hashMap.put("product_id",product_id);
                                hashMap.put("category_id",category_id);
                                hashMap.put("seller_name",seller_name);
                                hashMap.put("image",image);
                                hashMap.put("TotalRating",TotalRating);

                                JSONArray option_data = generalFunc.getJsonArray("option_data",obj_cat);
                                if(option_data.length()!=0&&option_data!=null)
                                {
                                    for(int k=0;k<option_data.length();k++)
                                    {
                                        JSONObject data = generalFunc.getJsonObject(option_data, k);
                                        HashMap<String,String> map=new HashMap<>();
                                        map.put("product_id",product_id);
                                        map.put("option_value_name",generalFunc.getJsonValue("option_value_name",data));
                                        map.put("index_of_list",i+"");
                                        option_list.add(map);
                                    }
                                }


                                dataList.add(hashMap);

                            }

                            Collections.sort(dataList, new Comparator() {
                                @Override
                                public int compare(Object o, Object t1) {
                                    HashMap<String, String>  data = ( HashMap<String, String>) o;
                                    HashMap<String, String>  data1 = ( HashMap<String, String>) t1;

                                    return data.get("name").toUpperCase().compareTo(data1.get("name").toUpperCase());
                                }
                            });

                            adapter.notifyDataSetChanged();
                        }

                    }


                    else if (dataList.size()==0)
                    {
                        withdata.setVisibility(View.GONE);

                        layout_cart_empty.setVisibility(View.VISIBLE);

                    }

//                        generalFunc.showGeneralMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));

                } else {
                    //  generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();
    }

    private void getProductCategoryWisedesc()
    {
        dataList.clear();
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("type", "getAllProductsOfCategory");
        parameters.put("category_id", category_id);
        parameters.put("country_id", "0");


        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(final String responseString) {
                Utils.printLog("ResponseData", "Data::" + responseString);
                if (responseString != null && !responseString.equals("")) {
                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);
                    if (isDataAvail) {

                        JSONArray msgArr = generalFunc.getJsonArray(Utils.message_str, responseString);

                        if (msgArr != null) {
                            for (int i = 0; i < msgArr.length(); i++) {
                                JSONObject obj_cat = generalFunc.getJsonObject(msgArr, i);
                                HashMap<String,String> hashMap=new HashMap<>();

                                String name = generalFunc.getJsonValue("name", obj_cat);
                                String seller_id = generalFunc.getJsonValue("seller_id", obj_cat);
                                String store_name = generalFunc.getJsonValue("store_name", obj_cat);
                                String store_country = generalFunc.getJsonValue("store_country", obj_cat);
                                String description = generalFunc.getJsonValue("description", obj_cat);
                                String image = generalFunc.getJsonValue("image", obj_cat);
                                String price = generalFunc.getJsonValue("price", obj_cat);
                                String product_id = generalFunc.getJsonValue("product_id", obj_cat);
                                String category_id = generalFunc.getJsonValue("category_id", obj_cat);
                                String seller_name = generalFunc.getJsonValue("seller_name", obj_cat);
                                String TotalRating = generalFunc.getJsonValue("TotalRating", obj_cat);

                                hashMap.put("name",name);
                                hashMap.put("seller_id",seller_id);
                                hashMap.put("store_name",store_name);
                                hashMap.put("description",description);
                                hashMap.put("store_country",store_country);
                                hashMap.put("price",price);
                                hashMap.put("product_id",product_id);
                                hashMap.put("category_id",category_id);
                                hashMap.put("seller_name",seller_name);
                                hashMap.put("image",image);
                                hashMap.put("TotalRating",TotalRating);
                                JSONArray option_data = generalFunc.getJsonArray("option_data",obj_cat);
                                if(option_data.length()!=0&&option_data!=null)
                                {
                                    for(int k=0;k<option_data.length();k++)
                                    {
                                        JSONObject data = generalFunc.getJsonObject(option_data, k);
                                        HashMap<String,String> map=new HashMap<>();
                                        map.put("product_id",product_id);
                                        map.put("option_value_name",generalFunc.getJsonValue("option_value_name",data));
                                        map.put("index_of_list",i+"");
                                        option_list.add(map);
                                    }
                                }


                                dataList.add(hashMap);

                            }

                            Collections.reverse(dataList);



                            adapter.notifyDataSetChanged();
                        }

                    }


                    else if (dataList.size()==0)
                    {
                        withdata.setVisibility(View.GONE);

                        layout_cart_empty.setVisibility(View.VISIBLE);

                    }

//                        generalFunc.showGeneralMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));

                } else {
                    //  generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 987 && resultCode == Activity.RESULT_OK && data != null) {
            if (!data.getStringExtra("minprice").isEmpty() && !data.getStringExtra("maxprice").isEmpty())
            {
                min= Integer.parseInt(data.getStringExtra("minprice"));
                max= Integer.parseInt(data.getStringExtra("maxprice"));
                Log.e("sl","sl:::"+max+""+min);
                isPrice_flag=true;
            }
            if (data.getStringExtra("count").equals("1"))
            {
                countfilter.setText("1");
                countfilter.setVisibility(View.VISIBLE);
            }
            else if (data.getStringExtra("count").equals("0"))
            {
                countfilter.setText("0");
                recreate();
            }
            if(data.getStringExtra("color")!=null)
            {
                color_op=data.getStringExtra("color");
                isColor=true;
            }
            if(data.getStringExtra("size")!=null) {
                size_op = data.getStringExtra("size");
                isSize = true;
            }
            getList();
        }

    }



    private void getList()
    {
        Log.d("data",size_op+" --"+color_op+"--"+min+"-"+max);
        ArrayList<HashMap<String, String>> dataList1 = new ArrayList<>();
        for (int i = 0; i < dataList.size(); i++)
        {
            HashMap<String, String> data = dataList.get(i);
            try {
                //if(isPrice_flag&&isSize&&isColor)
                {
                    float price=Float.parseFloat(data.get("price"));
                    if(((min< price && max >= price)||(min==0&&max==0))&&checkData(i,data.get("product_id"),size_op)&&
                            checkData1(i,data.get("product_id"),color_op))
                    {
                        dataList1.add(data);
                    }

                }
                /*else if(isPrice_flag&&isSize)
                {
                    float price=Float.parseFloat(data.get("price"));
                    if((min< price && max >= price)&&checkData(i,data.get("product_id"),size_op))
                    {
                        dataList1.add(data);
                    }
                }
                else if(isPrice_flag&&isColor)
                {

                    float price=Float.parseFloat(data.get("price"));
                    if((min< price && max >= price)&&checkData1(i,data.get("product_id"),color_op))
                    {
                        dataList1.add(data);
                        Log.d("abcd",data.toString());
                    }
                }
                else if(isColor&&isSize)
                {
                    if(checkData(i,data.get("product_id"),size_op)&&checkData1(i,data.get("product_id"),color_op))
                    {
                        dataList1.add(data);
                        Log.d("abcd",data.toString());
                    }
                }
                else if(isPrice_flag)
                {

                    float price=Float.parseFloat(data.get("price"));
                    if((min< price && max >= price))
                    {
                        dataList1.add(data);
                        Log.d("abcd",data.toString());
                    }
                }
                else if(isSize)
                {
                    if(checkData(i,data.get("product_id"),size_op))
                    {
                        dataList1.add(data);
                        Log.d("abcd",data.toString());
                    }
                }
                else if(isColor)
                {
                    if(checkData1(i,data.get("product_id"),color_op))
                        dataList1.add(data);
                }
                else
                {

                }*/


            }
            catch (NumberFormatException e)
            {

            }
            Log.d("sorting",data.toString());
        }
        adapter.filterList(dataList1);
        // adapter.notifyDataSetChanged();
    }

    private boolean checkData1(int i, String product_id, String color_op)
    {
        if(color_op.isEmpty())
            return true;
        if(option_list.size()>0) {
            for (int l = 0; l < option_list.size(); l++)
            {
                HashMap<String, String> data = option_list.get(l);
                if(data.get("product_id").equals(product_id)&&data.get("option_value_name").equalsIgnoreCase(color_op))
                    return true;
            }
        }
        return false;
    }

    private boolean checkData(int i, String product_id, String size_op)
    {
        if(size_op.isEmpty())
        {
            return true;
        }
        if(option_list.size()>0) {
            for (int l = 0; l < option_list.size(); l++)
            {
                HashMap<String, String> data = option_list.get(l);
                String op_size= String.valueOf(data.get("option_value_name").charAt(0));
                if(data.get("product_id").equals(product_id)&&op_size.equalsIgnoreCase(size_op))
                    return true;
            }
        }
        return false;
    }


    public boolean dataCheck(String data, String data1)
    {
        if(data.trim().isEmpty())
            return true;
        else if(data1.contains(data))
            return true;
        else
            return false;

    }


    private Context getActContext() {
        return Category_Product_List.this;
    }


}
