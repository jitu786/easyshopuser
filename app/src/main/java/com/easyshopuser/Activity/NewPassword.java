package com.easyshopuser.Activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.easyshopuser.R;
import com.easyshopuser.generalfiles.ExecuteWebServerUrl;
import com.easyshopuser.generalfiles.GeneralFunctions;
import com.easyshopuser.generalfiles.Utils;
import com.easyshopuser.view.GenerateAlertBox;

import java.util.HashMap;

public class NewPassword extends AppCompatActivity {

    ImageView pd_back;
    TextView po_title;
    GeneralFunctions generalFunc;
    Button reset_password;
    String otpcode="",customer_id="";
    String emailget;
    TextView new_show,confirm_show;
    EditText otp,new_password,confirm_password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_password);
        emailget=getIntent().getStringExtra("email");

//Toast.makeText(this, ""+emailget, Toast.LENGTH_SHORT).show();
        initialize();
        reset_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (chekvalidation())
                {
                    updatePassword();
                }


            }
        });
        pd_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        new_show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPassword(new_password,new_show);

            }
        });

        confirm_show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showPassword(confirm_password,confirm_show);
            }
        });



    }

    private void updatePassword() {

        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("type", "changePassword");
        parameters.put("email",emailget);
        parameters.put("codeVerification","codeVerification");
        parameters.put("newPassword", confirm_password.getText().toString());

        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.setIsDeviceTokenGenerate(false, "vDeviceToken");
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(final String responseString) {

                Utils.printLog("ResponseData", "Data::" + responseString);

                if (responseString != null && !responseString.equals("")) {
                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);

                    if (isDataAvail) {

                        final GenerateAlertBox generateAlert = new GenerateAlertBox(getActContext());
                        generateAlert.setCancelable(false);
                        generateAlert.setBtnClickList(new GenerateAlertBox.HandleAlertBtnClick() {
                            @Override
                            public void handleBtnClick(int btn_id) {
                                if (btn_id == 1) {

//Toast.makeText(NewPassword.this, ""+responseString, Toast.LENGTH_SHORT).show();
                                    Intent intent=new Intent(NewPassword.this,Login.class);
                                    startActivity(intent);

                                }
                            }
                        });
                        generateAlert.setContentMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));
                        generateAlert.setPositiveBtn("Ok");
                        generateAlert.showAlertBox();
                    } else {
                        generalFunc.showGeneralMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));
                    }

                } else {
                    generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();
    }

    private Context getActContext() {

        return NewPassword.this;
    }


    private boolean chekvalidation() {


        if (otp.getText().toString().isEmpty()) {
            otp.setError("OTP Is Required");
            return false;
        } else if (!otp.getText().toString().equals(otpcode)) {
            otp.setError("OTP Does Not Match");
            return false;
        }

        if (new_password.getText().toString().isEmpty()) {
            new_password.setError("Password is Required");
            return false;
        }
        if (confirm_password.getText().toString().isEmpty()) {
            confirm_password.setError("ConfirmPassword is Required");
            return false;
        } else if (!new_password.getText().toString().trim().isEmpty() && !new_password.getText().toString().trim().equals(confirm_password.getText().toString().trim())) {
            confirm_password.setError("Password and confirm password should be same");
            return false;
        }

        return true;
    }

    public void initialize()
    {
        po_title=findViewById(R.id.po_title);
        new_show=findViewById(R.id.new_show);
        confirm_show=findViewById(R.id.confirm_show);

        pd_back=findViewById(R.id.pd_back);
        po_title.setText("Change Your Password");
        generalFunc=new GeneralFunctions(NewPassword.this);
        reset_password=findViewById(R.id.reset_password);
        otpcode=getIntent().getStringExtra("VerificationCode");
        customer_id=getIntent().getStringExtra("customer_id");
        otp=findViewById(R.id.otp);
        new_password=findViewById(R.id.new_password);
        confirm_password=findViewById(R.id.confirm_password);

// Toast.makeText(this, ""+otpcode, Toast.LENGTH_SHORT).show();
    }


    private void showPassword(EditText editText, TextView textView) {

        if(textView.getText().toString().equals("Show"))
        {
            textView.setText("Hide");
            Typeface type = Typeface.createFromAsset(getAssets(),"font/montserratregular.ttf");

            new_password.setTypeface(type);
            confirm_show.setTypeface(type);
            editText.setInputType(InputType.TYPE_CLASS_TEXT );
            editText.setTypeface(type);



        }
        else
        {
            textView.setText("Show");
            Typeface type = Typeface.createFromAsset(getAssets(),"font/montserratregular.ttf");

            new_password.setTypeface(type);
            confirm_show.setTypeface(type);
            editText.setInputType(InputType.TYPE_CLASS_TEXT |
                    InputType.TYPE_TEXT_VARIATION_PASSWORD);
            editText.setTypeface(type);

//editText.setInputType(InputType.TYPE_CLASS_TEXT );//| InputType.TYPE_TEXT_VARIATION_PASSWORD


        }

    }



}
