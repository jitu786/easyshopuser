package com.easyshopuser.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.easyshopuser.R;
import com.easyshopuser.generalfiles.GeneralFunctions;
import com.easyshopuser.helper.Utility1;
import com.easyshopuser.view.GenerateAlertBox;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.model.Charge;
import com.stripe.net.RequestOptions;

import java.util.HashMap;
import java.util.Map;

public class Payment_Method extends AppCompatActivity  implements View.OnClickListener{

    Utility1 myUtility;
    HashMap<String, String> selectedAddressData = null;
    GeneralFunctions generalFunc;
    EditText editCardNumber,edtmonth,editCvcNumber,editRefDesc,editEmail,edtyear;
    Button btnChargeCard;
    private Button checkout_button;
    private String userPublishableKey = "pk_test_7azE2X27TnjPITdBvDw63JdL00IDCySDnh";
    private String userSecretKey = "sk_test_euKoWaVRYX9cG54OvM74tZOB000SIqfMZS";
    private String cardNumber,cardCVC,userDescription,userEmail;
    private int cardExpMonth,cardExpYear;
    private TextView po_title;
    private ImageView pd_back;
    private ProgressDialog mProgressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment__method);
        //com.stripe.Stripe.apiKey = "sk_test_euKoWaVRYX9cG54OvM74tZOB000SIqfMZS";

        //PaymentConfiguration.init(this, "pk_test_7azE2X27TnjPITdBvDw63JdL00IDCySDnh");

        generalFunc=new GeneralFunctions(this);
        myUtility=new Utility1(Payment_Method.this);
        editCardNumber=findViewById(R.id.editCardNumber);
        edtmonth=findViewById(R.id.edtmonth);
        edtyear=findViewById(R.id.edtyear);
        editCvcNumber=findViewById(R.id.editCvcNumber);
        editRefDesc=findViewById(R.id.editRefDesc);
        btnChargeCard=findViewById(R.id.btnChargeCard);
        editEmail=findViewById(R.id.editEmail);
        po_title=findViewById(R.id.po_title);
        pd_back=findViewById(R.id.pd_back);
        po_title.setText("Payment Method");



        btnChargeCard.setOnClickListener(this);

        Intent data=getIntent();
        selectedAddressData = (HashMap<String, String>) data.getSerializableExtra("DATA");



        pd_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


    }

    @Override
    public void onClick(View view) {
        if(isCardValid()) {
            Card card = new Card(cardNumber, cardExpMonth, cardExpYear, cardCVC);
            try {
                Stripe stripe = new Stripe(userPublishableKey);
                stripe.createToken(
                        card,
                        userPublishableKey,
                        new TokenCallback() {
                            @Override
                            public void onError(Exception error) {
                                myUtility.setAlertMessage("Error", error.getMessage());

                            }

                            @Override
                            public void onSuccess(Token token) {
                                if (myUtility.checkInternetConnection())

                                    sendCardToInfoToStripe(token.getId(), false);
                                else {
                                    myUtility.setInternetAlertMessage();

                                }


                            }
                        });
            } catch (Exception e) {
            }
        }

    }



    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("Loading..");
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }

    private void sendCardToInfoToStripe(final String id, boolean b)
    {


        showProgressDialog();
        userDescription = editRefDesc.getText().toString(); // Get User Description[bad code]
        userEmail = editEmail.getText().toString(); // Get User Email Id
        new Thread(new Runnable() {
            @Override
            public void run() {

                Map<String, Object> addressParams = new HashMap<>();
                addressParams.put("line1", selectedAddressData.get("address_1"));
                addressParams.put("postal_code", selectedAddressData.get("postcode"));
                addressParams.put("city", selectedAddressData.get("city"));
                addressParams.put("state", selectedAddressData.get("state_name"));
                addressParams.put("country", selectedAddressData.get("country_name"));
                Map<String, Object> shippingParams = new HashMap<>();
                shippingParams.put("name",  selectedAddressData.get("firstname")+" "+selectedAddressData.get("lastname"));
                shippingParams.put("address", addressParams);

                final Map<String, Object> chargeParams = new HashMap<String, Object>();

                if(selectedAddressData.get("amount").contains(".")) {
                    String amt=""+Math.round(Float.parseFloat(selectedAddressData.get("amount")));
                    chargeParams.put("amount", Integer.parseInt(amt.replace(".0",""))*100);
                }
                else
                    chargeParams.put("amount", (selectedAddressData.get("amount")));

                chargeParams.put("currency", "inr");
                chargeParams.put("source", id);
                chargeParams.put("description", userDescription);
                chargeParams.put("receipt_email", userEmail);
                chargeParams.put("shipping", shippingParams);
                final RequestOptions requestOptions;
                requestOptions = RequestOptions.builder().setStripeAccount(("acct_1GE7anLu06Y4kNkC")).setApiKey(userSecretKey).build();


                try {
                    final Charge charge= Charge.create(chargeParams, requestOptions);



                    //charge.getId();
                    runOnUiThread(

                            new Runnable() {
                                @Override
                                public void run() {
                                    callSuccess(charge);                        }
                            });

                } catch (final Exception e) {
                    myUtility.dismissDialog();
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            myUtility.setAlertMessage("Error", e.getMessage());
                        }
                    });
                }
            }
        }).start();


    }

    private void callSuccess(final Charge charge)
    {
        hideProgressDialog();
        GenerateAlertBox alertBox= generalFunc.showGeneralMessage("Sucess","Your payment is received sucessfully");
        alertBox.setCancelable(false);
        alertBox.setBtnClickList(new GenerateAlertBox.HandleAlertBtnClick() {
            @Override
            public void handleBtnClick(int btn_id) {
                Intent intent = new Intent();
                intent.putExtra("payment_id", charge.getId());
                setResult(RESULT_OK, intent);
                Payment_Method.super.onBackPressed();
            }
        });
    }



    private  boolean isCardValid()
    {
        cardNumber =editCardNumber.getText().toString().replace(" ", "");
        cardCVC = editCvcNumber.getText().toString();
        if(!edtmonth.getText().toString().trim().isEmpty()&&!edtyear.getText().toString().trim().isEmpty()) {
            cardExpMonth = Integer.parseInt(edtmonth.getText().toString());
            cardExpYear = Integer.parseInt(edtyear.getText().toString());

        }
        Card card = new Card(cardNumber, cardExpMonth, cardExpYear, cardCVC);
        if(editCardNumber.getText().toString().isEmpty() && !card.validateNumber())
        {
            myUtility.setAlertMessage("Error", "The card number that you entered is invalid.");
            return  false;
        }
        if (edtyear.getText().toString().isEmpty() && !card.validateExpiryDate())
        {
            myUtility.setAlertMessage("Error", "The expiration date that you entered is invalid.");
            return  false;
        }
        if (editCvcNumber.getText().toString().isEmpty() &&!card.validateCVC() )
        {
            myUtility.setAlertMessage("Error","The CVC code that you entered is invalid.");
            return  false;
        }
        if (editRefDesc.getText().toString().isEmpty())
        {
            myUtility.setAlertMessage("Error","The description that you entered is invalid.");
            return  false;
        }
        if (editEmail.getText().toString().isEmpty())
        {
            myUtility.setAlertMessage("Error","The EmailId is  that you entered is invalid.");
            return  false;
        }
        return  true;
    }

    private boolean isCardValid2()
    { cardNumber =editCardNumber.getText().toString().replace(" ", "");
        cardCVC = editCvcNumber.getText().toString();
        if(!edtmonth.getText().toString().trim().isEmpty()&&!edtyear.getText().toString().trim().isEmpty()) {
            cardExpMonth = Integer.parseInt(edtmonth.getText().toString());
            cardExpYear = Integer.parseInt(edtyear.getText().toString());
        }

// Card card = new Card("4242424242424242", 12, 2016, "123");
        Card card = new Card(cardNumber, cardExpMonth, cardExpYear, cardCVC);
        boolean validation = card.validateCard();
        if (validation) {
            return true;
        } else if (!card.validateNumber()) {
            myUtility.setAlertMessage("Error", "The card number that you entered is invalid.");
        } else if (!card.validateExpiryDate()) {
            myUtility.setAlertMessage("Error", "The expiration date that you entered is invalid.");

        } else if (!card.validateCVC()) {
            myUtility.setAlertMessage("Error","The CVC code that you entered is invalid.");

        } else {
            myUtility.setAlertMessage("Error", "The card details that you entered are invalid.");

        }
        return  false;
    }
}

