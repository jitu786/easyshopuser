package com.easyshopuser.Activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.easyshopuser.R;
import com.easyshopuser.generalfiles.ExecuteWebServerUrl;
import com.easyshopuser.generalfiles.GeneralFunctions;
import com.easyshopuser.generalfiles.Utils;
import com.easyshopuser.utils.SharedPref;
import com.facebook.CallbackManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.HashMap;

public class Register extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    ImageView cancle,facebook,google;
    LinearLayout ca_signin;
    EditText email_id,password,retypepassword,firstname,lastname,mobileno;
    Button signup;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    GeneralFunctions generalFunc;
    TextView passwordshow,confirmshow;
    String DeviceToken="";
    LoginWithGoogle loginWithGoogle;
    CallbackManager mCallbackManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        initialize();

        google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                loginWithGoogle = new LoginWithGoogle(getActContext(),DeviceToken);
            }
        });

        facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new LoginWithFacebook(getActContext(), mCallbackManager);
            }
        });

    }
    public void initialize()
    {

        generalFunc=new GeneralFunctions(Register.this);
        DeviceToken = FirebaseInstanceId.getInstance().getToken();
        mCallbackManager = CallbackManager.Factory.create();
        retypepassword=findViewById(R.id.retypepassword);
        facebook=findViewById(R.id.facebook);
        google=findViewById(R.id.google);
        email_id=findViewById(R.id.email_id);
        password=findViewById(R.id.password);
        signup=findViewById(R.id.signup);
        cancle=findViewById(R.id.cancle);
//pincode=findViewById(R.id.pincode);
        passwordshow=findViewById(R.id.passwordshow);
        confirmshow=findViewById(R.id.confirmshow);
        firstname=findViewById(R.id.fname);
        lastname=findViewById(R.id.lname);
        mobileno=findViewById(R.id.mobileno);
        ca_signin=findViewById(R.id.ca_signin);

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validdata())
                {

                    chekemaildata();

                }
            }
        });

        ca_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getApplicationContext(),Login.class);
                startActivity(intent);
                overridePendingTransition( R.anim.left_to_right, R.anim.right_to_left );

            }
        });



        passwordshow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPassword(password,passwordshow);

            }
        });

        confirmshow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showPassword(retypepassword,confirmshow);
            }
        });




        cancle=findViewById(R.id.cancle);
        cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
//overridePendingTransition( R.anim.slide_in_down, R.anim.slide_out_down );
            }
        });



    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Utils.GOOGLE_SIGN_IN_REQ_CODE)
        {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            loginWithGoogle.handleSignInResult(result);
        }
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void chekemaildata() {


        HashMap<String, String> parameters = new HashMap<>();
        // parameters.put("city", Utils.getText(cityBox));
        parameters.put("type", "registerUser");
        parameters.put("firstName",firstname.getText().toString());
        parameters.put("lastName",lastname.getText().toString());
        parameters.put("mobile",mobileno.getText().toString());
        parameters.put("country", "");
        parameters.put("country_id", "");
        parameters.put("customer_group_id","1");
        parameters.put("password", retypepassword.getText().toString());
        parameters.put("email", email_id.getText().toString());
        parameters.put("fb_token",DeviceToken);

        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.setIsDeviceTokenGenerate(true, "vDeviceToken");
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(final String responseString) {

                Utils.printLog("ResponseData", "Data::" + responseString);

                if (responseString !=null && !responseString.equals(""))
                {
                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);

                    if (isDataAvail) {

                        generalFunc.storedata("COUNTRY_ID", "0");
                        generalFunc.storeUserData(generalFunc.getJsonValue(Utils.message_str, responseString));
                        SharedPref sharedPref=new SharedPref();
                        sharedPref.WriteSharePrefrence(getActContext(),"userdata",responseString);
                        Intent intent = new Intent(Register.this, Dashboard.class);
                        startActivity(intent);
                        finish();

                    } else {
                        generalFunc.showGeneralMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));
                    }

                } else {
                    generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();
    }



    private Context getActContext() {
        return Register.this;

    }

    private void showPassword(EditText editText, TextView textView) {

        if(textView.getText().toString().equals("Show"))
        {
            textView.setText("Hide");
            Typeface type = Typeface.createFromAsset(getAssets(),"font/montserratregular.ttf");

            password.setTypeface(type);
            confirmshow.setTypeface(type);

            editText.setInputType(InputType.TYPE_CLASS_TEXT );
            editText.setTypeface(type);



        }
        else
        {
            textView.setText("Show");
            Typeface type = Typeface.createFromAsset(getAssets(),"font/montserratregular.ttf");

            password.setTypeface(type);
            confirmshow.setTypeface(type);
            editText.setInputType(InputType.TYPE_CLASS_TEXT |
                    InputType.TYPE_TEXT_VARIATION_PASSWORD);
            editText.setTypeface(type);

//editText.setInputType(InputType.TYPE_CLASS_TEXT );//| InputType.TYPE_TEXT_VARIATION_PASSWORD


        }

    }

    private boolean validdata() {

        if (firstname.getText().toString().isEmpty())
        {
            firstname.setError("First name is required");
            return false;
        }
        if (lastname.getText().toString().isEmpty())
        {
            lastname.setError("Last name is required");
            return false;
        }
        if (email_id.getText().toString().isEmpty()) {
            email_id.setError("Email id is required");
            return false;
        }
        else if (!email_id.getText().toString().matches(emailPattern)) {
            email_id.setError("Email should be in correct format");
            return false;
        }
        if (mobileno.getText().toString().isEmpty())
        {
            mobileno.setError("Phone  number is required");
            return false;
        }
        else  if (mobileno.getText().toString().trim().length()!=10)
        {
            mobileno.setError("Phone number should be 10 digit.");
            return false;
        }



        if (password.getText().toString().isEmpty())
        {
            password.setError("Password is required");
            return false;
        }

        if (retypepassword.getText().toString().isEmpty())
        {
            retypepassword.setError("Confirm password is required");
            return false;
        }else if (!password.getText().toString().trim().isEmpty() && !password.getText().toString().trim().equals(retypepassword.getText().toString().trim())) {
            retypepassword.setError("Password and confirm password should be same");
            return false;
        }
       /* if (pincode.getText().toString().isEmpty())
        {
            pincode.setError("PinCode is required");
            return false;
        }
*/
        return true;
    }



    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
