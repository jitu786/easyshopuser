package com.easyshopuser.Activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.easyshopuser.R;

public class Contackus extends AppCompatActivity {

    ImageView pd_back;
    TextView po_title;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contack_us);


        pd_back=findViewById(R.id.pd_back);
        po_title=findViewById(R.id.po_title);

        po_title.setText("Contack Us Page ");

        pd_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
                finish();
            }
        });

    }
}
