package com.easyshopuser.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.easyshopuser.R;
import com.easyshopuser.generalfiles.ExecuteWebServerUrl;
import com.easyshopuser.generalfiles.GeneralFunctions;
import com.easyshopuser.generalfiles.Utils;
import com.easyshopuser.utils.SharedPref;

import org.json.JSONObject;

import java.util.HashMap;

public class NavigationList  extends AppCompatActivity {


    ImageView cancle,navsearch;
    TextView nav_signin,txtCartcount,name;
    String customer_id="";
    GeneralFunctions generalFunc;
    RelativeLayout dashboard_cart;
    LinearLayout home,All_category,navigation_list,order,message,favourite,settings;
    RelativeLayout my_profile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_list);

        initialize();
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),Dashboard.class));
            }
        });
        All_category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), Dashboard.class));
            }
        });

        order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),My_Order.class));
            }
        });

        message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), My_Message.class));
            }
        });

        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),Settings.class));
            }
        });

        favourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),My_Favourite.class));
            }
        });

        my_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), My_Profile.class));
            }
        });
        getUserInfo();
/* navsearch.setOnClickListener(new View.OnClickListener() {
@Override
public void onClick(View view) {
(new StartActProcess(getActContext())).startAct(SearchProductsActivity.class);
}
});*/

        dashboard_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i2 = new Intent(getApplicationContext(), Add_To_Cart.class);
                startActivity(i2);
                overridePendingTransition( R.anim.left_to_right, R.anim.right_to_left );

            }
        });


    }

    private Context getActContext() {
        return this;
    }

    public void initialize()
    {

        txtCartcount=findViewById(R.id.txtCartcount);
        txtCartcount.setText(SharedPref.ReadSharePrefrence(getActContext(),"cart"));
        dashboard_cart=findViewById(R.id.dashboard_cart);
        my_profile=findViewById(R.id.my_profile);
        name=findViewById(R.id.name);
        All_category=findViewById(R.id.All_category);
        favourite=findViewById(R.id.favourite);
        home=findViewById(R.id.home);
        message=findViewById(R.id.message);
        settings=findViewById(R.id.settings);
        nav_signin=findViewById(R.id.nav_sigin);
        order=findViewById(R.id.order);
        cancle=findViewById(R.id.cancle);
        generalFunc=new GeneralFunctions(NavigationList.this);

        customer_id=generalFunc.getMemberId();

        if (customer_id.equals("0")) {
            nav_signin.setVisibility(View.VISIBLE);
            my_profile.setVisibility(View.GONE);


        }
        else
        {
            nav_signin.setVisibility(View.GONE);
            my_profile.setVisibility(View.VISIBLE);
        }


        nav_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i2 = new Intent(getApplicationContext(), Login.class);
                startActivity(i2);
                overridePendingTransition( R.anim.slide_in_up, R.anim.slide_out_up );
            }
        });

        cancle=findViewById(R.id.cancle);
        cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
                overridePendingTransition( R.anim.slide_in_up, R.anim.slide_out_up );
            }
        });
        if(generalFunc.getMemberId().isEmpty()) {
            my_profile.setVisibility(View.GONE);
//order.setVisibility(View.GONE);
//message.setVisibility(View.GONE);
//favourite.setVisibility(View.GONE);
        }

    }


    private void getUserInfo() {

        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("type", "getUserInfo");
        parameters.put("customer_id", generalFunc.getMemberId());

        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(getActContext(),true,generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(String responseString) {


                if (responseString != null && !responseString.equals("")) {

                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);
                    if (isDataAvail == true) {
                        JSONObject obj_msg = generalFunc.getJsonObject(Utils.message_str, responseString);

                        name.setText(generalFunc.getJsonValue("email",obj_msg));

                    }
                }
            }
        });
        exeWebServer.execute();

    }



}
