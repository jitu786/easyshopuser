package com.easyshopuser.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;

import com.easyshopuser.R;
import com.easyshopuser.generalfiles.ExecuteWebServerUrl;
import com.easyshopuser.generalfiles.GeneralFunctions;
import com.easyshopuser.generalfiles.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class ReturnOrderActivity extends AppCompatActivity {
    EditText first_name,last_name,email_id,phone_no,order_id,product_name,product_code,product_quet,order_date;
    AppCompatEditText other_detail;
    TextView dobSelectTxtView,po_title;
    Button btnSubmit;
    ImageView pd_back;
    GeneralFunctions generalFunc;
    HashMap<String, String> OrderData = null;
    LinearLayout radio_view;
    RadioButton rd_yes,rd_no;
    String reason_id="";
    ArrayList<HashMap<String, String>> dataList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_return_order);
        initlized();
    }

    private void initlized()
    {
        po_title=findViewById(R.id.po_title);
        po_title.setText("Return Order");
        pd_back=findViewById(R.id.pd_back);
        rd_yes=findViewById(R.id.rd_yes);
        rd_no=findViewById(R.id.rd_no);
        radio_view=findViewById(R.id.radio_view);
        OrderData = (HashMap<String, String>) getIntent().getSerializableExtra("oderDATA");
        generalFunc=new GeneralFunctions(this);
        first_name=findViewById(R.id.first_name);
        last_name=findViewById(R.id.last_name);
        email_id=findViewById(R.id.email_id);
        phone_no=findViewById(R.id.phone_no);
        order_id=findViewById(R.id.order_id);
        product_name=findViewById(R.id.product_name);
        product_code=findViewById(R.id.product_code);
        product_quet=findViewById(R.id.product_quet);
        other_detail=findViewById(R.id.other_detail);
        order_date=findViewById(R.id.order_date);
        btnSubmit=findViewById(R.id.btnSubmit);

        if(OrderData!=null)
        {
            order_id.setText(OrderData.get("order_id"));
            product_name.setText(OrderData.get("productName"));
            product_code.setText(OrderData.get("productModel"));
            product_quet.setText(OrderData.get("productQuantity"));
            order_date.setText(OrderData.get("date_added"));
        }
        getUserInfo();
        getReason();

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                addData();
            }
        });

        pd_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void addData() {
        btnSubmit.setVisibility(View.GONE);
        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("type", "setReturnData");
        parameters.put("customer_id", generalFunc.getMemberId());
        parameters.put("order_id", Utils.getText(order_id));
        parameters.put("firstname", Utils.getText(first_name));
        parameters.put("lastname", Utils.getText(last_name));
        parameters.put("telephone", Utils.getText(phone_no));
        parameters.put("model", Utils.getText(product_code));
        parameters.put("quantity", Utils.getText(product_quet));
        parameters.put("product", Utils.getText(product_name));
        parameters.put("email", Utils.getText(email_id));
        parameters.put("return_reason_id", reason_id);
        parameters.put("return_action_id", "0");
        parameters.put("return_status_id", "2");
        parameters.put("date_ordered", Utils.getText(order_date));
        parameters.put("comment", Utils.getText(other_detail));
        parameters.put("product_id", OrderData.get("product_id"));
        if(rd_yes.isChecked())
            parameters.put("opened", "1");
        else
            parameters.put("opened", "0");


        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(String responseString) {

                if (responseString != null && !responseString.equals("")) {

                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);
                    if (isDataAvail == true) {

                        Toast.makeText(ReturnOrderActivity.this, ""+generalFunc.getJsonValue("message",responseString), Toast.LENGTH_LONG).show();

                        updateOrderstatus();
                      /*  Intent intent=new Intent(ReturnOrderActivity.this,My_Order.class);
                        startActivity(intent);
                        finish();
*/                    }
                    else
                    {
                        Toast.makeText(ReturnOrderActivity.this, ""+generalFunc.getJsonValue("message",responseString), Toast.LENGTH_LONG).show();

                    }
                }
            }
        });
        exeWebServer.execute();

    }

    private void updateOrderstatus() {

        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("type", "updateOrderStatus");
        parameters.put("order_status_id","17");
        parameters.put("order_id",Utils.getText(order_id));

        Log.d("parater",parameters.toString());
        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(ReturnOrderActivity.this, true, generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(String responseString)
            {
                if (responseString != null && !responseString.equals("")) {


                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);
                    if (isDataAvail)
                    {
                        Intent intent=new Intent(ReturnOrderActivity.this,My_Order.class);
                        startActivity(intent);
                        finish();
                        // Toast.makeText(EditArticleList.this, "" + generalFunc.getJsonValue("message", responseString), Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(ReturnOrderActivity.this, "" + generalFunc.getJsonValue("message", responseString), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        exeWebServer.execute();

    }

    private void getUserInfo() {

        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("type", "getUserInfo");
        parameters.put("customer_id", generalFunc.getMemberId());

        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(String responseString) {

                if (responseString != null && !responseString.equals("")) {

                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);
                    if (isDataAvail == true) {
                        JSONObject obj_msg = generalFunc.getJsonObject(Utils.message_str, responseString);
                        first_name.setText(generalFunc.getJsonValue("firstname",obj_msg));
                        last_name.setText(generalFunc.getJsonValue("lastname",obj_msg));
                        email_id.setText(generalFunc.getJsonValue("email",obj_msg));
                        phone_no.setText(generalFunc.getJsonValue("telephone",obj_msg));
                    }
                }
            }
        });
        exeWebServer.execute();


    } private void getReason() {

        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("type", "getReasonData");

        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(String responseString) {

                if (responseString != null && !responseString.equals("")) {

                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);
                    if (isDataAvail == true) {
                        JSONArray message = generalFunc.getJsonArray(Utils.message_str, responseString);
                        if(message.length()>0)
                        {
                            final RadioButton[] rb = new RadioButton[message.length()];
                            RadioGroup rg = new RadioGroup(ReturnOrderActivity.this); //create the RadioGroup
                            rg.setOrientation(RadioGroup.VERTICAL);//or RadioGroup.VERTICAL
                            for(int i=0; i<message.length(); i++){
                                HashMap<String,String> map=new HashMap<>();
                                JSONObject object=generalFunc.getJsonObject(message,i);
                                rb[i] = new RadioButton(ReturnOrderActivity.this);
                                rb[i].setText(generalFunc.getJsonValue("name",object));
                                rb[i].setId(i);
                                rg.addView(rb[i]);
                                map.put("name",generalFunc.getJsonValue("name",object));
                                map.put("language_id",generalFunc.getJsonValue("language_id",object));
                                map.put("return_reason_id",generalFunc.getJsonValue("return_reason_id",object));
                                dataList.add(map);
                            }
                            radio_view.addView(rg);//you add the whole RadioGroup to the layout

                            rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(RadioGroup radioGroup, int i) {
                                    reason_id=dataList.get(i).get("return_reason_id");
                                }
                            });

                        }
                    }
                }
            }
        });
        exeWebServer.execute();
    }
}
