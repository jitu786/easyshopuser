package com.easyshopuser.Fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.easyshopuser.Activity.SearchProductsActivity;
import com.easyshopuser.Adapter.CatAdapter;
import com.easyshopuser.Adapter.Flash_DealAdapter;
import com.easyshopuser.Adapter.NewArrivalAdapter;
import com.easyshopuser.R;
import com.easyshopuser.generalfiles.ExecuteWebServerUrl;
import com.easyshopuser.generalfiles.GeneralFunctions;
import com.easyshopuser.generalfiles.Utils;
import com.easyshopuser.helper.StartActProcess;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import static com.easyshopuser.generalfiles.Utils.closeLoader;

public class Home   extends Fragment {
    RecyclerView cat_list, flashdeal_list, new_arrival_list;
    CatAdapter catadapter;
    NewArrivalAdapter newArrivalAdapter;
    Flash_DealAdapter flash_dealAdapter;
    TextView hour,second,minute,day;


    CardView lyt_serach;
    GeneralFunctions generalFunc;
    ArrayList<HashMap<String, String>> dataList = new ArrayList<>();
    ArrayList<HashMap<String, String>> flash_list = new ArrayList<>();
    ArrayList<HashMap<String, String>> arrival_dataList = new ArrayList<>();
    ArrayList<HashMap<String, String>> rated_dataList = new ArrayList<>();


    ArrayList<String> date_list=new ArrayList<String>();


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.home_fragment, container, false);

        generalFunc = new GeneralFunctions(getActContext());
        day=v.findViewById(R.id.day);
        hour=v.findViewById(R.id.hour);
        minute=v.findViewById(R.id.minute);
        second=v.findViewById(R.id.second);
        cat_list = v.findViewById(R.id.cat_list);
        lyt_serach = v.findViewById(R.id.lyt_serach);
        @SuppressLint("WrongConstant") LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        cat_list.setLayoutManager(layoutManager);
        catadapter = new CatAdapter(getActivity(), dataList);
        cat_list.setAdapter(catadapter);
        cat_list.setNestedScrollingEnabled(false);
        catadapter.notifyDataSetChanged();
        try {
            getcategory();
        } catch (Exception e) {
        }

        lyt_serach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                (new StartActProcess(getActContext())).startAct(SearchProductsActivity.class);

            }
        });

        flashdeal_list = v.findViewById(R.id.flashdeal_list);
        @SuppressLint("WrongConstant") GridLayoutManager layoutManager_flash = new GridLayoutManager(v.getContext(), 2, GridLayoutManager.VERTICAL, false);
        flashdeal_list.setLayoutManager(layoutManager_flash);
        flash_dealAdapter = new Flash_DealAdapter(getActivity(), flash_list);
        flashdeal_list.setNestedScrollingEnabled(false);
        flashdeal_list.setAdapter(flash_dealAdapter);
        flash_dealAdapter.notifyDataSetChanged();

        try {
            findProducts();
        } catch (Exception e) {
            e.printStackTrace();
        }


        new_arrival_list = v.findViewById(R.id.new_arrival_list);
        @SuppressLint("WrongConstant") GridLayoutManager layoutManager_new_arrival = new GridLayoutManager(v.getContext(), 2, GridLayoutManager.VERTICAL, false);
        new_arrival_list.setLayoutManager(layoutManager_new_arrival);
        newArrivalAdapter = new NewArrivalAdapter(getActivity(),arrival_dataList);
        new_arrival_list.setNestedScrollingEnabled(false);
        new_arrival_list.setAdapter(newArrivalAdapter);
        newArrivalAdapter.notifyDataSetChanged();
        try {
            getProduct();
        } catch (Exception e) {
            e.printStackTrace();
        }


        return v;
    }

    private Context getActContext() {

        return getContext();
    }

    public void getcategory() {
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("type", "getAllCategories");

        Log.e("category", "cat" + parameters.toString());

        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(final String responseString) {
                Utils.printLog("ResponseData", "Data::" + responseString);
                if (responseString != null && !responseString.equals("")) {
                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);
                    if (isDataAvail) {
                        JSONArray msgArr = generalFunc.getJsonArray(Utils.message_str, responseString);
                        dataList.clear();
                        if (msgArr != null) {
                            for (int i = 0; i < msgArr.length(); i++) {
                                JSONObject obj_service = generalFunc.getJsonObject(msgArr, i);
                                HashMap<String, String> dataMap_products = new HashMap<>();
                                dataMap_products.put("name", generalFunc.getJsonValue("name", String.valueOf(obj_service)));
                                dataMap_products.put("category_id", generalFunc.getJsonValue("category_id", String.valueOf(obj_service)));
                                dataMap_products.put("image", generalFunc.getJsonValue("image", String.valueOf(obj_service)));
                                dataList.add(dataMap_products);
                            }
                            catadapter.notifyDataSetChanged();

                        }
                    } else
                        generalFunc.showGeneralMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));
                } else {
                    generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();
    }


    public void findProducts() throws Exception {
        flash_list.clear();
        date_list.clear();
        flash_dealAdapter.notifyDataSetChanged();


        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("type", "getCurrentDeals");
        parameters.put("customer_id", generalFunc.getMemberId());

        Utils.printLog("ProductsCategoryParameters", "::" + parameters.toString());
        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
// exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.setIsDeviceTokenGenerate(true, "vDeviceToken");
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(final String responseString) {

                Utils.printLog("ResponseData", "Data::" + responseString);

                if (responseString != null && !responseString.equals("")) {

                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);
                    flash_list.clear();
                    flash_dealAdapter.notifyDataSetChanged();

                    if (isDataAvail == true) {
                        JSONArray msgArr = generalFunc.getJsonArray(Utils.message_str, responseString);
                        JSONArray wishListDataArr = generalFunc.getJsonArray("UserWishListData", responseString);
                        ArrayList<String> wishListProductIdsList = new ArrayList<>();
                        if (wishListDataArr != null) {
                            for (int i = 0; i < wishListDataArr.length(); i++) {

                                JSONObject obj_temp = generalFunc.getJsonObject(wishListDataArr, i);

                                wishListProductIdsList.add(generalFunc.getJsonValue("product_id", obj_temp));
                            }
                        }
                        if (msgArr != null) {

                            for (int i = 0; i < msgArr.length(); i++) {
                                JSONObject obj_temp = generalFunc.getJsonObject(msgArr, i);
                                String productImg = generalFunc.getJsonValue("image", obj_temp);
                                String productName = generalFunc.getJsonValue("name", obj_temp);
                                String productDes = generalFunc.getJsonValue("description", obj_temp);
                                String productId = generalFunc.getJsonValue("product_id", obj_temp);
                                String special_price = generalFunc.getJsonValue("special_price", obj_temp);
                                String special_date_end = generalFunc.getJsonValue("special_date_end", obj_temp);
                                String price = generalFunc.getJsonValue("price", obj_temp);
// String specialPrice = generalFunc.getJsonValue("specialPrice", obj_temp);
                                String date_end = generalFunc.getJsonValue("date_end", obj_temp);
                                String catId = generalFunc.getJsonValue("category_id", obj_temp);
                                String store_country = generalFunc.getJsonValue("store_country", obj_temp);
                                String seller_id = generalFunc.getJsonValue("seller_id", obj_temp);
                                String seller_name = generalFunc.getJsonValue("seller_name", obj_temp);
                                String TotalRating = generalFunc.getJsonValue("TotalRating", obj_temp);
                                String TotalRatingCount = generalFunc.getJsonValue("TotalRatingCount", obj_temp);


                                HashMap<String, String> dataMap_products = new HashMap<>();
                                dataMap_products.put("seller_id", seller_id);
                                dataMap_products.put("seller_name", seller_name);
                                dataMap_products.put("seller_country", store_country);
                                dataMap_products.put("name", productName);
                                dataMap_products.put("category_id", catId);
                                dataMap_products.put("product_id", productId);
                                dataMap_products.put("price", price);
//dataMap_products.put("description", productDes);
                                dataMap_products.put("description", Utils.html2text(productDes));
                                dataMap_products.put("image", productImg);
                                dataMap_products.put("date_end", date_end);
                                dataMap_products.put("special_price", special_price);
                                dataMap_products.put("special_date_end", special_date_end);
                                dataMap_products.put("isWishlisted", wishListProductIdsList.contains(productId) == true ? "Yes" : "No");
                                dataMap_products.put("TotalRating", TotalRating);
                                dataMap_products.put("TotalRatingCount", TotalRatingCount);

                                if(!generalFunc.getJsonValue("date_end", obj_temp).isEmpty())
                                    date_list.add(String.valueOf(generalFunc.getJsonValue("date_end", obj_temp)));
                                //dataMap_products.put("TYPE", "" + MainPageCategoryRecycleAdapter.TYPE_ITEM);
                                flash_list.add(dataMap_products);
                            }

                        }

                        String minDate = Collections.min(date_list);
                    //    Toast.makeText(getContext(), ""+minDate, Toast.LENGTH_SHORT).show();
                        Log.e("mindate12",""+minDate);
                            runThred(minDate);

                        SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
                        Date today2=new Date();
                        try {
                             today2=dateFormat1.parse(minDate);

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        /*// DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                        Date date=new Date();
                        try {
                             date = dateFormat1.parse(minDate);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
*/
                        /*String datee = new SimpleDateFormat("yyyy-MM-dd",   Locale.getDefault()).format(date);
                        Log.e("mindate",""+datee);
                        runThred(datee);*/
                       // Toast.makeText(getContext(), ""+today2, Toast.LENGTH_SHORT).show();
                        Log.e("mindate1",""+today2);


                        // flash_dealAdapter.notifyDataSetChanged();

                    } else {
/*noProductsTxtView.setText(generalFunc.getJsonValue(Utils.message_str, responseString));
noProductsTxtView.setVisib\ility(View.VISIBLE);*/
                    }

                    closeLoader();
                } else {
// generateErrorView();
                }
            }
        });
        exeWebServer.execute();
    }
    public void getProduct() throws Exception {
        arrival_dataList.clear();
        newArrivalAdapter.notifyDataSetChanged();


        HashMap<String, String> parameters = new HashMap<>();
        Log.e("new", "newarrival" + parameters.toString());

        parameters.put("type", "getAllProductsOfCategory");
        parameters.put("customer_id", generalFunc.getMemberId());
        parameters.put("category", "newarrival");
        parameters.put("country_id", "0");


        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
// exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.setIsDeviceTokenGenerate(true, "vDeviceToken");
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(final String responseString) {

                Utils.printLog("ResponseData", "Data::" + responseString);

                if (responseString != null && !responseString.equals("")) {

                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);
                    arrival_dataList.clear();
                    newArrivalAdapter.notifyDataSetChanged();

                    if (isDataAvail == true) {
                        JSONArray msgArr = generalFunc.getJsonArray(Utils.message_str, responseString);
                        JSONArray wishListDataArr = generalFunc.getJsonArray("UserWishListData", responseString);
                        ArrayList<String> wishListProductIdsList = new ArrayList<>();
                        if (wishListDataArr != null) {
                            for (int i = 0; i < wishListDataArr.length(); i++) {

                                JSONObject obj_temp = generalFunc.getJsonObject(wishListDataArr, i);

                                wishListProductIdsList.add(generalFunc.getJsonValue("product_id", obj_temp));
                            }
                        }
                        if (msgArr != null) {

                            for (int i = 0; i < msgArr.length(); i++) {
                                JSONObject obj_temp = generalFunc.getJsonObject(msgArr, i);
                                String name = generalFunc.getJsonValue("name", obj_temp);
                                String des = generalFunc.getJsonValue("description", obj_temp);
                                String image = generalFunc.getJsonValue("image", obj_temp);
                                String price = generalFunc.getJsonValue("price", obj_temp);
                                String product_id = generalFunc.getJsonValue("product_id", obj_temp);
                                String category_id = generalFunc.getJsonValue("category_id", obj_temp);
                                String TotalRating = generalFunc.getJsonValue("TotalRating", obj_temp);
                                String TotalRatingCount = generalFunc.getJsonValue("TotalRatingCount", obj_temp);

                                HashMap<String, String> dataMap_products = new HashMap<>();
                                dataMap_products.put("name", name);
                                dataMap_products.put("description", des);
                                dataMap_products.put("image", image);
                                dataMap_products.put("price", price);
                                dataMap_products.put("product_id", product_id);
                                dataMap_products.put("category_id", category_id);
                                dataMap_products.put("TotalRating", TotalRating);
                                dataMap_products.put("TotalRatingCount", TotalRatingCount);

                                dataMap_products.put("isWishlisted", wishListProductIdsList.contains(product_id) == true ? "Yes" : "No");


//dataMap_products.put("TYPE", "" + MainPageCategoryRecycleAdapter.TYPE_ITEM);
                                arrival_dataList.add(dataMap_products);

                            }
                        }

                        newArrivalAdapter.notifyDataSetChanged();

                    } else {
/*noProductsTxtView.setText(generalFunc.getJsonValue(Utils.message_str, responseString));
noProductsTxtView.setVisib\ility(View.VISIBLE);*/
                    }

                    closeLoader();
                } else {
// generateErrorView();
                }
            }
        });
        exeWebServer.execute();
    }


    public void printDifference(Date startDate, Date endDate){

        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        System.out.println("startDate : " + startDate);
        System.out.println("endDate : "+ endDate);
        System.out.println("different : " + different);

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        //long elapsedDays = different / daysInMilli;
        //different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        System.out.printf(
                "%d hours, %d minutes, %d seconds%n",
                elapsedHours, elapsedMinutes, elapsedSeconds);

        day.setText(""+((elapsedHours/24))+"\nDays");
        elapsedHours=elapsedHours%24;
        hour.setText(elapsedHours+"\nHours");
        minute.setText(elapsedMinutes+"\nMin");
        second.setText(elapsedSeconds+"\nSec");

        Log.e("databound",elapsedHours+""+ elapsedMinutes+""+elapsedSeconds);


    }
    public void runThred(final String minDate)
    {

        Log.e("databound_r","callrun"+minDate);
        this.minDate=minDate;
        timerHandler.postDelayed(timerRunnable, 0);


    }
    String minDate="";
    final Handler timerHandler = new Handler();
    Runnable timerRunnable = new Runnable() {

        @Override
        public void run()
        {
            Calendar c = Calendar.getInstance();
            System.out.println("Current time => "+c.getTime());

            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String startDate = df.format(c.getTime());
            Log.e("databound","runthread"+minDate+"  "+startDate);


            try {
                Date end_Date= df.parse(minDate+" 00:00:00");
                Date start_date= df.parse(startDate);
                Log.e("databound12","runthread"+startDate.toString()+"  "+end_Date);
                printDifference(start_date,end_Date);
            } catch (ParseException e) {
                //  Log.e("databound","runthreaderror"+e.getMessage());
                e.printStackTrace();
            }


            timerHandler.postDelayed(this, 500);

        }
    };



}

