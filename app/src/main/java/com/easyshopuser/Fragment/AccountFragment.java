package com.easyshopuser.Fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.easyshopuser.Activity.Add_To_Cart;
import com.easyshopuser.Activity.Dashboard;
import com.easyshopuser.Activity.Feedback;
import com.easyshopuser.Activity.Login;
import com.easyshopuser.Activity.My_Favourite;
import com.easyshopuser.Activity.My_Order;
import com.easyshopuser.Activity.My_Profile;
import com.easyshopuser.Activity.NavigationList;
import com.easyshopuser.Activity.Order_Return;
import com.easyshopuser.Activity.Recent_Viewed;
import com.easyshopuser.Activity.Reset_Password;
import com.easyshopuser.Activity.Settings;
import com.easyshopuser.Activity.Shipping_Address;
import com.easyshopuser.Activity.ViewProductTransactionActivity;
import com.easyshopuser.R;
import com.easyshopuser.generalfiles.ExecuteWebServerUrl;
import com.easyshopuser.generalfiles.GeneralFunctions;
import com.easyshopuser.generalfiles.Utils;
import com.easyshopuser.utils.SharedPref;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class AccountFragment  extends Fragment
{
    TextView signin,gt_title,name,txt_login,txtCartcount;
    ImageView navigation_menu,img_serach;
    RelativeLayout cart;
    LinearLayout shipping_address,my_profile,my_favourite,logout,manage_product,store_info,changepassword,view_production;
    LinearLayout My_Message,my_order,feedback,settings,my_refer,recently_viewed,lyt_recent_purchased,become_seller,lyt_review,product_return;
    GeneralFunctions generalFunc;
    String response="";
    RelativeLayout account_layout;
    Dashboard dashboard;
    ArrayList<HashMap<String, String>> dataList = new ArrayList<>();
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
// Inflate the layout for this fragment



        final View v=inflater.inflate(R.layout.account_fragment,container,false);
        shipping_address=v.findViewById(R.id.shipping_address);
        name=v.findViewById(R.id.name);
        product_return=v.findViewById(R.id.product_return);
        lyt_recent_purchased=v.findViewById(R.id.lyt_recent_purchased);
        txt_login=v.findViewById(R.id.txt_login);
        my_favourite=v.findViewById(R.id.my_favourite);
        manage_product=v.findViewById(R.id.manage_product);
        logout=v.findViewById(R.id.logout);
        signin=v.findViewById(R.id.signin);
        store_info=v.findViewById(R.id.store_info);
        recently_viewed=v.findViewById(R.id.recently_viewed);
        generalFunc=new GeneralFunctions(getContext());
        become_seller=v.findViewById(R.id.become_seller);
        lyt_review=v.findViewById(R.id.lyt_review);
       // img_serach=v.findViewById(R.id.img_serach);
        changepassword=v.findViewById(R.id.changepassword);
        view_production=v.findViewById(R.id.view_production);

        //response= SharedPref.ReadSharePrefrence(getContext(),"userdata");

        My_Message=v.findViewById(R.id.My_Message);
        my_order=v.findViewById(R.id.my_order);
        settings=v.findViewById(R.id.settings);
        shipping_address=v.findViewById(R.id.shipping_address);
        my_favourite=v.findViewById(R.id.my_favourite);
        my_profile=v.findViewById(R.id.my_profile);
        cart=v.findViewById(R.id.dashboard_cart);
        txtCartcount=v.findViewById(R.id.txtCartcount);
        txtCartcount.setText(SharedPref.ReadSharePrefrence(getActContext(),"cart"));
        feedback=v.findViewById(R.id.feedback);
      //  gt_title=v.findViewById(R.id.gt_title);
        navigation_menu=v.findViewById(R.id.navigation_menu);
        account_layout=v.findViewById(R.id.account_layout);
        my_refer=v.findViewById(R.id.my_refer);

        if(generalFunc.getMemberId().isEmpty())
        {
            My_Message.setVisibility(View.GONE);
            my_profile.setVisibility(View.GONE);
            my_order.setVisibility(View.GONE);
            my_favourite.setVisibility(View.GONE);
            shipping_address.setVisibility(View.GONE);
            recently_viewed.setVisibility(View.GONE);
            product_return.setVisibility(View.GONE);
            lyt_recent_purchased.setVisibility(View.GONE);
            changepassword.setVisibility(View.GONE);
        }
        try {
            getUserInfo();
        }catch (Exception e){}

       /* store_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), Seller_Profile.class));
            }
        });
        manage_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), Manage_Product.class));
            }
        });*/
        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), Settings.class));
            }
        });

        /*my_refer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), My_Refer.class));
            }
        });
*/
        my_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getContext(), My_Order.class);
                startActivity(intent);
            }
        });
        My_Message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(),com.easyshopuser.Activity.My_Message.class));

            }
        });


        if (!generalFunc.getMemberId().isEmpty())
        {
            signin.setVisibility(View.GONE);
        }
        else
        {
            logout.setVisibility(View.GONE);
        }

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                logout_user();
            }
        });
        recently_viewed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getContext(), Recent_Viewed.class);
                intent.putExtra("flag","recent");
                startActivity(intent);
            }
        });
        my_favourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i2 = new Intent(getActivity(), My_Favourite.class);
                startActivity(i2);
                getActivity().overridePendingTransition( R.anim.left_to_right, R.anim.right_to_left );
            }
        });

        my_profile=v.findViewById(R.id.my_profile);
        my_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i2 = new Intent(getActivity(), My_Profile.class);
                startActivity(i2);
                getActivity().overridePendingTransition( R.anim.left_to_right, R.anim.right_to_left );

            }
        });

        shipping_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i2 = new Intent(getActivity(), Shipping_Address.class);
                startActivity(i2);
                getActivity().overridePendingTransition( R.anim.left_to_right, R.anim.right_to_left );

            }
        });

        cart=v.findViewById(R.id.dashboard_cart);
        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i2 = new Intent(getActivity(), Add_To_Cart.class);
                startActivity(i2);
                getActivity().overridePendingTransition( R.anim.left_to_right, R.anim.right_to_left );
            }
        });

        signin=v.findViewById(R.id.signin);
        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i2 = new Intent(getActivity(), Login.class);
                startActivity(i2);
                getActivity().overridePendingTransition( R.anim.slide_in_down, R.anim.slide_out_down );

            }
        });

       /* gt_title=v.findViewById(R.id.gt_title);
        gt_title.setText("My Account");*/

        navigation_menu=v.findViewById(R.id.navigation_menu);
        account_layout=v.findViewById(R.id.account_layout);
        navigation_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i2 = new Intent(getActivity(), NavigationList.class);
                startActivity(i2);
                getActivity().overridePendingTransition( R.anim.slide_in_down, R.anim.slide_out_down );

            }
        });

        feedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), Feedback.class));

            }
        });
        lyt_recent_purchased.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getContext(),My_Order.class);
                intent.putExtra("status","Complete");
                startActivity(intent);

            }
        });

       /* img_serach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                (new StartActProcess(getActContext())).startAct(SearchProductsActivity.class);
            }
        });*/
        product_return.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i2 = new Intent(getActivity(), Order_Return.class);
                startActivity(i2);
                getActivity().overridePendingTransition( R.anim.left_to_right, R.anim.right_to_left );
            }
        });
       /* become_seller.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), Seller_Profile.class));

            }
        });
       */ changepassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle=new Bundle();
                bundle.putString("codeVerification","codeVerification");
                bundle.putString("flag","account");
                bundle.putString("userId",generalFunc.getMemberId());
                generalFunc.startActivity(Reset_Password.class,bundle);
            }
        });
        view_production.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getContext(), ViewProductTransactionActivity.class);
                startActivity(intent);
            }
        });

        return v;
    }

    private void getUserInfo() {

        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("type", "getUserInfo");
        parameters.put("customer_id", generalFunc.getMemberId());

        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(getActContext(),true,generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(String responseString) {


                if (responseString != null && !responseString.equals("")) {

                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);
                    if (isDataAvail == true) {
                        JSONObject obj_msg = generalFunc.getJsonObject(Utils.message_str, responseString);

                        name.setText(generalFunc.getJsonValue("email",obj_msg));
                       /* try {
                            //checkCustomerIsSeller();
                        }catch (Exception e){}
*/

                    }
                }
            }
        });
        exeWebServer.execute();

    }

    private void logout_user() {
        SharedPref.ClearaSharePrefrence(getContext());
        SharedPref.WriteSharePrefrence1(getActContext(),"newarrival",dataList);
        SharedPref.WriteSharePrefrence1(getActContext(),"trading",dataList);

        Intent intent=new Intent(getContext(), Dashboard.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
    public void checkCustomerIsSeller() {

        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("type", "checkCustomerSeller");
        parameters.put("customer_id", generalFunc.getMemberId());

        Utils.printLog("parameters", "::" + parameters.toString());
        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(String responseString) {

                if (responseString != null && !responseString.equals("")) {

                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);

                    if (isDataAvail)
                    {
                        String store_status = generalFunc.getJsonValue("message", responseString);
                        if (store_status.equalsIgnoreCase("yes")) {
                            manage_product.setVisibility(View.VISIBLE);
                            store_info.setVisibility(View.VISIBLE);
                            become_seller.setVisibility(View.GONE);
                            //lyt_review.setVisibility(View.VISIBLE);
                            view_production.setVisibility(View.VISIBLE);

                        } else {
                            manage_product.setVisibility(View.GONE);
                            store_info.setVisibility(View.GONE);
                            become_seller.setVisibility(View.VISIBLE);
                            //lyt_review.setVisibility(View.GONE);
                            view_production.setVisibility(View.GONE);
                        }

//                        if (generalFunc.getJsonValue("IS_ALL_INFO_DONE", responseString).equalsIgnoreCase("Yes")) {
////                            becomeAsellerArea.setVisibility(View.GONE);
//                            becomeSellerTxtView.setText("Store Information");
//                        } else {
//                            becomeAsellerArea.setVisibility(View.VISIBLE);
//                        }
                    } else {
                        manage_product.setVisibility(View.GONE);
                        store_info.setVisibility(View.GONE);
                        become_seller.setVisibility(View.VISIBLE);
                        lyt_review.setVisibility(View.GONE);
                    }
                } else {
                    generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();
    }

    public Context getActContext() {

        return getContext();
    }

}
