package com.easyshopuser.Fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.easyshopuser.Adapter.CategoryAdapter;
import com.easyshopuser.R;
import com.easyshopuser.generalfiles.ExecuteWebServerUrl;
import com.easyshopuser.generalfiles.GeneralFunctions;
import com.easyshopuser.generalfiles.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class CategoryFragment extends Fragment
{

    RecyclerView category_list;
    CategoryAdapter categoryAdapter;
    GeneralFunctions generalFunc;
    ArrayList<HashMap<String, String>> dataList = new ArrayList<>();
    String category_id="";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.category_fragment, container, false);

        generalFunc=new GeneralFunctions(getActContext());

        category_list=v.findViewById(R.id.category_list);
        @SuppressLint("WrongConstant") GridLayoutManager layoutManager=new GridLayoutManager(v.getContext(),2, GridLayoutManager.VERTICAL,false);
        category_list.setLayoutManager(layoutManager);
        categoryAdapter = new CategoryAdapter(getActivity(),dataList);
        category_list.setAdapter(categoryAdapter);
        category_list.setNestedScrollingEnabled(false);
        categoryAdapter.notifyDataSetChanged();
        getCategory();



        return v;
    }


    private void getCategory()
    {
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("type", "getAllCategories");

        Log.e("category","cat"+parameters.toString());

        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(final String responseString) {
                Utils.printLog("ResponseData", "Data::" + responseString);
                if (responseString != null && !responseString.equals("")) {
                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);
                    if (isDataAvail)
                    {
                        JSONArray msgArr = generalFunc.getJsonArray(Utils.message_str, responseString);
                        dataList.clear();
                        if (msgArr != null)
                        {
                            for (int i = 0; i < msgArr.length(); i++) {
                                JSONObject obj_service = generalFunc.getJsonObject(msgArr, i);
                                HashMap<String, String> dataMap_products = new HashMap<>();
                                dataMap_products.put("name",generalFunc.getJsonValue("name", String.valueOf(obj_service)));
                                dataMap_products.put("category_id",generalFunc.getJsonValue("category_id", String.valueOf(obj_service)));
                                dataMap_products.put("image",generalFunc.getJsonValue("image", String.valueOf(obj_service)));
                                dataList.add(dataMap_products);
                            }
                            categoryAdapter.notifyDataSetChanged();

                        }
                    }
                    else
                        generalFunc.showGeneralMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));
                } else
                {
                    generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();
    }
    private Context getActContext() {
        return getContext();
    }
}
