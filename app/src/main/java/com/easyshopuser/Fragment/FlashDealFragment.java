package com.easyshopuser.Fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.easyshopuser.Adapter.Flash_DealAdapter;
import com.easyshopuser.R;
import com.easyshopuser.generalfiles.ExecuteWebServerUrl;
import com.easyshopuser.generalfiles.GeneralFunctions;
import com.easyshopuser.generalfiles.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static com.easyshopuser.generalfiles.Utils.closeLoader;

public class FlashDealFragment extends Fragment
{

    RecyclerView flash_deal_list;
    ArrayList<HashMap<String, String>> dataList = new ArrayList<>();
    GeneralFunctions generalFunc;

    Flash_DealAdapter adapter;
    View layout_cart_empty;
    LinearLayout data;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.flash_deal_fragment, container, false);

        generalFunc=new GeneralFunctions(getActivity());
        layout_cart_empty=v.findViewById(R.id.layout_cart_empty);
        data=v.findViewById(R.id.data);
        flash_deal_list=v.findViewById(R.id.flash_deal_list);
        @SuppressLint("WrongConstant") GridLayoutManager layoutManager_flash=new GridLayoutManager(getActivity(),2, GridLayoutManager.VERTICAL,false);
        flash_deal_list.setLayoutManager(layoutManager_flash);
        adapter = new Flash_DealAdapter(getActivity(),dataList);
        flash_deal_list.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        findProducts();
        return v;
    }

    public void findProducts() {
        dataList.clear();
        adapter.notifyDataSetChanged();

        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("type", "getCurrentDeals");
        parameters.put("customer_id", generalFunc.getMemberId());

        Utils.printLog("ProductsCategoryParameters", "::" + parameters.toString());
        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
//        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.setIsDeviceTokenGenerate(true, "vDeviceToken");
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(final String responseString) {

                Utils.printLog("ResponseData", "Data::" + responseString);

                if (responseString != null && !responseString.equals("")) {

                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);
                    dataList.clear();
                    adapter.notifyDataSetChanged();

                    if (isDataAvail == true) {
                        JSONArray msgArr = generalFunc.getJsonArray(Utils.message_str, responseString);
                        JSONArray wishListDataArr = generalFunc.getJsonArray("UserWishListData", responseString);
                        ArrayList<String> wishListProductIdsList = new ArrayList<>();
                        if (wishListDataArr != null) {
                            for (int i = 0; i < wishListDataArr.length(); i++) {

                                JSONObject obj_temp = generalFunc.getJsonObject(wishListDataArr, i);

                                wishListProductIdsList.add(generalFunc.getJsonValue("product_id", obj_temp));
                            }
                        }
                        if (msgArr != null) {

                            for (int i = 0; i < msgArr.length(); i++) {
                                JSONObject obj_temp = generalFunc.getJsonObject(msgArr, i);
                                String productImg = generalFunc.getJsonValue("image", obj_temp);
                                String productName = generalFunc.getJsonValue("name", obj_temp);
                                String productDes = generalFunc.getJsonValue("description", obj_temp);
                                String productId = generalFunc.getJsonValue("product_id", obj_temp);
                                String special_price = generalFunc.getJsonValue("special_price", obj_temp);
                                String special_date_end = generalFunc.getJsonValue("special_date_end", obj_temp);
                                String price = generalFunc.getJsonValue("price", obj_temp);
                                // String specialPrice = generalFunc.getJsonValue("specialPrice", obj_temp);
                                String date_end = generalFunc.getJsonValue("date_end", obj_temp);
                                String catId = generalFunc.getJsonValue("category_id", obj_temp);
                                String store_country = generalFunc.getJsonValue("store_country", obj_temp);
                                String seller_id = generalFunc.getJsonValue("seller_id", obj_temp);
                                String seller_name = generalFunc.getJsonValue("seller_name", obj_temp);
                                String TotalRating=generalFunc.getJsonValue("TotalRating",obj_temp);


                                HashMap<String, String> dataMap_products = new HashMap<>();
                                dataMap_products.put("seller_id", seller_id);
                                dataMap_products.put("seller_name", seller_name);
                                dataMap_products.put("seller_country", store_country);
                                dataMap_products.put("name", productName);
                                dataMap_products.put("category_id", catId);
                                dataMap_products.put("product_id", productId);
                                dataMap_products.put("price", price);
                                dataMap_products.put("description", productDes);
                                dataMap_products.put("image", productImg);
                                dataMap_products.put("date_end", date_end);
                                dataMap_products.put("special_price", special_price);
                                dataMap_products.put("TotalRating", TotalRating);
                                dataMap_products.put("special_date_end", special_date_end);
                                dataMap_products.put("isWishlisted", wishListProductIdsList.contains(productId) == true ? "Yes" : "No");

                                //dataMap_products.put("TYPE", "" + MainPageCategoryRecycleAdapter.TYPE_ITEM);
                                dataList.add(dataMap_products);
                            }
                        }

                        adapter.notifyDataSetChanged();

                    } else {

                        if (dataList.size()==0)
                        {
                            data.setVisibility(View.GONE);
                            layout_cart_empty.setVisibility(View.VISIBLE);
                        }
                        /*noProductsTxtView.setText(generalFunc.getJsonValue(Utils.message_str, responseString));
                        noProductsTxtView.setVisibility(View.VISIBLE);*/
                    }

                    closeLoader();
                } else {
                    //   generateErrorView();
                }
            }
        });
        exeWebServer.execute();
    }

}

