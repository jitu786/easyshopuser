package com.easyshopuser.Fragment;

import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.easyshopuser.R;
import com.easyshopuser.bannerslider.banners.Banner;
import com.easyshopuser.generalfiles.ExecuteWebServerUrl;
import com.easyshopuser.generalfiles.GeneralFunctions;
import com.easyshopuser.generalfiles.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SpecificationFragment extends Fragment
{

    GeneralFunctions generalFunc;
    String message="";
    TextView desciption,p_modelnum,weightt,sellernamee,p_model_name;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.specification_fragment, container, false);

        desciption=v.findViewById(R.id.desciption);
        p_modelnum=v.findViewById(R.id.p_modelnum);
        weightt=v.findViewById(R.id.weight);
        sellernamee=v.findViewById(R.id.sellername);
        p_model_name=v.findViewById(R.id.p_model_name);

        Bundle bundle = getArguments();
        message = bundle.getString("product_id");
        generalFunc=new GeneralFunctions(getActivity());
        getProductInfo();

        return v;
    }



    public void getProductInfo()
    {
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("type", "getProductInfo");
        parameters.put("product_id", message);
        parameters.put("customer_id", generalFunc.getMemberId());


        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(getActivity(), true, generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(final String responseString) {
                Utils.printLog("ResponseData", "Data::" + responseString);
                if (responseString != null && !responseString.equals("")) {
                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);
                    if (isDataAvail) {
                        JSONObject obj_cat = generalFunc.getJsonObject(Utils.message_str, responseString);


                        String name = generalFunc.getJsonValue("name", obj_cat);
                        String quantity = generalFunc.getJsonValue("quantity", obj_cat);
                        String store_name = generalFunc.getJsonValue("store_name", obj_cat);
                        String store_country = generalFunc.getJsonValue("store_country", obj_cat);
                        String description = generalFunc.getJsonValue("description", obj_cat);
                        String image = generalFunc.getJsonValue("image", obj_cat);
                        String price = generalFunc.getJsonValue("price", obj_cat);
                        String sp_price = generalFunc.getJsonValue("special_price", obj_cat);
                        String product_id = generalFunc.getJsonValue("product_id", obj_cat);
                        String category_id = generalFunc.getJsonValue("category_id", obj_cat);
                        String seller_name = generalFunc.getJsonValue("seller_name", obj_cat);
                        String shipping = generalFunc.getJsonValue("shipping", obj_cat);
                        String points = generalFunc.getJsonValue("points", obj_cat);
                        String weight = generalFunc.getJsonValue("weight", obj_cat);
                        String viewed = generalFunc.getJsonValue("viewed", obj_cat);
                        String stock_status_id = generalFunc.getJsonValue("stock_status_id", obj_cat);
                        String TotalRating = generalFunc.getJsonValue("TotalRating", obj_cat);
                        String TotalRatingCount = generalFunc.getJsonValue("TotalRatingCount", obj_cat);
                        String discount = generalFunc.getJsonValue("discount", obj_cat);
                        String SHARE_DATA = generalFunc.getJsonValue("SHARE_DATA", obj_cat);
                        String title = generalFunc.getJsonValue("title", obj_cat);
                        String seller_id = generalFunc.getJsonValue("seller_id", obj_cat);
                        String model = generalFunc.getJsonValue("model", obj_cat);
                        String date_end_format = generalFunc.getJsonValue("date_end_format", obj_cat);
                        JSONArray msgArr = generalFunc.getJsonArray("MoreImages", obj_cat);

                        List<Banner> banners = new ArrayList<>();

                        p_model_name.setText(name);
//desciption.setText(description);
                        desciption.setText(Html.fromHtml(Utils.html2text(description)));

                        if (model.isEmpty())
                        {
                            p_modelnum.setText("N/A");

                        }
                        else
                        {
                            p_modelnum.setText(model);
                        }


                        if (weight.isEmpty())
                        {
                            weightt.setText("N/A");

                        }
                        else {
                            weightt.setText(weight);
                        }
                        if (seller_name.isEmpty())
                        {

                            sellernamee.setText("N/A");
                        }
                        else
                        {
                            sellernamee.setText(seller_name);
                        }

                        JSONArray wishListDataArr = generalFunc.getJsonArray("UserWishListData", responseString);
                        ArrayList<String> wishListProductIdsList = new ArrayList<>();
                        if (wishListDataArr != null) {
                            for (int i = 0; i < wishListDataArr.length(); i++) {

                                JSONObject obj_temp = generalFunc.getJsonObject(wishListDataArr, i);

                                wishListProductIdsList.add(generalFunc.getJsonValue("product_id", obj_temp));
                            }
                        }



//JsonArray msg =generalFunc.getJsonArray("MoreImages",);

                    }
                    else
                        generalFunc.showGeneralMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));

                } else {
                    generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();
    }




}
